import '../../utils/app_localizations.dart';

extension Languages on String {
  l(context, [List args = const []]) {
    return AppLocalizations.of(context).t(this, args);
  }
}

final _Strings Strings = _Strings();

//class _Strings {
//  final tooltips = Tooltips();
//}
//
//class Tooltips {
//  final String hamburger = "hamburger";
//}

class _Strings {
  final String app_name = "app_name";
  final tooltips = Tooltips();
  final pages = Pages();
  final errors = Errors();
}

class Tooltips {
  final String hamburger = "tooltips.hamburger";
  final String search = "tooltips.search";
  final String cart = "tooltips.cart";
  final String bookmark = "tooltips.bookmark";
}

class Pages {
  final home_screen = Home_screen();
}

class Home_screen {
  final String title = "pages.home_screen.title";
  final filter = Filter();
}

class Filter {
  final String all_text = "pages.home_screen.filter.all_text";
  final String temples_text = "pages.home_screen.filter.temples_text";
  final String monasteries_text = "pages.home_screen.filter.monasteries_text";
}

class Errors {
  final String web_error = "errors.web_error";
}
