import '../brain.dart';

enum Images {
  cart,
  rounded_pause_media,
  info,
  phone,
  play_media,
  location,
  social_facebook,
  social_gplus,
  search,
  uber_taxi,
  location_marker,
  forward_media,
  ruble,
  route,
  backward_media,
  social_vk,
  pause_media,
  bookmark,
  booking,
  new_note,
  rounded_play_media,
  bookmark_filled,
  about_health,
  hamburger,
  yandex_taxi,
  about_repose,
}

extension IconExtension on Images {
  String i() {
    String name = toString().replaceAll(runtimeType.toString() + '.', '');
    String filename;
    //if (name.contains('_'))
    //  filename = name.re('_', '.');
    //else
    filename = name + '.svg';
    return mindClass.getI() + filename;
  }
}
