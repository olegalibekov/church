parseTree(Map map) {
  Map newObj = {};
  internal(String outerKey, Map data) {
    data.forEach((key, value) {
      if (value is! Map)
        newObj.addEntries([MapEntry('$outerKey/$key', value)]);
      else
        internal('$outerKey/$key', value);
    });
  }

  internal('', map);
  return newObj;
}

parseList(Map map) {
  Map newObj = {};
  map.forEach((key, value) {
    List<String> keys = key.split('/');
    keys = keys.sublist(1);
    var currentObj = newObj;
    while (keys.length > 1) {
      if (!currentObj.containsKey(keys.first)) currentObj[keys.first] = {};
      currentObj = currentObj[keys.first];
      keys.removeAt(0);
    }
    currentObj[keys.first] = value;
  });
  return newObj;
}

//abstract class PageW extends StatefulWidget {}
//
//abstract class PageState<T extends StatefulWidget> extends State<T> {}
//
//abstract class PageModel<T> {
//  @protected
//  initParams(Object data);
//}