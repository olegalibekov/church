import 'package:church/head_module/obj_conv.dart';
import 'package:church/models/church/owner_model.dart';
import 'package:church/models/user/user_model.dart';
import 'package:church/utils/constants.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';

import 'brain.dart';

class MapInterface extends ModelInterface {
  final Function objectInitializer;

  MapInterface(this.objectInitializer);

  @override
  String child() => null;

  @override
  Map<TString, Function> fields() {
    throw UnimplementedError();
  }

  @override
  toJson() {
    //if (_typeReview()) throw ModelInterface.uploadTypeError;
    _o.removeWhere((key, value) => value == null);
    var copy = _o;
    return copy.map((key, value) => MapEntry(
        key,
        value is NullObject
            ? null
            : value is ModelInterface ? value.toJson() : value));
  }

  @override
  ModelInterface fromJson(rawJson) {
    if (rawJson == null) rawJson = {};
    final tmpMap = Map.from(rawJson);
    if (tmpMap == null) throw NullThrownError();
    tmpMap.keys.forEach((element) {
      set([TString('empty').key(element)], tmpMap[element]);
    });
    return this;
  }

  @override
  set(List<TString> keys, Object value) {
    if (keys.first._key.isEmpty) {
      _o = value;
    }
    if (value == null) throw NullThrownError();
    if (keys.length == 0) return;

    if (!_o.containsKey(keys.first._key) || _o[keys.first._key] == null) {
      _o[keys.first._key] = objectInitializer();
    }
    if (keys.length > 1)
      (_o[keys.first._key] as ModelInterface).set(keys.sublist(1), value);
    else
      _o[keys.first._key] = value;

    //if (autoUpdate) upload();
  }

  @override
  get(List<TString> keys) {
    if (keys.first._key.isEmpty) {
      return _o;
    }
    if (keys == null) return null;
    if (keys.length == 0) return null;

    if (!_o.containsKey(keys.first._key) || _o[keys.first._key] == null) {
      return null;
    }

    if (keys.length > 1)
      return (_o[keys.first._key] as ModelInterface).get(keys.sublist(1));
    else
      return _o[keys.first._key];
  }

  //getAll(){}

  @override
  ModelInterface init() => MapInterface(objectInitializer);
}

/// [ModelInterface] данный класс - основа для любой САМОСТОЯТЕЛЬНОЙ модели, например User или Church, но не для Owner
abstract class ModelInterface {
  Map<String, Object> _o = {};
  String data;

  DatabaseReference baseReference;
  String key;
  Duration _localTimeout;
  bool autoUpdate = false;

  static final String uploadStateDone = "upload_state_done";
  static final String uploadStateTimeout = "upload_state_timeout";
  static final String uploadTypeError = "upload_type_error";

  ///Path to object without last part (id)
  String child();

  Map<TString, Function> fields();

  ModelInterface init();

  @override
  String toString() {
    return _o.toString();
  }

  toJson() {
    if (_typeReview()) throw uploadTypeError;
    var _fields = fields().map((key, value) => MapEntry(key.field, value));
    _o.removeWhere((key, value) => !_fields.containsKey(key));
    _o.removeWhere((key, value) => value == null);
    var copy = _o;
    return copy.map((key, value) => MapEntry(
        key,
        value is NullObject
            ? null
            : value is ModelInterface ? value.toJson() : value));
  }

  reload() async {
    fromJson((await baseReference.child(child()).child(key).once()).value);
  }

  ModelInterface fromJson(dynamic rawJson) {
    if (rawJson == null) rawJson = {};
    final tmpMap = Map.from(rawJson);
    if (tmpMap == null) throw NullThrownError();
    fields().keys.forEach((element) {
      set([element], tmpMap[element.field]);
    });
    return this;
  }

  upload() async {
    bool isTimeout = false;
    if (_typeReview()) return uploadTypeError;
    var _fields = fields().map((key, value) => MapEntry(key.field, value));
    _o.removeWhere((key, value) => !_fields.containsKey(key));
    _o.removeWhere((key, value) => value == null);
    final Map lowCopy = parseTree(_o.map((key, value) => MapEntry(
        key,
        value is NullObject
            ? null
            : value is ModelInterface ? value.toJson() : value)));
    await baseReference
        .child(child())
        .child(key)
        .update(lowCopy.map((key, value) => MapEntry(
            key,
            value is NullObject
                ? null
                : value is ModelInterface ? value.toJson() : value)))
        .timeout(_localTimeout, onTimeout: () {
      isTimeout = true;
    });
    if (isTimeout) return uploadStateTimeout;
    return uploadStateDone;
  }

  _typeReview() {
    for (Object i in _o.values) {
      if (i is String) if (i.isEmpty) return true;
    }
    return false;
  }

  ModelInterface({baseReference, localTimeout}) {
    this._localTimeout = localTimeout;
    this.baseReference = baseReference;
    if (this.baseReference == null)
      this.baseReference = mindClass.rootReference;
    if (this._localTimeout == null) this._localTimeout = timeout;
  }

  remove(TString key) => _o[key.field] = NullObject();

  _tryRepair(TString key, value) {
    if (key._type is String) {
      if (value is num) return value.toString();
    }
    if (key._type is num) {
      if (value is String) return num.tryParse(value);
    }
    return null;
  }

  set(List<TString> keys, Object value) {
    if (value == null) {
      print(
          'Sorry, but you cannot write null value, use remove method if you want to delete some data');
      return;
    }
    if (keys.length == 0) {
      print('You need to put not null and >0 keys here');
      return;
    }
    if (!fields().containsKey(keys.first)) {
      print('Sorry, but we do not have such fields here');
      return;
    }

    if (keys.length > 1) {
      var type = keys.first.runtimeType.toString();
      type = type.substring(type.indexOf('<') + 1, type.lastIndexOf('>'));

      //if (keys.first._type is! ModelInterface ||
      //    keys.first._type is! Map<String, ModelInterface>) return;
      if (!_o.containsKey(keys.first.field) || _o[keys.first.field] == null) {
        _o[keys.first.field] = keys.first.objectInitializer();
      }
      var key = keys.first.field;
      keys.removeAt(0);
      (_o[key] as ModelInterface).set(keys, value);
    } else {
      if (keys.first.objectInitializer != null &&
          keys.first.objectInitializer() is ModelInterface) {
        if (!_o.containsKey(keys.first.key) || _o[keys.first.key] == null) {
          _o[keys.first.field] = keys.first.objectInitializer();
        }
        (_o[keys.first.field] as ModelInterface).fromJson(value);
      } else
        _o[keys.last.field] = value;
    }
    if (autoUpdate) upload();
  }

  get(List<TString> keys) {
    if (keys == null) return null;
    if (keys.length == 0) return null;
    if (keys.length > 1) {
      var type = keys.first.runtimeType.toString();
      //type = type.substring(type.indexOf('<') + 1, type.lastIndexOf('>'));

      if (!type.contains('Model')) return;
      if (!_o.containsKey(keys.first.field) || _o[keys.first.field] == null) {
        return null;
      }
      var key = keys.first.field;
      return (_o[key] as ModelInterface).get(keys.sublist(1));
    } else {
      if (keys.first.objectInitializer != null &&
          keys.first.objectInitializer() is ModelInterface) {
//        print("yeah");
        if (!_o.containsKey(keys.first.field) || _o[keys.first.field] == null) {
          return null;
        }
        return (_o[keys.first.field] as ModelInterface);
      } else
        return _o[keys.last.field] is NullObject ? null : _o[keys.last.field];
    }
  }

  Object operator [](List<TString> keys) => get(keys);

  void operator []=(List<TString> keys, Object value) => set(keys, value);
}

extension Nap<S extends String, V extends ModelInterface> on Map<String, V> {
  Future<Map<String, ModelInterface>> loadAll(ModelInterface modelInterface,
      [String query]) async {
    if (modelInterface == null) throw NullThrownError();
    var tmpModels = (await modelInterface.baseReference
            .child(modelInterface.child())
            .once())
        .value;
    if (tmpModels == null) {
      print("no values at all");
      return null;
    }
    Map models = Map.from(tmpModels as Map);
    List<MapEntry<String, V>> modelEntries = [];
    for (var i = 0, l = models.length, entries = models.entries.toList();
    i < l;
    i++) {
      MapEntry modelEntry = (entries[i]);
      ModelInterface modelValue =
      modelInterface.init().fromJson(modelEntry.value);
      modelValue.key = modelEntry.key;
      modelEntries.add(MapEntry(modelEntry.key, modelValue));
    }
    this.addEntries(modelEntries);
    return this;
  }
}

extension Mist<V extends ModelInterface> on List<V> {
  loadAll(ModelInterface modelInterface) async {
    if (modelInterface == null) throw NullThrownError();
    var tmpModels = (await modelInterface.baseReference
        .child(modelInterface.child())
        .once())
        .value;
    if (tmpModels == null) {
      print("no values at all");
      return;
    }
    List models = (tmpModels as List);
    for (var i = 0, l = models.length; i < l; i++) {
      ModelInterface modelValue = modelInterface.init().fromJson(models[i]);
      modelValue.key = i.toString();
      this.add(modelValue);
    }
  }
}

/// [NullObject] данный класс позволяет удалить те или иные данные.
/// скорее всего им не придется пользоваться, т.к есть метод [remove]
class NullObject {}

/// [T] as Type; [K] as Key. данный класс позволяет описать ключ, а также предполагаемый используемый тип.
/// по сути выступает в качестве константы
class TString<T> {
  String field;

  T _type;
  Function typeCheck;
  Function valueReader;
  Function objectInitializer;
  String _key = "";

  TString key(String key) {
    final tString = TString<T>(field);
    tString._key = key;
    return tString;
  }

  factory TString(field, {valueReader, typeCheck, objectInitializer}) {
    assert(field.runtimeType != dynamic.runtimeType);
    return TString._internal(field,
        valueReader: valueReader,
        typeCheck: typeCheck,
        objectInitializer: objectInitializer);
  }

//  void operator =(String field){
//    TString._internal(field);
//  };
  //TString.special(){};

  TString._internal(this.field,
      {this.valueReader, this.typeCheck, this.objectInitializer}) {
    if (T.toString().contains('Model')) {
      if (objectInitializer == null) {
        print('For TString<$T> in [${T.toString()}] no objectInitializer');
        throw new ErrorHint(
            'For TString<$T> in [${T.toString()}] no objectInitializer');
      }
      if (T.toString().contains('Map')) {
//        print('Printed map: $T, $field');
        objectInitializer = () => MapInterface(objectInitializer);
      }
    }
  }

  Object operator <<(Object obj) => field = obj;
  Object operator = (Object obj) => print("hi");
}

/// [tString] данное расширение не обязательно для использования, хотя добавляет немного легкости при написании кода
/// например:
/// static final TString<String> userName = TString("user_name"); - обычный способ
///  static final TString<String> secondName = "second_name".t();  - способ с использованием расщирения
extension tString on String {
  /// [t] helps you to create instance of TString<T> with Special type
  t<T>({Function valueReader, Function typeCheck, Function objectInitializer}) {
    return new TString<T>(this,
        valueReader: valueReader,
        typeCheck: typeCheck,
        objectInitializer: objectInitializer);
  }
}

/// [UserModel] данный класс демонстрирует использование вышеописанных инструментов, его не стоит использовать где либо.
/// в дальнешем, если нужно будет создать модель со схожим названием, достаточно будет просто написать так:
/// import 'написать путь к файлу, который ты сейчас читаешь.dart' hide UserModel;
//class UserModel extends ModelInterface {
//  static final TString userNameField = new TString<String>("user_name");
//  static final TString secondNameField = "second_name".t<String>();
//  static final TString ownerField =
//  "owner".t<OwnerModel>(objectInitializer: () => OwnerModel());
//
//  static final TString roleField = "role".t<String>();
//
//  ///[child] обязательно нужно указать данный параметр
//  @override
//  String child() => "user_data";
//
//  static final String owner = "owner";
//  static final String admin = "admin";
//  static final String editor = "editor";
//  static final String user = null;
//
//  roles() {
//    return [user, owner, admin, editor];
//  }
//
//  @override
//  Map<TString, void> params() => {userNameField: null, secondNameField: null};
//
//  @override
//  ModelInterface init() => UserModel();
//}

//class OwnerModel extends ModelInterface {
//  static final TString<String> userNameField = TString("user_name");
//  static final TString<String> secondNameField = "second_name".t();
//
//  @override
//  String child() {
//    return "user_data/id/owner";
//  }
//
//  @override
//  Map<TString, void> params() {
//    return {userNameField: null, secondNameField: null};
//  }
//
//  @override
//  ModelInterface init() => OwnerModel();
//}

/// [testMethod] данный метод демонстрирует работу созданной модели. чуть позже будет пример с моделью,
/// внутри которой будет находится комплексный обьект(например другая мини модель)
testMethod() {
  _print(val) {
    print('t: $val');
  }

  List<int> array = new List();
  UserModel user = UserModel();
  final TString userNameField = TString<String>("user_name");

  user.key = "hhj80khcdcgustdfgc";
  user[[UserModel.firstNameField]] = "Yaroslav";
  _print("test");
  _print(user.get([UserModel.userInfoField]));
  user.remove(UserModel.userInfoField);
  user.set([UserModel.userInfoField], UserModel.admin);
  user[[UserModel.firstNameField]] = "Ronan";

  _print(user.get([UserModel.userInfoField]));
  _print(user.get([UserModel.firstNameField]));
//  _print(user.get([UserModel.ownerField]));
//  _print(user.get([UserModel.ownerField, OwnerModel.userNameField]));

  user.set([UserModel.firstNameField], OwnerModel());

//  _print(user.get([UserModel.ownerField]));
//  _print(user.get([UserModel.ownerField, OwnerModel.userNameField]));
}





