import 'package:church/head_module/brain.dart';
import 'package:church/pages/bookmarks/bookmarks_page.dart';
import 'package:church/pages/cart/cart_page.dart';
import 'package:church/pages/church/church_page.dart';
import 'package:church/pages/history/history_page.dart';
import 'package:church/pages/podcast/podcast_page.dart';
import 'package:church/pages/profile/profile_page.dart';
import 'package:church/pages/root_page.dart';
import 'package:church/pages/signIn/sign_in_page.dart';
import 'package:church/utils/fluro/fluro.dart';
import 'package:flutter/material.dart';

class FluroRouter {
  static Router router = Router();

  static Handler _rootPageHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return RootPage();
  });
  static Handler _profilePageHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    if (mindClass.user.isAnonymous) return SignIn();
    return Profile();
  });
  static Handler _podcastPageHandler =
  Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return Podcast();
  });
  static Handler _bookmarksPageHandler =
  Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return Bookmarks();
  });
  static Handler _historyPageHandler =
  Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return History();
  });
  static Handler _cartPageHandler =
  Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return Cart();
  });

  static Handler _churchPageHandler =
  Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    //var argument = Argument()(context);
    return ChurchPage(
      churchModel: Argument()(context),
    );
  });

//  static _objTakeOut(Map<String, dynamic> params) {
//    String objKey = params["objectKey"][0];
//    var object = mindClass.romData[objKey];
//    mindClass.romData.remove(objKey);
//    return object;
//  }

  static void setupRouter() {
    router.define('/',
        handler: _rootPageHandler, transitionType: TransitionType.cupertino);

    router.define(Pages.profile(),
        handler: _profilePageHandler, transitionType: TransitionType.cupertino);

    router.define(Pages.podcast(),
        handler: _podcastPageHandler, transitionType: TransitionType.cupertino);

    router.define(Pages.bookmarks(),
        handler: _bookmarksPageHandler,
        transitionType: TransitionType.cupertino);

    router.define(Pages.history(),
        handler: _historyPageHandler, transitionType: TransitionType.cupertino);

    router.define(Pages.cart(),
        handler: _cartPageHandler, transitionType: TransitionType.cupertino);

    router.define(Pages.church_page(),
        handler: _churchPageHandler, transitionType: TransitionType.cupertino);
  }
}

enum Pages { church_page, cart, history, bookmarks, podcast, profile }

extension PagesX on Pages {
  call() {
    return toString().replaceAll(runtimeType.toString() + '.', '');
  }
}

class Argument {
  Object call(BuildContext context) {
    return ModalRoute.of(context).settings.arguments;
  }
}

moveTo(BuildContext context, String key, Object object) {
//  var objectKey = DateTime.now().millisecondsSinceEpoch.toString();
//  mindClass.romData[objectKey] = object;
  Navigator.pushNamed(context, '$key', arguments: object);
}
