import 'package:church/head_module/database_extender.dart';
import 'package:church/models/podcast_model.dart';

import '../brain.dart';
import 'auth_extension.dart';

extension Podcast on DatabaseModule {
  getPodcasts() async {
    if (checkUser()) return;
    Map<String, PodcastModel> podcasts = {};
    await podcasts.loadAll(PodcastModel());
    return podcasts;
  }

  sortNewestPodcasts(Map<String, PodcastModel> podcasts) {
    var sortedKeys = podcasts.keys.toList(growable: false)
      ..sort((k1, k2) => podcasts[k2]
          .get([PodcastModel.timestampField]).compareTo(
              podcasts[k1].get([PodcastModel.timestampField])));
    Map<String, PodcastModel> sortedMap = {};
    for (var item = 0; item < sortedKeys.length; item++) {
      sortedMap[sortedKeys[item]] = podcasts[sortedKeys[item]];
      if (item == 9) break;
    }
    return sortedMap;
  }

  sortPopularPodcasts(Map<String, PodcastModel> podcasts) {
    Map<String, int> podcastsRating = {};

    for (var p in podcasts.entries) {
      var rating = p.value.get([PodcastModel.ratingField]);
      int totalRating = 0;
      for (var r in rating.values)
        totalRating += r;
      podcastsRating[p.key] = totalRating;
    }

    var sortedKeys = podcastsRating.keys.toList(growable: false)
      ..sort((k1, k2) => podcastsRating[k2].compareTo(podcastsRating[k1]));

    Map<String, PodcastModel> sortedMap = {};
    for (var item = 0; item < sortedKeys.length; item++)
      sortedMap[sortedKeys[item]] = podcasts[sortedKeys[item]];
    return sortedMap;
  }

  updatePodcastRating(MapEntry<String, PodcastModel> podcast) async {
    if (checkUser()) return;
    int rating =
    podcast.value.get([PodcastModel.ratingField])[mindClass.user.uid];
    if (rating != null)
      podcast.value
          .set([PodcastModel.ratingField], {mindClass.user.uid: rating += 1});
    else
      podcast.value.set([PodcastModel.ratingField], {mindClass.user.uid: 1});
    await podcast.value.upload();
//    int rating = mindClass.globalPodcasts.value[podcastKey]
//        .get([PodcastModel.ratingField])[mindClass.user.uid];
//    mindClass.globalPodcasts.value[podcastKey]
//        .set([PodcastModel.ratingField], {mindClass.user.uid: rating += 1});
//    await mindClass.globalPodcasts.value[podcastKey].upload();
  }
}

//  createPodcast(
//      {@required String podcastType, @required String podcastId}) async {
//    if (checkUser()) return;
//    PodcastModel podcastModel = PodcastModel();
//    podcastModel.key = podcastId;
//    podcastModel.set([PodcastModel.nameField], 'empty');
//    await podcastModel.upload();
//  }

//  updatePodcast({@required String type,
//    @required String id,
//    @required TString valueToUpdate,
//    @required String data}) async {
//    PodcastModel podcastModel = PodcastModel();
//    podcastModel.podcastType = type;
//    podcastModel.key = id;
//    podcastModel.set([valueToUpdate], data);
//    await podcastModel.upload();
//  }

//  createPodcast(bool typeIsPopular) async {
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.podcasts)
//        .child(
//        typeIsPopular == true ? DatabasePaths.popular : DatabasePaths.all)
//        .push();
//
//    databaseReference.set('Empty');
//  }

//  updatePodcast(bool typeIsPopular, String podcastId,
//      Map<dynamic, dynamic> podcastData) async {
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.podcasts)
//        .child(
//        typeIsPopular == true ? DatabasePaths.popular : DatabasePaths.all)
//        .child(podcastId);
//
//    databaseReference.update(podcastData);
//  }

//  List<PodcastModel> formatPodcasts(dynamic value) {
//    Map<String, dynamic> requests = Map.from(value);
//    List<PodcastModel> trueRequests = requests
//        .map((v1, v2) {
////          print(v2);
//          var podcastAudio = PodcastModel.fromJson(Map.from(v2));
//          podcastAudio.podcastAudioId = v1;
//          return MapEntry(v1, podcastAudio);
//        })
//        .values
//        .toList();
////    print(trueRequests[0].rating.length);
//    return trueRequests;
//  }

//  readAllPodcasts() async {
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.podcasts)
//        .child(DatabasePaths.all);
//    DataSnapshot snapshot = await databaseReference.once();
//    return formatPodcasts(Map.from(snapshot.value));
//  }

//  readPopularPodcasts() async {
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.podcasts)
//        .child(DatabasePaths.popular);
//    DataSnapshot snapshot = await databaseReference.once();
//    return formatPodcasts(Map.from(snapshot.value));
//  }

//  podcastRating(String podcastType, String podcastId) async {
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.podcasts)
//        .child(podcastType)
//        .child(podcastId);
//    DataSnapshot snapshot = await databaseReference.once();
//    return snapshot;
//  }

//  getPodcasts({@required String type}) async {
//    PodcastModel podcastModel = PodcastModel();
//    podcastModel.podcastType = type;
////    podcastModel.get([PodcastModel.], data);
//    print(podcastModel.reload());
//  }

//  getPodcast(String podcastType, String podcastId) async {
//    if (checkUser()) return;
//    PodcastModel podcastModel = PodcastModel();
//    podcastModel.podcastType = podcastType;
//    podcastModel.key = podcastId;
//    await podcastModel.reload();
//    print(podcastModel);
//    return podcastModel;
//  }
