import 'package:church/head_module/database_extender.dart';
import 'package:church/models/story_model.dart';

import '../brain.dart';
import 'auth_extension.dart';

extension Stories on DatabaseModule {
  getStories() async {
    if (checkUser()) return;
    Map<String, StoryModel> stories = {};
    await stories.loadAll(StoryModel());
    return stories;
  }

  sortPopularStories(Map<String, StoryModel> stories) {
    Map<String, int> storiesRating = {};

    for (var p in stories.entries) {
      var rating = p.value.get([StoryModel.ratingField]);
      int totalRating = 0;
      for (var r in rating.values) totalRating += r;
      storiesRating[p.key] = totalRating;
    }

    var sortedKeys = storiesRating.keys.toList(growable: false)
      ..sort((k1, k2) => storiesRating[k2].compareTo(storiesRating[k1]));

    Map<String, StoryModel> sortedMap = {};
    for (var item = 0; item < sortedKeys.length; item++)
      sortedMap[sortedKeys[item]] = stories[sortedKeys[item]];
    return sortedMap;
  }

  updateStoryRating(MapEntry<String, StoryModel> story) async {
    if (checkUser()) return;
    int rating = story.value.get([StoryModel.ratingField])[mindClass.user.uid];
    if (rating != null)
      story.value
          .set([StoryModel.ratingField], {mindClass.user.uid: rating += 1});
    else
      story.value.set([StoryModel.ratingField], {mindClass.user.uid: 1});
    await story.value.upload();
  }
}
