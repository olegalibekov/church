import 'package:church/head_module/database_extender.dart';
import 'package:church/models/bookmarked_church_model.dart';
import 'package:church/models/church/church_model.dart';
import 'package:church/models/church/owner_model.dart';
import 'package:church/models/user/user_model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';

import '../brain.dart';
import 'auth_extension.dart';

extension ChurchEx on DatabaseModule {
//  isBookmarked(String churchId) {
//    var databaseReference = rootReference
//        .child(DatabasePaths.user_data)
//        .child(user.uid)
//        .child(DatabasePaths.churches)
//        .child(churchId)
//        .child(DatabasePaths.bookmark);
//    return databaseReference.onValue;
//  }

  isBookmarked(String churchId) async {
    if (checkUser()) return;
    if (churchId.isEmpty) return;
    UserModel user = UserModel();
    user.key = mindClass.user.uid;
    await user.reload();
    Map response = user.get([
      UserModel.churchesField,
      BookmarkedChurchModel.bookmarkField.key(churchId)
    ]);
    if (response == null) return false;
    return response['bookmark'];
  }

  bookmarkChurch(String churchId) async {
    if (checkUser()) return;
    if (churchId.isEmpty) return;
    UserModel user = UserModel();
    user.key = mindClass.user.uid;
    await user.reload();
    Map response = user[[
      UserModel.churchesField,
      BookmarkedChurchModel.bookmarkField.key(churchId)
    ]];
    if (response == null || response.isEmpty) {
      user[[
        UserModel.churchesField,
        BookmarkedChurchModel.bookmarkField.key(churchId)
      ]] = {'bookmark': true};
    } else {
      user[[
        UserModel.churchesField,
        BookmarkedChurchModel.bookmarkField.key(churchId)
      ]] = {'bookmark': !response['bookmark']};
    }
    user.upload();
  }

  //    set(bool value) {
//      user.set([
//        UserModel.churchesField,
//        BookmarkedChurchModel.bookmarkField.key(churchId)
//      ], value);
//    }
  //    if (response == false || response == null)
//      set(true);
//    else
//      set(false);

//  bookmarkChurch(ChurchModel churchModel) async {
//    if (checkUser()) return;
////    if (churchModel.isIdEmpty()) return;
//    DatabaseReference databaseReference = rootReference
//        .child(DatabasePaths.user_data)
//        .child(user.uid)
//        .child(DatabasePaths.churches)
//        .child(churchModel.key);
//    DatabaseReference currentReference =
//        databaseReference.child(DatabasePaths.bookmark);
//    DataSnapshot snapshot = await currentReference.once();
//    bool isBookmarked = false;
//    if (snapshot.value != null) {
//      isBookmarked = snapshot.value;
//    }
//    await databaseReference
//        .update({"${DatabasePaths.bookmark}": !isBookmarked});
//  }
  getBookmarks() async {
    if (checkUser()) return;
    UserModel user = UserModel();
    user.key = mindClass.user.uid;
    await user.reload();

    Map response =
        user[[UserModel.churchesField, BookmarkedChurchModel.bookmarkField]];
    List bookmarkedChurches = [];
    for (MapEntry entry in response.entries) {
      if (entry.value['bookmark'] == true)
        bookmarkedChurches.add(await this.getChurch(entry.key));
    }
    return bookmarkedChurches;
  }

//
//  getBookmarks() async {
//    if (checkUser()) return;
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.churches);
//    DataSnapshot snapshot = await databaseReference.once();
//    if (snapshot.value == null) {
//      return;
//    }
//    return ChurchModel.fromJson(Map.from(snapshot.value));
//  }

//  List<ChurchModel> formatChurches(dynamic value) {
//    Map<String, dynamic> requests = Map.from(value);
//    List<ChurchModel> trueRequests = requests
//        .map((v1, v2) {
//      var church = ChurchModel.fromJson(Map.from(v2));
//      church.churchId = v1;
//      return MapEntry(v1, church);
//    })
//        .values
//        .toList();
//    return trueRequests;
//  }

  getChurchStream(String churchId) {
    var databaseReference = rootReference
        .child(DatabasePaths.static_data)
        .child(DatabasePaths.churches)
        .child(churchId);
    return databaseReference.onValue;
  }

  getChurches([String query]) async {
    if (checkUser()) return;
    Map<String, ChurchModel> churches = {};
    await churches.loadAll(ChurchModel());
    return churches;
//    print(churchModel.get([ChurchModel.addressField]));
//    print(churchModel.toJson());
//    churchModel[[ChurchModel.nameField]] = "Name";
//    churchModel.key = 'third_church';
////    churchModel.upload();
//    churchModel.set([ChurchModel.nameField], ChurchModel());
//    return churchModel;
  }

  getChurch(String churchId) async {
    if (checkUser()) return;
    ChurchModel churchModel = ChurchModel();
    churchModel.key = churchId;
    await churchModel.reload();
//    OwnerModel ownerModel = OwnerModel();
//    ownerField.get([OwnerModel.nameField]);
//    ownerModel.get(churchModel.get([ChurchModel.ownerField]));
//    churchModel.get([ChurchModel.ownerField])
//    print('1555555 ${ownerModel.get([OwnerModel.phoneNumberField])}');
    return churchModel;
//    print(churchModel.get([ChurchModel.addressField]));
//    print(churchModel.toJson());
//    churchModel[[ChurchModel.nameField]] = "Name";
//    churchModel.key = 'third_church';
////    churchModel.upload();
//    churchModel.set([ChurchModel.nameField], ChurchModel());
//    return churchModel;
  }

  getChurchesStream() {
    if (checkUser()) return;
    var databaseReference = rootReference
        .child(DatabasePaths.static_data)
        .child(DatabasePaths.churches);
    return databaseReference.onValue;
  }

//  getChurches() async {
//    if (checkUser()) return;
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.churches);
//    DataSnapshot snapshot = await databaseReference.once();
//    if (snapshot.value == null) {
//      return;
//    }
//    Map<String, ChurchModel> churches = {};
//    await churches.load(ChurchModel());
//    return churches;
//  }

//  Future<String> createChurch() async {
//    DatabaseReference databaseReference =
//        rootReference.child('static_data').child('all_churches').push();
//    await databaseReference.push().set("empty");
//    return databaseReference.key;
//  }

  createChurch(String churchId) async {
    ChurchModel churchModel = ChurchModel();
    churchModel.key = churchId;
    churchModel.set([ChurchModel.nameField], 'empty');
    await churchModel.upload();
    return churchModel.key;
  }

  updateChurch(String churchId, TString value, String churchData) async {
    if (checkUser()) return;
    if (churchId.isEmpty) throw ErrorHint("id cannot be empty");
    if (value == null) throw ErrorHint("value cannot be empty");
    if (churchData.isEmpty) throw ErrorHint("data cannot be empty");
    ChurchModel churchModel = ChurchModel();
    churchModel.key = churchId;
    await churchModel.set([value], churchData);
    churchModel.upload();
  }

  createChurchOwner(String churchId) async {
    ChurchModel churchModel = ChurchModel();
    churchModel.key = churchId;
    churchModel.set([ChurchModel.ownerField, OwnerModel.nameField], 'empty');
    await churchModel.upload();
    return churchModel.key;
  }

  updateChurchOwner(String churchId, TString value,
      String churchOwnerData) async {
    if (checkUser()) return;
    if (churchId.isEmpty) throw ErrorHint("id cannot be empty");
    if (value == null) throw ErrorHint("value cannot be empty");
    if (churchOwnerData.isEmpty) throw ErrorHint("data cannot be empty");
    ChurchModel churchModel = ChurchModel();
    churchModel.key = churchId;
    await churchModel.set([ChurchModel.ownerField, value], churchOwnerData);
    churchModel.upload();
  }

  //    churchModel.set([], {'1':'111111','2':'222222222','3':'3333333'});
//    await churchModel.upload();
//  updateChurch(String churchId, Map churchData) async {
//    if (checkUser()) return;
//    if (churchId.isEmpty) throw ErrorHint("id cannot be empty");
//    if (churchData.length == 0) throw ErrorHint("data cannot be empty");
//    DatabaseReference databaseReference = rootReference
//        .child('static_data')
//        .child('all_churches')
//        .child(churchId);
//    bool timeout = false;
//    await databaseReference.update(churchData).timeout(Duration(seconds: 10),
//        onTimeout: () {
//          timeout = true;
//        });
//    if (timeout) return "Something went wrong";
//  }

  Future<String> updateBookingPlace(Map<String, dynamic> data) async {
    DatabaseReference databaseReference = rootReference
        .child('static_data')
        .child('all_churches')
        .child('23')
        .child('booking');

    await databaseReference.update({"3": data});
    return databaseReference.key;
  }
}
