import 'package:church/head_module/database_extender.dart';
import 'package:church/models/user/user_info_model.dart';
import 'package:church/models/user/user_model.dart';

import '../brain.dart';
import 'auth_extension.dart';

extension Users on DatabaseModule {
  getUsers([String query, bool renew = false]) async {
    if (checkUser()) return;
    Map<String, UserModel> users = {};
    await users.loadAll(UserModel());
    if (query != null) {
      List filteredList = [];
      users.forEach((key, value) {
        if (value.get([UserModel.userInfoField, UserInfoModel.roleField]) ==
            query) filteredList.add(key);
      });
      return filteredList;
    }
//    print(users);
    return users;
  }

  setUserInfo({String userId, String userRole, String userState}) {
    if (checkUser()) return;
    if (userId.isEmpty) return;
    UserModel user = UserModel();
    user.key = userId;
    print('userRole: $userRole, userState: $userState');
    if (userRole != null)
      user.set([UserModel.userInfoField, UserInfoModel.roleField], userRole);
    if (userState != null)
      user.set([UserModel.userInfoField, UserInfoModel.stateField], userState);
    user.upload();
  }

//  bookmarkChurch(String churchId) async {
//    if (checkUser()) return;
//    if (churchId.isEmpty) return;
//    UserModel user = UserModel();
//    user.key = mindClass.user.uid;
//    await user.reload();
//    Map response = user[[
//      UserModel.churchesField,
//      BookmarkedChurchModel.bookmarkField.key(churchId)
//    ]];
//    if (response == null) {
//      user[[
//        UserModel.churchesField,
//        BookmarkedChurchModel.bookmarkField.key(churchId)
//      ]] = {'bookmark': true};
//    } else {
//      user[[
//        UserModel.churchesField,
//        BookmarkedChurchModel.bookmarkField.key(churchId)
//      ]] = {'bookmark': !response['bookmark']};
//    }
//    user.upload();
//  }

  getUser(String userId) async {
    UserModel userModel = UserModel();
    userModel.key = userId;
    await userModel.reload();
    print(userModel);
    return userModel;
  }
}
