import 'dart:math';

import 'package:church/head_module/database_extender.dart';
import 'package:church/models/tour/tour_model.dart';

import '../brain.dart';
import 'auth_extension.dart';

extension ToursChurch on DatabaseModule {
//  List<Tour> formatTour(dynamic value) {
//    Map<String, dynamic> requestTours = Map.from(value);
//    List<Tour> trueRequests = requestTours
//        .map((v1, v2) {
////        print(v2);
//          var tours = Tour.fromJson(Map.from(v2));
////          print(MapEntry(v1,tours));
//          return MapEntry(v1, tours);
//        })
//        .values
//        .toList();
////    print(trueRequests[1].slides.values.map((e) => e.header));
//    return trueRequests;
//  }

//  tour(String tourId) async {
//    var databaseReference = rootReference
//        .child(DatabasePaths.static_data)
//        .child(DatabasePaths.tours)
//        .child(tourId);
//    DataSnapshot snapshot = await databaseReference.once();
//    return Tour.fromJson(Map.from(snapshot.value));
//  }

  getTours() async {
    if (checkUser()) return;
    Map<String, TourModel> tours = {};
    await tours.loadAll(TourModel());
    return tours;
  }

  getTour(String tourId) async {
    if (checkUser()) return;
    TourModel tourModel = TourModel();
    tourModel.key = tourId;
    await tourModel.reload();
//  print('Printed: ${tourModel[[TourModel.slidesField]]}');
    return tourModel;
  }

  createTour() async {
    if (checkUser()) return;
    TourModel tourModel = TourModel();
    int tourId = Random().nextInt(1000);
    tourModel.key = 'NewTour$tourId';
    tourModel.set([TourModel.nameField], 'empty');
    await tourModel.upload();
    return tourModel.key;
  }
}

//print(Tour.fromJson(Map.from(snapshot.value)).slides.values.map((e) => e.url));