import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../brain.dart';

extension Auth on DatabaseModule {
  Future<FirebaseUser> signInWithGoogle() async {
    print('Start0');
    final GoogleSignInAccount googleUser = await googleSignIn.signIn();
    if (googleUser == null) return null;
    print('Start email ${googleUser.email}');
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    print(googleAuth.accessToken);
    print('Start1');
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    FirebaseUser _user;
    _user = (await auth.signInWithCredential(credential)).user;
    //    if (this.user.isAnonymous)
    //      _user = (await user.linkWithCredential(credential)).user;
    //    else
    //
    if (_user != null) this.user = _user;
    return _user;
  }

  Future<FirebaseUser> signInWithFacebook() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    if (result?.accessToken?.token == null) return null;
    final facebookAuthCred = FacebookAuthProvider.getCredential(
        accessToken: result.accessToken.token);
    final FirebaseUser user =
        (await auth.signInWithCredential(facebookAuthCred)).user;
    if (user == null) return null;
    print("signed in " + user.displayName);
    this.user = user;
    return user;
  }

  signInAnonymously() async {
    return (await auth.signInAnonymously()).user;
  }

  currentUser() async {
    user = (await auth.currentUser());
    return (await auth.currentUser());
  }

  bool checkUser() {
    return user == null;
  }
}
