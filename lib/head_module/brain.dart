import 'dart:collection';

import 'package:audioplayers/audioplayers.dart';
import 'package:church/head_module/database_extensions/podcast_extension.dart';
import 'package:church/head_module/database_extensions/stories_extension.dart';
import 'package:church/models/podcast_model.dart';
import 'package:church/models/story_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:frideos_core/frideos_core.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'database_extensions/auth_extension.dart';

abstract class EventsList {
  static const String went_offline =
      "WENT_OFFLINE"; //когда уходит в оффлайн оьновляется бар. предыщий прогресс обновлется, а новые штуки не обновляются.
  static const String went_online = "WENT_ONLINE";

  //new block for page loading animation
  static const String reset_animation = "RESET_ANIMATION";
  static const String one_more_point_load = "ONE_MORE_POINT_LOAD";
  static const String close_page = "CLOSE_PAGE";
  static const String close_page_ready = "CLOSE_PAGE_READY";

  static const String start_loading = "START_LOADING";
  static const String stop_loading = "STOP_LOADING";
  static const String start_end = "START_END";
  static const String stop_end = "STOP_END";
}

class MindClass with DatabaseModule, PodcastsModule, StoriesModule, KeyPaths {
  static final MindClass _mindClass = new MindClass._internal();

  final eventSteam = StreamedValue<String>();
  final Map<String, dynamic> romData = Map();

  @override
  FirebaseAuth auth;

  MindClass._internal() {
    auth = FirebaseAuth.instance;
    eventSteam.value = "";
  }

  factory MindClass() {
    return _mindClass;
  }

  updateEvent(event) {
    eventSteam.value = event;
  }

  launchBrain([GlobalKey<ScaffoldState> key, BuildContext buildContext]) async {
    await signInAnonymously();
    await currentUser();
    eventSteam.onChange((val) {});

    mindClass.podcastPlayer.setReleaseMode(ReleaseMode.STOP);
    mindClass.storyPlayer.setReleaseMode(ReleaseMode.STOP);
  }

  dispose() {
    eventSteam.dispose();
  }
}

final mindClass = MindClass();

/// Module to work with stories player globally
class StoriesModule {
  final AudioPlayer storyPlayer = AudioPlayer(playerId: 'stories_player');
  ValueNotifier<Map<String, StoryModel>> globalStories = ValueNotifier(Map());
  ValueNotifier<MapEntry<String, StoryModel>> globalLastChosenStory =
      ValueNotifier(MapEntry(null, null));
  ValueNotifier<int> globalStoryCurrentTime = ValueNotifier(0);
  ValueNotifier<double> globalStoryIndicatorPosition = ValueNotifier(0.0);
  ValueNotifier<bool> globalStoryIndPosIsChanging = ValueNotifier(false);
  ValueNotifier<bool> globalStoryIsPlaying = ValueNotifier(false);

  ///For init method of each story card-widget
  bool checkStoryPlaying(String storyKey) {
    if (globalLastChosenStory.value.key != null &&
        globalLastChosenStory.value.key == storyKey &&
        globalLastChosenStory != null &&
        storyPlayer.state == AudioPlayerState.PLAYING) return true;
    return false;
  }

  ///For init method of page with player
  globalStoryListener() {
    storyPlayer.onAudioPositionChanged.listen((Duration currentTime) {
      globalStoryCurrentTime.value = currentTime.inSeconds;
      if (!globalStoryIndPosIsChanging.value)
        globalStoryIndicatorPosition.value = currentTime.inSeconds /
            globalLastChosenStory.value.value.get([StoryModel.durationField]);
    });

    storyPlayer.onPlayerStateChanged.listen((event) {
      if (event == AudioPlayerState.PLAYING)
        globalStoryIsPlaying.value = true;
      else if (event == AudioPlayerState.PAUSED)
        globalStoryIsPlaying.value = false;
    });

    storyPlayer.onPlayerCompletion.listen((event) {
      globalStoryIsPlaying.value = false;
      resetStory();
    });
  }

  ///Change story and reset previous settings
  resetStory([MapEntry<String, StoryModel> story]) {
    if (mindClass.globalPodcastIsPlaying.value) {
      mindClass.globalPodcastIsPlaying.value = false;
      mindClass.resetPodcast();
    }
    storyPlayer.stop();

    if (story != null) {
      mindClass.updateStoryRating(story);
      storyPlayer.setUrl(
          mindClass.globalStories.value[story.key].get([StoryModel.urlField]));
      globalLastChosenStory.value = story;
      globalLastChosenAudio.value = story;
      globalCurrentAudioType.value = DatabasePaths.story;
    }

    globalStoryIndicatorPosition.value = 0.0;
    globalStoryCurrentTime.value = 0;
  }

  ///Player change story to the previous
  storyBackwardTap() {
    MapEntry previousStory = MapEntry(null, null);
    for (var story in mindClass.globalStories.value.entries) {
      if (mindClass.globalLastChosenStory.value.key == story.key) {
        var audioWasPlaying = false;
        if (mindClass.globalStoryIsPlaying.value) audioWasPlaying = true;
        mindClass.resetStory(previousStory);
        if (audioWasPlaying) mindClass.storyPlayer.resume();
        break;
      }
      previousStory = story;
    }
  }

  ///Player change story to the next
  storyForwardTap() {
    MapEntry previousStory = MapEntry(null, null);
    final reversedStories = LinkedHashMap.fromEntries(
        mindClass.globalStories.value.entries.toList().reversed);
    for (var story in reversedStories.entries) {
      if (mindClass.globalLastChosenStory.value.key == story.key) {
        var audioWasPlaying = false;
        if (mindClass.globalStoryIsPlaying.value) audioWasPlaying = true;
        mindClass.resetStory(previousStory);
        if (audioWasPlaying) mindClass.storyPlayer.resume();
        break;
      }
      previousStory = story;
    }
  }
}

mixin KeyPaths {
  String language = 'assets/langs/';

  getL() {
    return language;
  }

  getI() {
    return icons;
  }

  String icons = 'assets/icons/';
}

mixin DatabasePaths {
  static const String churches = "churches";
  static const String static_data = "static_data";
  static const String user_data = "user_data";
  static const String timestamp = "timestamp";
  static const String bookmark = "bookmark";
  static const String podcasts = "podcasts";
  static const String all = "all";
  static const String popular = "popular";
  static const String podcast = "podcast";
  static const String story = "story";
  static const String tours = "tours";

}

///Модуль для аутентификации и простого взаимодействия с базой данных
class DatabaseModule {
  FirebaseAuth auth;
  FirebaseUser user;
  DatabaseReference rootReference = FirebaseDatabase.instance.reference();

  final GoogleSignIn googleSignIn = GoogleSignIn();

  final FirebaseAuth authUser = FirebaseAuth.instance;
}

class DataDrive {
  Map _o = {};

  DataDrive() {}

  Object operator [](Object key) => _o[key];

  void operator []=(Object key, Object value) {
    _o[key] = value;
    print('Key: "$key" updated with the value "$value"');
  }
}

ValueNotifier<MapEntry<String, dynamic>> globalLastChosenAudio =
ValueNotifier(MapEntry(null, null));
ValueNotifier<String> globalCurrentAudioType = ValueNotifier(null);

/// Module to work with podcasts player globally
class PodcastsModule {
  final AudioPlayer podcastPlayer = AudioPlayer(playerId: 'podcasts_player');
  ValueNotifier<Map<String, PodcastModel>> globalPodcasts =
  ValueNotifier(Map());
  ValueNotifier<Map<String, PodcastModel>> globalNewestPodcasts =
  ValueNotifier(Map());
  ValueNotifier<MapEntry<String, PodcastModel>> globalLastChosenPodcast =
  ValueNotifier(MapEntry(null, null));
  ValueNotifier<int> globalPodcastCurrentTime = ValueNotifier(0);
  ValueNotifier<double> globalPodcastIndicatorPosition = ValueNotifier(0.0);
  ValueNotifier<bool> globalPodcastIndPosIsChanging = ValueNotifier(false);
  ValueNotifier<bool> globalPodcastIsPlaying = ValueNotifier(false);

  ///For init method of each podcast card-widget
  bool checkPodcastPlaying(String podcastKey) {
    if (globalLastChosenPodcast.value.key != null &&
        globalLastChosenPodcast.value.key == podcastKey &&
        globalLastChosenPodcast != null &&
        podcastPlayer.state == AudioPlayerState.PLAYING) return true;
    return false;
  }

  ///For init method of page with player
  globalPodcastListener() {
    podcastPlayer.onAudioPositionChanged.listen((Duration currentTime) {
      globalPodcastCurrentTime.value = currentTime.inSeconds;
      if (!globalPodcastIndPosIsChanging.value)
        globalPodcastIndicatorPosition.value = currentTime.inSeconds /
            globalLastChosenPodcast.value.value
                .get([PodcastModel.durationField]);
    });

    podcastPlayer.onPlayerStateChanged.listen((event) {
      if (event == AudioPlayerState.PLAYING)
        globalPodcastIsPlaying.value = true;
      else if (event == AudioPlayerState.PAUSED)
        globalPodcastIsPlaying.value = false;
    });

    podcastPlayer.onPlayerCompletion.listen((event) {
      globalPodcastIsPlaying.value = false;
      resetPodcast();
    });
  }

  ///Change podcast and reset previous settings
  resetPodcast([MapEntry<String, PodcastModel> podcast]) {
    if (mindClass.globalStoryIsPlaying.value) {
      mindClass.globalStoryIsPlaying.value = false;
      mindClass.resetStory();
    }
    podcastPlayer.stop();

    if (podcast != null) {
      mindClass.updatePodcastRating(podcast);
      podcastPlayer.setUrl(mindClass.globalPodcasts.value[podcast.key]
          .get([PodcastModel.urlField]));
      globalLastChosenPodcast.value = podcast;
      globalLastChosenAudio.value = podcast;
      globalCurrentAudioType.value = DatabasePaths.podcast;
    }

    globalPodcastIndicatorPosition.value = 0.0;
    globalPodcastCurrentTime.value = 0;
  }

  ///Player change podcast to the previous
  podcastBackwardTap() {
    MapEntry previousPodcast = MapEntry(null, null);
    for (var podcast in mindClass.globalPodcasts.value.entries) {
      if (mindClass.globalLastChosenPodcast.value.key == podcast.key) {
        var audioWasPlaying = false;
        if (mindClass.globalPodcastIsPlaying.value) audioWasPlaying = true;
        mindClass.resetPodcast(previousPodcast);
        if (audioWasPlaying) mindClass.podcastPlayer.resume();
        break;
      }
      previousPodcast = podcast;
    }
  }

  ///Player change podcast to the next
  podcastForwardTap() {
    MapEntry previousPodcast = MapEntry(null, null);
    final reversedPodcasts = LinkedHashMap.fromEntries(
        mindClass.globalPodcasts.value.entries
            .toList()
            .reversed);
    for (var podcast in reversedPodcasts.entries) {
      if (mindClass.globalLastChosenPodcast.value.key == podcast.key) {
        var audioWasPlaying = false;
        if (mindClass.globalPodcastIsPlaying.value) audioWasPlaying = true;
        mindClass.resetPodcast(previousPodcast);
        if (audioWasPlaying) mindClass.podcastPlayer.resume();
        break;
      }
      previousPodcast = podcast;
    }
  }
}
