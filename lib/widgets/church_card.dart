import 'package:align_positioned/align_positioned.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/head_module/route.dart';
import 'package:church/models/church/church_model.dart';
import 'package:church/utils/app_localizations.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:church/widgets/church_shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../head_module/database_extensions/church_extension.dart';

class ChurchCard extends StatefulWidget {
  final ChurchModel church;

  const ChurchCard({Key key, this.church}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChurchCard();
}

class _ChurchCard extends State<ChurchCard> with AutomaticKeepAliveClientMixin {
  bool isBookmarked;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (widget.church == null) return getShimmerCard(context);
    return GestureDetector(
        onTap: () {
          moveTo(context, 'church_page', widget.church);
        },
        child: getChurchCard(context, widget.church));
  }

  getShimmerCard(BuildContext context) {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: Container(
            height: MediaQuery.of(context).size.height * 0.273,
            child: Stack(overflow: Overflow.visible, children: <Widget>[
              Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      color: Colors.grey[400],
                      borderRadius: BorderRadius.circular(12)),
                  child: ChurchShimmer())
            ])));
  }

  getChurchCard(BuildContext context, ChurchModel church) {
    return Padding(
        padding:
//            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.06),
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.023),
        child: Container(
//          height: MediaQuery
//              .of(context)
//              .size
//              .height * 0.320,
            height: D().w(context) < 550
                ? MediaQuery.of(context).size.height * 0.334
                : 420,
            child: Stack(overflow: Overflow.visible, children: <Widget>[
              Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                    color: Colors.grey[400],
                    borderRadius: BorderRadius.circular(12)),
                child: Opacity(
                  opacity: .9,
                  child: SizedBox(
                    width: double.infinity,
                    height: D().w(context) < 550
                        ? MediaQuery.of(context).size.height * 0.29
                        : 400,
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: double.maxFinite,
//                  imageUrl:
//                      "https://epds.ru/wp-content/uploads/2018/11/Mozhegorov-200x300.jpg",
                      imageUrl: church.get([ChurchModel.urlsField])[0],
//                imageUrl: "https://sib-catholic.ru/wp-content/uploads/2017/05/2017-05-18_14-53-24.png",
                      placeholder: (buildContext, url) => ChurchShimmer(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              Positioned(
                  right: 12,
                  top: 12,
                  child: Row(children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Card(
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0)),
                          child: InkWell(
                            borderRadius: BorderRadius.circular(12),
                            onTap: () {},
                            child: IconButton(
                              onPressed: null,
                              icon: Text("7.2\nкм",
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .copyWith(color: blue05)),
                            ),
                          )),
                    ),
                    Card(
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0)),
                        child: FutureBuilder(
                            future: mindClass.isBookmarked(widget.church.key),
                            builder: (BuildContext context,
                                AsyncSnapshot asyncSnapshot) {
                              if (asyncSnapshot.hasData &&
                                  !asyncSnapshot.hasError &&
                                  asyncSnapshot != null) {
                                isBookmarked = asyncSnapshot.data;
                                return InkWell(
                                    borderRadius: BorderRadius.circular(12),
                                    onTap: () async {
                                      await mindClass
                                          .bookmarkChurch(widget.church.key);
                                      var result = await mindClass
                                          .isBookmarked(widget.church.key);
                                      setState(() => isBookmarked = result);
                                    },
                                    child: IconButton(
                                        onPressed: null,
                                        icon: isBookmarked
                                            ? SvgPicture.asset(
                                                Images.bookmark_filled.i())
                                            : SvgPicture.asset(
                                                Images.bookmark.i()),
                                        tooltip: AppLocalizations.of(context)
                                            .t('tooltips.bookmark')));
                              }
                              return IconButton(
                                  icon: SvgPicture.asset(Images.bookmark.i()),
//                              icon: SvgPicture.asset(
//                                  mindClass.getI() + 'bookmark.svg'),
                                  onPressed: () => mindClass
                                      .bookmarkChurch(widget.church.key),
                                  tooltip: AppLocalizations.of(context)
                                      .t('tooltips.bookmark'));
                            }))
                  ])),
              AlignPositioned(
                  alignment: Alignment.bottomLeft,
                  dx: 15,
                  minChildWidth: D().w(context) * 0.2,
                  maxChildWidth: D().w(context) * 0.65,
//                  touch: Touch.inside,
                  child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0)),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(12.0),
                        onTap: () {
                          moveTo(context, 'church_page', widget.church);
                        },
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child: Text(
                            church.get([ChurchModel.nameField]),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(fontWeight: FontWeight.w500),
                          ),
                        ),
                      ))),
            ])));
  }

  @override
  bool get wantKeepAlive => true;
}
