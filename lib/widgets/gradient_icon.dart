import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

class GradientIcon extends StatelessWidget {
  final icon;
  final List<Color> gradientColors;

  const GradientIcon({@required this.icon, @required this.gradientColors});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
        blendMode: BlendMode.srcIn,
        shaderCallback: (Rect bounds) {
          return ui.Gradient.linear(
            Offset(4.0, 24.0),
            Offset(20.0, 4.0),
            gradientColors,
          );
        },
        child: SvgPicture.asset(icon));
  }
}
