import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';

class GradientContainer extends StatelessWidget {
  final Container container;
  final List<Color> gradientColors;

  GradientContainer({@required this.container, @required this.gradientColors});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
        blendMode: BlendMode.srcIn,
        shaderCallback: (Rect bounds) {
          return ui.Gradient.linear(
              Offset(4.0, 24.0), Offset(20.0, 4.0), gradientColors);
        },
        child: container);
  }
}
