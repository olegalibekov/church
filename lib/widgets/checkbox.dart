//// Copyright 2014 The Flutter Authors. All rights reserved.
//// Use of this source code is governed by a BSD-style license that can be
//// found in the LICENSE file.
//
//import 'dart:math' as math;
//
//import 'package:flutter/gestures.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';
//
///// A material design checkbox.
/////
///// The checkbox itself does not maintain any state. Instead, when the state of
///// the checkbox changes, the widget calls the [onChanged] callback. Most
///// widgets that use a checkbox will listen for the [onChanged] callback and
///// rebuild the checkbox with a new [value] to update the visual appearance of
///// the checkbox.
/////
///// The checkbox can optionally display three values - true, false, and null -
///// if [tristate] is true. When [value] is null a dash is displayed. By default
///// [tristate] is false and the checkbox's [value] must be true or false.
/////
///// Requires one of its ancestors to be a [Material] widget.
/////
///// See also:
/////
/////  * [CheckboxListTile], which combines this widget with a [ListTile] so that
/////    you can give the checkbox a label.
/////  * [Switch], a widget with semantics similar to [CheckboxS].
/////  * [Radio], for selecting among a set of explicit values.
/////  * [Slider], for selecting a value in a range.
/////  * <https://material.io/design/components/selection-controls.html#checkboxes>
/////  * <https://material.io/design/components/lists.html#types>
//class CheckboxS extends StatefulWidget {
//  /// Creates a material design checkbox.
//  ///
//  /// The checkbox itself does not maintain any state. Instead, when the state of
//  /// the checkbox changes, the widget calls the [onChanged] callback. Most
//  /// widgets that use a checkbox will listen for the [onChanged] callback and
//  /// rebuild the checkbox with a new [value] to update the visual appearance of
//  /// the checkbox.
//  ///
//  /// The following arguments are required:
//  ///
//  /// * [value], which determines whether the checkbox is checked. The [value]
//  ///   can only be null if [tristate] is true.
//  /// * [onChanged], which is called when the value of the checkbox should
//  ///   change. It can be set to null to disable the checkbox.
//  ///
//  /// The values of [tristate] and [autofocus] must not be null.
//  const CheckboxS({
//    Key key,
//    @required this.value,
//    this.tristate = false,
//    @required this.onChanged,
//    this.activeColor,
//    this.checkColor,
//    this.focusColor,
//    this.hoverColor,
//    this.materialTapTargetSize,
//    this.visualDensity,
//    this.focusNode,
//    this.autofocus = false,
//  })  : assert(tristate != null),
//        assert(tristate || value != null),
//        assert(autofocus != null),
//        super(key: key);
//
//  /// Whether this checkbox is checked.
//  ///
//  /// This property must not be null.
//  final bool value;
//
//  /// Called when the value of the checkbox should change.
//  ///
//  /// The checkbox passes the new value to the callback but does not actually
//  /// change state until the parent widget rebuilds the checkbox with the new
//  /// value.
//  ///
//  /// If this callback is null, the checkbox will be displayed as disabled
//  /// and will not respond to input gestures.
//  ///
//  /// When the checkbox is tapped, if [tristate] is false (the default) then
//  /// the [onChanged] callback will be applied to `!value`. If [tristate] is
//  /// true this callback cycle from false to true to null.
//  ///false
//  /// The callback provided to [onChanged] should update the state of the parent
//  /// [StatefulWidget] using the [State.setState] method, so that the parent
//  /// gets rebuilt; for example:
//  ///
//  /// ```dart
//  /// Checkbox(
//  ///   value: _throwShotAway,
//  ///   onChanged: (bool newValue) {
//  ///     setState(() {
//  ///       _throwShotAway = newValue;
//  ///     });
//  ///   },
//  /// )
//  /// ```
//  final ValueChanged<bool> onChanged;
//
//  /// The color to use when this checkbox is checked.
//  ///
//  /// Defaults to [ThemeData.toggleableActiveColor].
//  final Color activeColor;
//
//  /// The color to use for the check icon when this checkbox is checked.
//  ///
//  /// Defaults to Color(0xFFFFFFFF)
//  final Color checkColor;
//
//  /// If true the checkbox's [value] can be true, false, or null.
//  ///
//  /// Checkbox displays a dash when its value is null.
//  ///
//  /// When a tri-state checkbox ([tristate] is true) is tapped, its [onChanged]
//  /// callback will be applied to true if the current value is false, to null if
//  /// value is true, and to false if value is null (i.e. it cycles through false
//  /// => true => null => false when tapped).
//  ///
//  /// If tristate is false (the default), [value] must not be null.
//  final bool tristate;
//
//  /// Configures the minimum size of the tap target.
//  ///
//  /// Defaults to [ThemeData.materialTapTargetSize].
//  ///
//  /// See also:
//  ///
//  ///  * [MaterialTapTargetSize], for a description of how this affects tap targets.
//  final MaterialTapTargetSize materialTapTargetSize;
//
//  /// Defines how compact the checkbox's layout will be.
//  ///
//  /// {@macro flutter.material.themedata.visualDensity}
//  ///
//  /// See also:
//  ///
//  ///  * [ThemeData.visualDensity], which specifies the [density] for all widgets
//  ///    within a [Theme].
//  final VisualDensity visualDensity;
//
//  /// The color for the checkbox's [Material] when it has the input focus.
//  final Color focusColor;
//
//  /// The color for the checkbox's [Material] when a pointer is hovering over it.
//  final Color hoverColor;
//
//  /// {@macro flutter.widgets.Focus.focusNode}
//  final FocusNode focusNode;
//
//  /// {@macro flutter.widgets.Focus.autofocus}
//  final bool autofocus;
//
//  /// The width of a checkbox widget.
//  static const double width = 18.0;
//
//  @override
//  _CheckboxSState createState() => _CheckboxSState();
//}
//
//class _CheckboxSState extends State<CheckboxS> with TickerProviderStateMixin {
//  bool get enabled => widget.onChanged != null;
//  Map<LocalKey, ActionFactory> _actionMap;
//
//  @override
//  void initState() {
//    super.initState();
//    _actionMap = <LocalKey, ActionFactory>{
//      ActivateAction.key: _createAction,
//    };
//  }
//
//  void _actionHandler(FocusNode node, Intent intent) {
//    if (widget.onChanged != null) {
//      switch (widget.value) {
//        case false:
//          widget.onChanged(true);
//          break;
//        case true:
//          widget.onChanged(widget.tristate ? null : false);
//          break;
//        default: // case null:
//          widget.onChanged(false);
//          break;
//      }
//    }
//    final RenderObject renderObject = node.context.findRenderObject();
//    renderObject.sendSemanticsEvent(const TapSemanticEvent());
//  }
//
//  Action _createAction() {
//    return CallbackAction(
//      ActivateAction.key,
//      onInvoke: _actionHandler,
//    );
//  }
//
//  bool _focused = false;
//
//  void _handleFocusHighlightChanged(bool focused) {
//    if (focused != _focused) {
//      setState(() {
//        _focused = focused;
//      });
//    }
//  }
//
//  bool _hovering = false;
//
//  void _handleHoverChanged(bool hovering) {
//    if (hovering != _hovering) {
//      setState(() {
//        _hovering = hovering;
//      });
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    assert(debugCheckHasMaterial(context));
//    final ThemeData themeData = Theme.of(context);
//    Size size;
//    switch (widget.materialTapTargetSize ?? themeData.materialTapTargetSize) {
//      case MaterialTapTargetSize.padded:
//        size = const Size(
//            2 * kRadialReactionRadius + 8.0, 2 * kRadialReactionRadius + 8.0);
//        break;
//      case MaterialTapTargetSize.shrinkWrap:
//        size = const Size(2 * kRadialReactionRadius, 2 * kRadialReactionRadius);
//        break;
//    }
//    size +=
//        (widget.visualDensity ?? themeData.visualDensity).baseSizeAdjustment;
//    final BoxConstraints additionalConstraints = BoxConstraints.tight(size);
//    return FocusableActionDetector(
//      actions: _actionMap,
//      focusNode: widget.focusNode,
//      autofocus: widget.autofocus,
//      enabled: enabled,
//      onShowFocusHighlight: _handleFocusHighlightChanged,
//      onShowHoverHighlight: _handleHoverChanged,
//      child: Builder(
//        builder: (BuildContext context) {
//          return _CheckboxRenderObjectWidget(
//            value: widget.value,
//            tristate: widget.tristate,
//            activeColor: widget.activeColor ?? themeData.toggleableActiveColor,
//            checkColor: widget.checkColor ?? const Color(0xFFFFFFFF),
//            inactiveColor: enabled
//                ? themeData.unselectedWidgetColor
//                : themeData.disabledColor,
//            focusColor: widget.focusColor ?? themeData.focusColor,
//            hoverColor: widget.hoverColor ?? themeData.hoverColor,
//            onChanged: widget.onChanged,
//            additionalConstraints: additionalConstraints,
//            vsync: this,
//            hasFocus: _focused,
//            hovering: _hovering,
//          );
//        },
//      ),
//    );
//  }
//}
//
//class _CheckboxRenderObjectWidget extends LeafRenderObjectWidget {
//  const _CheckboxRenderObjectWidget({
//    Key key,
//    @required this.value,
//    @required this.tristate,
//    @required this.activeColor,
//    @required this.checkColor,
//    @required this.inactiveColor,
//    @required this.focusColor,
//    @required this.hoverColor,
//    @required this.onChanged,
//    @required this.vsync,
//    @required this.additionalConstraints,
//    @required this.hasFocus,
//    @required this.hovering,
//  })  : assert(tristate != null),
//        assert(tristate || value != null),
//        assert(activeColor != null),
//        assert(inactiveColor != null),
//        assert(vsync != null),
//        super(key: key);
//
//  final bool value;
//  final bool tristate;
//  final bool hasFocus;
//  final bool hovering;
//  final Color activeColor;
//  final Color checkColor;
//  final Color inactiveColor;
//  final Color focusColor;
//  final Color hoverColor;
//  final ValueChanged<bool> onChanged;
//  final TickerProvider vsync;
//  final BoxConstraints additionalConstraints;
//
//  @override
//  _RenderCheckbox createRenderObject(BuildContext context) => _RenderCheckbox(
//        value: value,
//        tristate: tristate,
//        activeColor: activeColor,
//        checkColor: checkColor,
//        inactiveColor: inactiveColor,
//        focusColor: focusColor,
//        hoverColor: hoverColor,
//        onChanged: onChanged,
//        vsync: vsync,
//        additionalConstraints: additionalConstraints,
//        hasFocus: hasFocus,
//        hovering: hovering,
//      );
//
//  @override
//  void updateRenderObject(BuildContext context, _RenderCheckbox renderObject) {
//    renderObject
//      ..value = value
//      ..tristate = tristate
//      ..activeColor = activeColor
//      ..checkColor = checkColor
//      ..inactiveColor = inactiveColor
//      ..focusColor = focusColor
//      ..hoverColor = hoverColor
//      ..onChanged = onChanged
//      ..additionalConstraints = additionalConstraints
//      ..vsync = vsync
//      ..hasFocus = hasFocus
//      ..hovering = hovering;
//  }
//}
//
//const double _kEdgeSize = 20;
//const Radius _kEdgeRadius = Radius.circular(6.0);
//const double _kStrokeWidth = 2.5;
//
//class _RenderCheckbox extends RenderToggleable {
//  _RenderCheckbox({
//    bool value,
//    bool tristate,
//    Color activeColor,
//    this.checkColor,
//    Color inactiveColor,
//    Color focusColor,
//    Color hoverColor,
//    BoxConstraints additionalConstraints,
//    ValueChanged<bool> onChanged,
//    bool hasFocus,
//    bool hovering,
//    @required TickerProvider vsync,
//  })  : _oldValue = value,
//        super(
//          value: value,
//          tristate: tristate,
//          activeColor: activeColor,
//          inactiveColor: inactiveColor,
//          focusColor: focusColor,
//          hoverColor: hoverColor,
//          onChanged: onChanged,
//          additionalConstraints: additionalConstraints,
//          vsync: vsync,
//          hasFocus: hasFocus,
//          hovering: hovering,
//        );
//
//  bool _oldValue;
//  Color checkColor;
//
//  @override
//  set value(bool newValue) {
//    if (newValue == value) return;
//    _oldValue = value;
//    super.value = newValue;
//  }
//
//  @override
//  void describeSemanticsConfiguration(SemanticsConfiguration config) {
//    super.describeSemanticsConfiguration(config);
//    config.isChecked = value == true;
//  }
//
//  // The square outer bounds of the checkbox at t, with the specified origin.
//  // At t == 0.0, the outer rect's size is _kEdgeSize (Checkbox.width)
//  // At t == 0.5, .. is _kEdgeSize - _kStrokeWidth
//  // At t == 1.0, .. is _kEdgeSize
//  RRect _outerRectAt(Offset origin, double t) {
//    final double inset = 1.0 - (t - 0.5).abs() * 2.0;
//    final double size = _kEdgeSize - inset * _kStrokeWidth;
//    final Rect rect =
//        Rect.fromLTWH(origin.dx + inset, origin.dy + inset, size, size);
//    return RRect.fromRectAndRadius(rect, _kEdgeRadius);
//  }
//
//  // The checkbox's border color if value == false, or its fill color when
//  // value == true or null.
//  Color _colorAt(double t) {
//    // As t goes from 0.0 to 0.25, animate from the inactiveColor to activeColor.
//    return onChanged == null
//        ? inactiveColor
//        : (t >= 0.25
//            ? activeColor
//            : Color.lerp(inactiveColor, activeColor, t * 4.0));
//  }
//
//  // White stroke used to paint the check and dash.
//  Paint _createStrokePaint() {
//    return Paint()
//      ..color = checkColor
//      ..style = PaintingStyle.stroke
//      ..strokeWidth = _kStrokeWidth;
//  }
//
//  void _drawBorder(Canvas canvas, RRect outer, double t, Paint paint) {
//    assert(t >= 0.0 && t <= 0.5);
//    final double size = outer.width;
//    // As t goes from 0.0 to 1.0, gradually fill the outer RRect.
//    final RRect inner =
//        outer.deflate(math.min(size / 2.0, _kStrokeWidth + size * t));
//    canvas.drawDRRect(outer, inner, paint);
//  }
//
//  void _drawCheck(Canvas canvas, Offset origin, double t, Paint paint) {
//    assert(t >= 0.0 && t <= 1.0);
//    // As t goes from 0.0 to 1.0, animate the two check mark strokes from the
//    // short side to the long side.
//    final Path path = Path();
//    const Offset start = Offset(_kEdgeSize * 0.14, _kEdgeSize * 0.45);
//    const Offset mid = Offset(_kEdgeSize * 0.45, _kEdgeSize * 0.7);
//    const Offset end = Offset(_kEdgeSize * 0.85, _kEdgeSize * 0.23);
//    if (t < 0.5) {
//      final double strokeT = t * 2.0;
//      final Offset drawMid = Offset.lerp(start, mid, strokeT);
//      path.moveTo(origin.dx + start.dx, origin.dy + start.dy);
//      path.lineTo(origin.dx + drawMid.dx, origin.dy + drawMid.dy);
//    } else {
//      final double strokeT = (t - 0.58) * 2.0;
//      final Offset drawEnd = Offset.lerp(mid, end, strokeT);
//      path.moveTo(origin.dx + start.dx * 1.9, origin.dy + start.dy * 1);
//      path.lineTo(origin.dx + mid.dx, origin.dy + mid.dy);
//      path.lineTo(origin.dx + drawEnd.dx, origin.dy + drawEnd.dy);
//    }
//    canvas.drawPath(path, paint);
//  }
//
//  void _drawDash(Canvas canvas, Offset origin, double t, Paint paint) {
//    assert(t >= 0.0 && t <= 1.0);
//    // As t goes from 0.0 to 1.0, animate the horizontal line from the
//    // mid point outwards.
//    const Offset start = Offset(_kEdgeSize * 0.2, _kEdgeSize * 0.5);
//    const Offset mid = Offset(_kEdgeSize * 0.5, _kEdgeSize * 0.5);
//    const Offset end = Offset(_kEdgeSize * 0.8, _kEdgeSize * 0.5);
//    final Offset drawStart = Offset.lerp(start, mid, 1.0 - t);
//    final Offset drawEnd = Offset.lerp(mid, end, t);
//    canvas.drawLine(origin + drawStart, origin + drawEnd, paint);
//  }
//
//  @override
//  void paint(PaintingContext context, Offset offset) {
//    final Canvas canvas = context.canvas;
//    paintRadialReaction(canvas, offset, size.center(Offset.zero));
//
//    final Paint strokePaint = _createStrokePaint();
//    final Offset origin =
//        offset + (size / 2.0 - const Size.square(_kEdgeSize) / 2.0 as Offset);
//    final AnimationStatus status = position.status;
//    final double tNormalized =
//        status == AnimationStatus.forward || status == AnimationStatus.completed
//            ? position.value
//            : 1.0 - position.value;
//
//    // Four cases: false to null, false to true, null to false, true to false
//    if (_oldValue == false || value == false) {
//      final double t = value == false ? 1.0 - tNormalized : tNormalized;
//      final RRect outer = _outerRectAt(origin, t);
//      final Paint paint = Paint()..color = _colorAt(t);
//
//      if (t <= 0.5) {
//        _drawBorder(canvas, outer, t, paint);
//      } else {
//        canvas.drawRRect(outer, paint);
//
//        final double tShrink = (t - 0.5) * 2.0;
//        if (_oldValue == null || value == null)
//          _drawDash(canvas, origin, tShrink, strokePaint);
//        else
//          _drawCheck(canvas, origin, tShrink, strokePaint);
//      }
//    } else {
//      // Two cases: null to true, true to null
//      final RRect outer = _outerRectAt(origin, 1.0);
//      final Paint paint = Paint()..color = _colorAt(1.0);
//      canvas.drawRRect(outer, paint);
//
//      if (tNormalized <= 0.5) {
//        final double tShrink = 1.0 - tNormalized * 2.0;
//        if (_oldValue == true)
//          _drawCheck(canvas, origin, tShrink, strokePaint);
//        else
//          _drawDash(canvas, origin, tShrink, strokePaint);
//      } else {
//        final double tExpand = (tNormalized - 0.5) * 2.0;
//        if (value == true)
//          _drawCheck(canvas, origin, tExpand, strokePaint);
//        else
//          _drawDash(canvas, origin, tExpand, strokePaint);
//      }
//    }
//  }
//}
//
//// Duration of the animation that moves the toggle from one state to another.
//const Duration _kToggleDuration = Duration(milliseconds: 200);
//
//// Radius of the radial reaction over time.
//final Animatable<double> _kRadialReactionRadiusTween =
//    Tween<double>(begin: 0.0, end: kRadialReactionRadius);
//
//// Duration of the fade animation for the reaction when focus and hover occur.
//const Duration _kReactionFadeDuration = Duration(milliseconds: 50);
//
///// A base class for material style toggleable controls with toggle animations.
/////
///// This class handles storing the current value, dispatching ValueChanged on a
///// tap gesture and driving a changed animation. Subclasses are responsible for
///// painting.
//abstract class RenderToggleable extends RenderConstrainedBox {
//  /// Creates a toggleable render object.
//  ///
//  /// The [activeColor], and [inactiveColor] arguments must not be
//  /// null. The [value] can only be null if tristate is true.
//  RenderToggleable({
//    @required bool value,
//    bool tristate = false,
//    @required Color activeColor,
//    @required Color inactiveColor,
//    Color hoverColor,
//    Color focusColor,
//    ValueChanged<bool> onChanged,
//    BoxConstraints additionalConstraints,
//    @required TickerProvider vsync,
//    bool hasFocus = false,
//    bool hovering = false,
//  })  : assert(tristate != null),
//        assert(tristate || value != null),
//        assert(activeColor != null),
//        assert(inactiveColor != null),
//        assert(vsync != null),
//        _value = value,
//        _tristate = tristate,
//        _activeColor = activeColor,
//        _inactiveColor = inactiveColor,
//        _hoverColor = hoverColor ?? activeColor.withAlpha(kRadialReactionAlpha),
//        _focusColor = focusColor ?? activeColor.withAlpha(kRadialReactionAlpha),
//        _onChanged = onChanged,
//        _hasFocus = hasFocus,
//        _hovering = hovering,
//        _vsync = vsync,
//        super(additionalConstraints: additionalConstraints) {
//    _tap = TapGestureRecognizer()
//      ..onTapDown = _handleTapDown
//      ..onTap = _handleTap
//      ..onTapUp = _handleTapUp
//      ..onTapCancel = _handleTapCancel;
//    _positionController = AnimationController(
//      duration: _kToggleDuration,
//      value: value == false ? 0.0 : 1.0,
//      vsync: vsync,
//    );
//    _position = CurvedAnimation(
//      parent: _positionController,
//      curve: Curves.linear,
//    )
//      ..addListener(markNeedsPaint)
//      ..addStatusListener(_handlePositionStateChanged);
//    _reactionController = AnimationController(
//      duration: kRadialReactionDuration,
//      vsync: vsync,
//    );
//    _reaction = CurvedAnimation(
//      parent: _reactionController,
//      curve: Curves.fastOutSlowIn,
//    )..addListener(markNeedsPaint);
//    _reactionHoverFadeController = AnimationController(
//      duration: _kReactionFadeDuration,
//      value: hovering || hasFocus ? 1.0 : 0.0,
//      vsync: vsync,
//    );
//    _reactionHoverFade = CurvedAnimation(
//      parent: _reactionHoverFadeController,
//      curve: Curves.fastOutSlowIn,
//    )..addListener(markNeedsPaint);
//    _reactionFocusFadeController = AnimationController(
//      duration: _kReactionFadeDuration,
//      value: hovering || hasFocus ? 1.0 : 0.0,
//      vsync: vsync,
//    );
//    _reactionFocusFade = CurvedAnimation(
//      parent: _reactionFocusFadeController,
//      curve: Curves.fastOutSlowIn,
//    )..addListener(markNeedsPaint);
//  }
//
//  /// Used by subclasses to manipulate the visual value of the control.
//  ///
//  /// Some controls respond to user input by updating their visual value. For
//  /// example, the thumb of a switch moves from one position to another when
//  /// dragged. These controls manipulate this animation controller to update
//  /// their [position] and eventually trigger an [onChanged] callback when the
//  /// animation reaches either 0.0 or 1.0.
//  @protected
//  AnimationController get positionController => _positionController;
//  AnimationController _positionController;
//
//  /// The visual value of the control.
//  ///
//  /// When the control is inactive, the [value] is false and this animation has
//  /// the value 0.0. When the control is active, the value either true or tristate
//  /// is true and the value is null. When the control is active the animation
//  /// has a value of 1.0. When the control is changing from inactive
//  /// to active (or vice versa), [value] is the target value and this animation
//  /// gradually updates from 0.0 to 1.0 (or vice versa).
//  CurvedAnimation get position => _position;
//  CurvedAnimation _position;
//
//  /// Used by subclasses to control the radial reaction animation.
//  ///
//  /// Some controls have a radial ink reaction to user input. This animation
//  /// controller can be used to start or stop these ink reactions.
//  ///
//  /// Subclasses should call [paintRadialReaction] to actually paint the radial
//  /// reaction.
//  @protected
//  AnimationController get reactionController => _reactionController;
//  AnimationController _reactionController;
//  Animation<double> _reaction;
//
//  /// Used by subclasses to control the radial reaction's opacity animation for
//  /// [hasFocus] changes.
//  ///
//  /// Some controls have a radial ink reaction to focus. This animation
//  /// controller can be used to start or stop these ink reaction fade-ins and
//  /// fade-outs.
//  ///
//  /// Subclasses should call [paintRadialReaction] to actually paint the radial
//  /// reaction.
//  @protected
//  AnimationController get reactionFocusFadeController =>
//      _reactionFocusFadeController;
//  AnimationController _reactionFocusFadeController;
//  Animation<double> _reactionFocusFade;
//
//  /// Used by subclasses to control the radial reaction's opacity animation for
//  /// [hovering] changes.
//  ///
//  /// Some controls have a radial ink reaction to pointer hover. This animation
//  /// controller can be used to start or stop these ink reaction fade-ins and
//  /// fade-outs.
//  ///
//  /// Subclasses should call [paintRadialReaction] to actually paint the radial
//  /// reaction.
//  @protected
//  AnimationController get reactionHoverFadeController =>
//      _reactionHoverFadeController;
//  AnimationController _reactionHoverFadeController;
//  Animation<double> _reactionHoverFade;
//
//  /// True if this toggleable has the input focus.
//  bool get hasFocus => _hasFocus;
//  bool _hasFocus;
//
//  set hasFocus(bool value) {
//    assert(value != null);
//    if (value == _hasFocus) return;
//    _hasFocus = value;
//    if (_hasFocus) {
//      _reactionFocusFadeController.forward();
//    } else {
//      _reactionFocusFadeController.reverse();
//    }
//    markNeedsPaint();
//  }
//
//  /// True if this toggleable is being hovered over by a pointer.
//  bool get hovering => _hovering;
//  bool _hovering;
//
//  set hovering(bool value) {
//    assert(value != null);
//    if (value == _hovering) return;
//    _hovering = value;
//    if (_hovering) {
//      _reactionHoverFadeController.forward();
//    } else {
//      _reactionHoverFadeController.reverse();
//    }
//    markNeedsPaint();
//  }
//
//  /// The [TickerProvider] for the [AnimationController]s that run the animations.
//  TickerProvider get vsync => _vsync;
//  TickerProvider _vsync;
//
//  set vsync(TickerProvider value) {
//    assert(value != null);
//    if (value == _vsync) return;
//    _vsync = value;
//    positionController.resync(vsync);
//    reactionController.resync(vsync);
//  }
//
//  /// False if this control is "inactive" (not checked, off, or unselected).
//  ///
//  /// If value is true then the control "active" (checked, on, or selected). If
//  /// tristate is true and value is null, then the control is considered to be
//  /// in its third or "indeterminate" state.
//  ///
//  /// When the value changes, this object starts the [positionController] and
//  /// [position] animations to animate the visual appearance of the control to
//  /// the new value.
//  bool get value => _value;
//  bool _value;
//
//  set value(bool value) {
//    assert(tristate || value != null);
//    if (value == _value) return;
//    _value = value;
//    markNeedsSemanticsUpdate();
//    _position
//      ..curve = Curves.easeIn
//      ..reverseCurve = Curves.easeOut;
//    if (tristate) {
//      switch (_positionController.status) {
//        case AnimationStatus.forward:
//        case AnimationStatus.completed:
//          _positionController.reverse();
//          break;
//        default:
//          _positionController.forward();
//      }
//    } else {
//      if (value == true)
//        _positionController.forward();
//      else
//        _positionController.reverse();
//    }
//  }
//
//  /// If true, [value] can be true, false, or null, otherwise [value] must
//  /// be true or false.
//  ///
//  /// When [tristate] is true and [value] is null, then the control is
//  /// considered to be in its third or "indeterminate" state.
//  bool get tristate => _tristate;
//  bool _tristate;
//
//  set tristate(bool value) {
//    assert(tristate != null);
//    if (value == _tristate) return;
//    _tristate = value;
//    markNeedsSemanticsUpdate();
//  }
//
//  /// The color that should be used in the active state (i.e., when [value] is true).
//  ///
//  /// For example, a checkbox should use this color when checked.
//  Color get activeColor => _activeColor;
//  Color _activeColor;
//
//  set activeColor(Color value) {
//    assert(value != null);
//    if (value == _activeColor) return;
//    _activeColor = value;
//    markNeedsPaint();
//  }
//
//  /// The color that should be used in the inactive state (i.e., when [value] is false).
//  ///
//  /// For example, a checkbox should use this color when unchecked.
//  Color get inactiveColor => _inactiveColor;
//  Color _inactiveColor;
//
//  set inactiveColor(Color value) {
//    assert(value != null);
//    if (value == _inactiveColor) return;
//    _inactiveColor = value;
//    markNeedsPaint();
//  }
//
//  /// The color that should be used for the reaction when [hovering] is true.
//  ///
//  /// Used when the toggleable needs to change the reaction color/transparency,
//  /// when it is being hovered over.
//  ///
//  /// Defaults to the [activeColor] at alpha [kRadialReactionAlpha].
//  Color get hoverColor => _hoverColor;
//  Color _hoverColor;
//
//  set hoverColor(Color value) {
//    assert(value != null);
//    if (value == _hoverColor) return;
//    _hoverColor = value;
//    markNeedsPaint();
//  }
//
//  /// The color that should be used for the reaction when [hasFocus] is true.
//  ///
//  /// Used when the toggleable needs to change the reaction color/transparency,
//  /// when it has focus.
//  ///
//  /// Defaults to the [activeColor] at alpha [kRadialReactionAlpha].
//  Color get focusColor => _focusColor;
//  Color _focusColor;
//
//  set focusColor(Color value) {
//    assert(value != null);
//    if (value == _focusColor) return;
//    _focusColor = value;
//    markNeedsPaint();
//  }
//
//  /// The color that should be used for the reaction when drawn.
//  ///
//  /// Used when the toggleable needs to change the reaction color/transparency
//  /// that is displayed when the toggleable is toggled by a tap.
//  ///
//  /// Defaults to the [activeColor] at alpha [kRadialReactionAlpha].
//  Color get reactionColor => _reactionColor;
//  Color _reactionColor;
//
//  set reactionColor(Color value) {
//    assert(value != null);
//    if (value == _reactionColor) return;
//    _reactionColor = value;
//    markNeedsPaint();
//  }
//
//  /// Called when the control changes value.
//  ///
//  /// If the control is tapped, [onChanged] is called immediately with the new
//  /// value. If the control changes value due to an animation (see
//  /// [positionController]), the callback is called when the animation
//  /// completes.
//  ///
//  /// The control is considered interactive (see [isInteractive]) if this
//  /// callback is non-null. If the callback is null, then the control is
//  /// disabled, and non-interactive. A disabled checkbox, for example, is
//  /// displayed using a grey color and its value cannot be changed.
//  ValueChanged<bool> get onChanged => _onChanged;
//  ValueChanged<bool> _onChanged;
//
//  set onChanged(ValueChanged<bool> value) {
//    if (value == _onChanged) return;
//    final bool wasInteractive = isInteractive;
//    _onChanged = value;
//    if (wasInteractive != isInteractive) {
//      markNeedsPaint();
//      markNeedsSemanticsUpdate();
//    }
//  }
//
//  /// Whether [value] of this control can be changed by user interaction.
//  ///
//  /// The control is considered interactive if the [onChanged] callback is
//  /// non-null. If the callback is null, then the control is disabled, and
//  /// non-interactive. A disabled checkbox, for example, is displayed using a
//  /// grey color and its value cannot be changed.
//  bool get isInteractive => onChanged != null;
//
//  TapGestureRecognizer _tap;
//  Offset _downPosition;
//
//  @override
//  void attach(PipelineOwner owner) {
//    super.attach(owner);
//    if (value == false)
//      _positionController.reverse();
//    else
//      _positionController.forward();
//    if (isInteractive) {
//      switch (_reactionController.status) {
//        case AnimationStatus.forward:
//          _reactionController.forward();
//          break;
//        case AnimationStatus.reverse:
//          _reactionController.reverse();
//          break;
//        case AnimationStatus.dismissed:
//        case AnimationStatus.completed:
//          // nothing to do
//          break;
//      }
//    }
//  }
//
//  @override
//  void detach() {
//    _positionController.stop();
//    _reactionController.stop();
//    super.detach();
//  }
//
//  // Handle the case where the _positionController's value changes because
//  // the user dragged the toggleable: we may reach 0.0 or 1.0 without
//  // seeing a tap. The Switch does this.
//  void _handlePositionStateChanged(AnimationStatus status) {
//    if (isInteractive && !tristate) {
//      if (status == AnimationStatus.completed && _value == false) {
//        onChanged(true);
//      } else if (status == AnimationStatus.dismissed && _value != false) {
//        onChanged(false);
//      }
//    }
//  }
//
//  void _handleTapDown(TapDownDetails details) {
//    if (isInteractive) {
//      _downPosition = globalToLocal(details.globalPosition);
//      _reactionController.forward();
//    }
//  }
//
//  void _handleTap() {
//    if (!isInteractive) return;
//    switch (value) {
//      case false:
//        onChanged(true);
//        break;
//      case true:
//        onChanged(tristate ? null : false);
//        break;
//      default: // case null:
//        onChanged(false);
//        break;
//    }
//    sendSemanticsEvent(const TapSemanticEvent());
//  }
//
//  void _handleTapUp(TapUpDetails details) {
//    _downPosition = null;
//    if (isInteractive) _reactionController.reverse();
//  }
//
//  void _handleTapCancel() {
//    _downPosition = null;
//    if (isInteractive) _reactionController.reverse();
//  }
//
//  @override
//  bool hitTestSelf(Offset position) => true;
//
//  @override
//  void handleEvent(PointerEvent event, BoxHitTestEntry entry) {
//    assert(debugHandleEvent(event, entry));
//    if (event is PointerDownEvent && isInteractive) _tap.addPointer(event);
//  }
//
//  /// Used by subclasses to paint the radial ink reaction for this control.
//  ///
//  /// The reaction is painted on the given canvas at the given offset. The
//  /// origin is the center point of the reaction (usually distinct from the
//  /// point at which the user interacted with the control, which is handled
//  /// automatically).
//  void paintRadialReaction(Canvas canvas, Offset offset, Offset origin) {
//    if (!_reaction.isDismissed ||
//        !_reactionFocusFade.isDismissed ||
//        !_reactionHoverFade.isDismissed) {
//      final Paint reactionPaint = Paint()
//        ..color = Color.lerp(
//          Color.lerp(activeColor.withAlpha(kRadialReactionAlpha), hoverColor,
//              _reactionHoverFade.value),
//          focusColor,
//          _reactionFocusFade.value,
//        );
//      final Offset center =
//          Offset.lerp(_downPosition ?? origin, origin, _reaction.value);
//      final double reactionRadius = hasFocus || hovering
//          ? kRadialReactionRadius
//          : _kRadialReactionRadiusTween.evaluate(_reaction);
//      if (reactionRadius > 0.0) {
//        canvas.drawCircle(center + offset, reactionRadius, reactionPaint);
//      }
//    }
//  }
//
//  @override
//  void describeSemanticsConfiguration(SemanticsConfiguration config) {
//    super.describeSemanticsConfiguration(config);
//
//    config.isEnabled = isInteractive;
//    if (isInteractive) config.onTap = _handleTap;
//  }
//
//  @override
//  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
//    super.debugFillProperties(properties);
//    properties.add(FlagProperty('value',
//        value: value, ifTrue: 'checked', ifFalse: 'unchecked', showName: true));
//    properties.add(FlagProperty('isInteractive',
//        value: isInteractive,
//        ifTrue: 'enabled',
//        ifFalse: 'disabled',
//        defaultValue: true));
//  }
//}
