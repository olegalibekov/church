import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {
  final Widget child;
  final List<Color> gradientColor;
  final Color color;
  final double width;
  final double height;
  final Function onPressed;

  const GradientButton({
    Key key,
    @required this.child,
    this.color,
    @required this.gradientColor,
    this.width = double.infinity,
    this.height = 50.0,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            gradient: LinearGradient(colors: gradientColor),
            color: color),
        child: Material(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(12),
            child: InkWell(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                onTap: onPressed,
                child: Center(child: child))));
  }
}
