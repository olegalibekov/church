import 'package:flutter/material.dart';

import '../utils/hex_color.dart';

class GradientBottomButton extends StatelessWidget {
  final String text;
  final List<Color> gradient;
  final bool customGradient;
  final Color color;
  final double width;
  final Function onPressed;

  GradientBottomButton({
    Key key,
    @required this.text,
    this.gradient,
    this.customGradient = false,
    this.color,
    this.width = double.infinity,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 90,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(13), topRight: Radius.circular(13)),
          gradient: LinearGradient(
              colors: customGradient == false
                  ? [HexColor('#9783E6'), HexColor('#666DE1')]
                  : gradient),
          color: color,
          boxShadow: [
            BoxShadow(
                color: Colors.grey[500],
                offset: Offset(0.0, 1.5),
                blurRadius: 1.5)
          ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: onPressed,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(13), topRight: Radius.circular(13)),
            child: Center(
                child: Text(text,
                    style: Theme
                        .of(context)
                        .textTheme
                        .button))),
      ),
    );
  }
}
