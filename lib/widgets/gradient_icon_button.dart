import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GradientIconButton extends StatelessWidget {
  final IconButton iconButton;
  final List<Color> gradientColors;

  GradientIconButton(
      {@required this.iconButton, @required this.gradientColors});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
        blendMode: BlendMode.srcIn,
        shaderCallback: (Rect bounds) {
          return ui.Gradient.linear(
            Offset(4.0, 24.0),
            Offset(20.0, 4.0),
            gradientColors,
          );
        },
        child: iconButton);
  }
}
