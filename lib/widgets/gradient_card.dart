import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GradientCard extends StatelessWidget {
  final _GradientPainter _painter;
  final Widget _child;
  final VoidCallback _callback;
  final bool radiusOnly;
  final double _radius;

  GradientCard({
    @required double strokeWidth,
    @required bool radiusOnly,
    @required double radius,
    @required List<Color> gradientColors,
    @required Widget child,
    VoidCallback onPressed,
  })  : this._painter = _GradientPainter(
            strokeWidth: strokeWidth,
            radiusOnly: radiusOnly,
            radius: radius,
      gradientColors: gradientColors),
        this._child = child,
        this.radiusOnly = radiusOnly,
        this._callback = onPressed,
        this._radius = radius;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        painter: _painter,
        child: InkWell(
            borderRadius: radiusOnly == false
                ? BorderRadius.all(Radius.circular(_radius))
                : BorderRadius.only(
                topLeft: Radius.circular(_radius),
                topRight: Radius.circular(_radius)),
            onTap: _callback,
            child: _child));
  }
}

class _GradientPainter extends CustomPainter {
  final Paint _paint = Paint();
  final bool radiusOnly;
  final double radius;
  final double strokeWidth;
  final List gradientColors;

  _GradientPainter(
      {@required double strokeWidth,
      @required bool radiusOnly,
      @required double radius,
        @required List gradientColors})
      : this.strokeWidth = strokeWidth,
        this.radiusOnly = radiusOnly,
        this.radius = radius,
        this.gradientColors = gradientColors;

  @override
  void paint(Canvas canvas, Size size) {
    // create outer rectangle equals size
    Rect outerRect = Offset.zero & size;
    var outerRRect = radiusOnly == false
        ? RRect.fromRectAndRadius(outerRect, Radius.circular(radius))
        : RRect.fromRectAndCorners(outerRect,
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius));

    // create inner rectangle smaller by strokeWidth
    Rect innerRect = Rect.fromLTWH(strokeWidth, strokeWidth,
        size.width - strokeWidth * 2, size.height - strokeWidth * 2);
    var innerRRect = radiusOnly == false
        ? RRect.fromRectAndRadius(
            innerRect, Radius.circular(radius - strokeWidth))
        : RRect.fromRectAndCorners(innerRect,
            topLeft: Radius.circular(radius - strokeWidth),
            topRight: Radius.circular(radius - strokeWidth));

    _paint.shader =
        LinearGradient(colors: gradientColors).createShader(outerRect);

    Path path1 = Path()..addRRect(outerRRect);
    Path path2 = Path()..addRRect(innerRRect);
    var path = Path.combine(PathOperation.difference, path1, path2);
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => oldDelegate != this;
}
