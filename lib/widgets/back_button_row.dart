import 'package:church/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utils/hex_color.dart';

class BackButtonRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      SizedBox(height: 42),
      IconButton(
          visualDensity: VisualDensity.compact,
          padding: EdgeInsets.all(0),
          icon: Icon(Icons.arrow_back, color: HexColor('#313131'), size: 25),
          onPressed: () => Navigator.of(context).pop()),
      GestureDetector(
          onTap: (() => Navigator.of(context).pop()),
          child: RotatedBox(
              quarterTurns: -1,
              child: Text('Назад',
                  style: textTheme.bodyText1.copyWith(color: Colors.grey))))
    ]);
  }
}
