import 'dart:math';

class PythT {
  double calcDiag(double width, double height) {
    var result = sqrt(width * width + height * height);
    return result;
  }

  double calcLeg(double diag, double width) {
    var result = sqrt((diag * diag - width * width));
    return result;
  }
}
