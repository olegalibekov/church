export 'theme_constants/animation_constants.dart';
export 'theme_constants/color_constants.dart';
export 'theme_constants/widget_constants.dart';

Duration timeout = Duration(seconds: 16);

//////////////////////////////////////////////////////////////////////
//TextTheme textTheme = TextTheme(
//    headline5: TextStyle(
//        fontSize: 24, fontWeight: FontWeight.w600, letterSpacing: -0.3),
//    headline6: TextStyle(
//        fontSize: 18, fontWeight: FontWeight.w400, letterSpacing: -0.3),
//    subtitle1: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
//    bodyText1: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
//    bodyText2: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
//    button: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
//    caption: TextStyle(fontSize: 12, fontWeight: FontWeight.w400));
//////////////////////////////////////////////////////////////////////
