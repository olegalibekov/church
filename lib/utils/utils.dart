class Utils {
  static String smartReplacer(String string, List data) {
    if (data.length > 0) {
      List dataTime = List.from(data);
      dataTime.removeAt(0);
      return smartReplacer(
          string.replaceFirst("{}", data.first.toString()), dataTime);
    } else
      return string;
  }
}
