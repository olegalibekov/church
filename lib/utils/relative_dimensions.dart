import 'package:flutter/cupertino.dart';

class D {
  w(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  h(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  cardD(BuildContext context, [bool width, int dimension]) {
    if (width == true)
      return MediaQuery.of(context).size.width * dimension;
    else
      return MediaQuery.of(context).size.height * 0.3;
  }

  playerDivider(BuildContext context, bool width, [int dimension]) {
    if (width == true)
      return MediaQuery.of(context).size.width * 0.9;
    else
      return MediaQuery.of(context).size.height * dimension;
  }
}
