import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../hex_color.dart';

List<Color> disabled0004Gradient = [black0004, black0004];
List<Color> disabled3105Gradient = [black3105, black3105];
List<Color> redGradient = [HexColor('F1967C'), HexColor('CE2E8E')];
List<Color> darkGradient = [HexColor('5E5095'), HexColor('393D79')];
List<Color> visaGradient = [HexColor('#A1A5FE'), HexColor('#A07EFF')];
List<Color> bottomButtonGradient = [HexColor('#9783E6'), HexColor('#666DE1')];
List<Color> playerOptionDisabledGradient = [
  HexColor('#7477A1'),
  HexColor('#8E84B4')
];
List<Color> floatingButtonGradient = [
  Color.fromRGBO(161, 165, 254, 0.85),
  Color.fromRGBO(160, 126, 255, 0.85)
];

Color playerBlueDark = HexColor('#4E4889');
Color playerBlueDarkDisabled = HexColor('#7B7AA6');
Color blueDark = HexColor('#393D79');
Color blue = HexColor('#3036CB');
Color blue05 = HexColor('#3036CB').withOpacity(0.5);

Color black3F03 = HexColor('#3F3F3F').withOpacity(0.3);
Color black3F07 = HexColor('#3F3F3F').withOpacity(0.7);

Color black00 = HexColor('#000000');
Color black0004 = HexColor('#000000').withOpacity(0.4);
Color black0005 = HexColor('#000000').withOpacity(0.5);

Color black31 = HexColor('#313131');
Color black3102 = HexColor('#313131').withOpacity(0.2);
Color black3105 = HexColor('#313131').withOpacity(0.5);
Color black3108 = HexColor('#313131').withOpacity(0.8);

Color blackLight = Color.fromRGBO(49, 49, 49, 0.15);

Color dividerColor01 = HexColor('#313131').withOpacity(0.1);
Color dividerColor02 = HexColor('#313131').withOpacity(0.2);
LinearGradient mainLinearGradient = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  // 10% of the width, so there are ten blinds.
  stops: [0.1, 1],
  colors: [const Color(0xFFA1A5FE), const Color(0xFFA07EFF)],
  // whitish to gray
  tileMode: TileMode.mirror, // repeats the gradient over the canvas
);
