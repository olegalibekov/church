import 'package:church/utils/constants.dart';
import 'package:church/widgets/tabbar_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

ThemeData mainTheme = ThemeData(
  scaffoldBackgroundColor: Colors.white,
  appBarTheme: AppBarTheme(elevation: 0, brightness: Brightness.light),
  primaryColor: Colors.white,
  backgroundColor: Colors.white,
  accentColor: Color(0xff9783E6),
  splashColor: Colors.transparent,
  textTheme: textTheme,

  tabBarTheme: TabBarTheme(
      indicatorSize: TabBarIndicatorSize.label,
      indicator: CustomUnderlineTabIndicator(
        borderSide: BorderSide(
          width: 3.0,
        ),
      ),
      labelColor: Color(0xff313131),
      unselectedLabelColor: Color(0xff313131).withOpacity(0.5)),
  //  textTheme: GoogleFonts.robotoTextTheme()
);

TextTheme textTheme = TextTheme(
    headline4: TextStyle(
        fontSize: 34,
        fontWeight: FontWeight.w400,
        color: black31,
        letterSpacing: -0.3),
    headline5: TextStyle(
        fontSize: 24, fontWeight: FontWeight.w700, letterSpacing: -0.3),
    headline6: TextStyle(
        fontSize: 20, fontWeight: FontWeight.w600, letterSpacing: -0.3),
    subtitle1: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
    subtitle2: TextStyle(fontSize: 13, fontWeight: FontWeight.w700),
    bodyText1: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
    bodyText2: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
    button: TextStyle(
        fontSize: 13, fontWeight: FontWeight.w700, color: Colors.white),
    caption: TextStyle(fontSize: 13, fontWeight: FontWeight.w500));
TextStyle boldBodyText1 =
    textTheme.bodyText1.copyWith(fontWeight: FontWeight.w700);
TextStyle boldBodyText2 =
    textTheme.bodyText2.copyWith(fontWeight: FontWeight.w700);
TextStyle captionGrey = textTheme.bodyText2.copyWith(color: black3105);
TextStyle signInTextStyle =
    TextStyle(fontSize: 20, fontWeight: FontWeight.w400, letterSpacing: -0.3);
TextStyle signInButtonTextStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Colors.white,
    letterSpacing: -0.3);
TextStyle leftMenuTextStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w400,
    color: Colors.white,
    letterSpacing: -0.3);
//TextStyle captionGrey3F = TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: black3F07);
TextStyle bodyTextMedium =
    textTheme.bodyText1.copyWith(fontWeight: FontWeight.w500);
TextStyle playerTextStyle =
    TextStyle(fontSize: 17, fontWeight: FontWeight.w400);
TextStyle playerTextStyleBold =
    TextStyle(fontSize: 17, fontWeight: FontWeight.w600);
TextStyle tabBarThemeUnselected =
    TextStyle(fontSize: 16, fontWeight: FontWeight.w400);
TextStyle tabBarThemeSelected =
    TextStyle(fontSize: 16, fontWeight: FontWeight.w700);
TextStyle noDataStyle = textTheme.bodyText1.copyWith(color: black3F07);
//TextStyle boldCaption =
//textTheme.caption.copyWith(fontWeight: FontWeight.w500);

EdgeInsets churchListPadding(BuildContext context) => EdgeInsets.only(
    left: MediaQuery.of(context).size.width * 0.173,
    right: MediaQuery.of(context).size.width * 0.064);

showLoadingPage(GlobalKey<ScaffoldState> pageKey) {
  return pageKey.currentState.showBottomSheet((context) => Container(
        color: Colors.white,
        height: double.maxFinite,
        child: Center(
          child: Wrap(
            children: <Widget>[
              Text('Идет загрузка...'),
              CupertinoActivityIndicator(),
            ],
          ),
        ),
      ));
}
