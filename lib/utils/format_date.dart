String formatToMinutes(Duration duration, bool leftPart) {
  String twoDigitsM(int n) {
    if (n >= 10) return "$n";
    return "$n";
  }

  String twoDigitsS(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }

  String twoDigitMinutes = twoDigitsM(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigitsS(duration.inSeconds.remainder(60));
  if (leftPart == true)
    return "-$twoDigitMinutes:$twoDigitSeconds";
  else
    return "$twoDigitMinutes:$twoDigitSeconds";
}
