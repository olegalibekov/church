import 'package:church/utils/constants.dart';
import 'package:church/utils/hex_color.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_card.dart';
import 'package:church/widgets/gradient_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool editState = false;

  @override
  Widget build(BuildContext context) {
    final List<Color> topButtonGradient = [
      HexColor('#9783E6'),
      HexColor('#666DE1')
    ];

    Widget topSection = Padding(
      padding: EdgeInsets.only(right: 16),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(bottom: 2.0),
                child: Text('Профиль',
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline5)),
            InkWell(
                radius: 30,
                onTap: () {
                  setState(() {
                    if (editState == false)
                      editState = true;
                    else
                      editState = false;
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GradientText(
                      editState == false ? 'Редактировать' : 'Сохранить',
                      gradientColors: topButtonGradient,
                      textStyle: Theme
                          .of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: Colors.white)),
                )),
          ]),
    );

    Widget textFieldWithTitle(String title) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: Text('$title',
                  style: Theme
                      .of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(color: black3F07))),
          SizedBox(height: 4),
          Theme(
            data: ThemeData(primaryColor: HexColor('#A094FE')),
            child: TextField(
                textAlignVertical: TextAlignVertical.center,
                textAlign: TextAlign.left,
                maxLines: 1,
                keyboardType: TextInputType.emailAddress,
                style: Theme
                    .of(context)
                    .textTheme
                    .bodyText1,
                decoration: InputDecoration(
                    enabled: editState == false ? false : true,
                    disabledBorder: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: HexColor('#A094FE'), width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: HexColor('#A094FE'), width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: D().w(context) * 0.05),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: HexColor('A094FE')),
                        borderRadius: BorderRadius.all(Radius.circular(12))))),
          ),
        ],
      );
    }

    Widget cardsTitle =
    Text('Мои карты', style: Theme
        .of(context)
        .textTheme
        .subtitle1);
    List<Color> gradientColors = [HexColor('#A1A5FE'), HexColor('#A07EFF')];

    Widget myCard() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: D().w(context) * 0.53,
            child: Card(
              elevation: 1.5,
              child: Padding(
                padding: EdgeInsets.all(D().w(context) * 0.05),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.asset('assets/icons/visa.png',
                            width: 40, height: 40),
                        //      Icon(Icons.credit_card, size: 30),
                        Text('9758 3432 1123 3432',
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1)
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('11/21',
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1),
                        Icon(Icons.minimize, size: 30)
                      ],
                    )
                  ],
                ),
              ),
              shape: RoundedRectangleBorder(
                side: BorderSide(color: HexColor('A094FE'), width: 1.0),
                borderRadius: BorderRadius.circular(15.0),
              ),
            ),
          ),
          SizedBox(height: 8),
        ],
      );
    }

    Widget addButton = Center(
        child: GradientCard(
            gradientColors: gradientColors,
            strokeWidth: 1,
            radius: 12,
            radiusOnly: false,
            onPressed: () {},
            child: Padding(
                padding: EdgeInsets.all(18),
                child: GradientText(
                  'Добавить'.toUpperCase(),
                  gradientColors: bottomButtonGradient,
                  textStyle: Theme
                      .of(context)
                      .textTheme
                      .button,
                ))));

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    BackButtonRow(),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 65),
                          topSection,
                          Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 20),
                                textFieldWithTitle('Имя'),
                                SizedBox(height: 12),
                                textFieldWithTitle('Email'),
                                SizedBox(height: 20),
                                cardsTitle,
                                SizedBox(height: 8),
                                myCard(),
                                myCard(),
                                SizedBox(height: 8),
                                addButton,
                                SizedBox(height: 24),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
