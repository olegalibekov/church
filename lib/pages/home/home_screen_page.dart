import 'dart:async';

import 'package:church/head_module/database_extender.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/models/church/church_model.dart';
import 'package:church/models/church/lat_lon_model.dart';
import 'package:church/pages/history/history_page.dart';
import 'package:church/utils/app_localizations.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/hex_color.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:church/widgets/church_card.dart';
import 'package:church/widgets/circle_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';

import '../search/search_mode_home_page.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, @required this.hamburger}) : super(key: key);

  final Function hamburger;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  ScrollController _scrollController;
  bool upDirection = true, flag = true;
  bool show = false;
  TabController _tabController;

  Timer showTabsTimer;
  bool showTabs = true;
  double showOffset = 0;

  timer() {
    makeItOn() {
      setState(() {
        showTabsTimer = null;
        showTabs = true;
      });
    }

    if (showTabsTimer == null)
      setState(() {
        showTabs = false;
      });
    if (showTabsTimer != null) showTabsTimer.cancel();
    showTabsTimer = Timer(Duration(milliseconds: 100), makeItOn);
  }

  String listMode = "2";
  LatLng currentLocation;
  final Distance distance = new Distance();
  bool showed = false;

  @override
  void initState() {
    super.initState();

    var initialIndex = 2;
    getLocation() async {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      if (position == null) return;
      currentLocation = null;
      setState(() {
        currentLocation = LatLng(position.latitude, position.longitude);
      });
    }

    getLocation();
    _scrollController = ScrollController();
    _scrollController
      ..addListener(() {
        upDirection = _scrollController.position.userScrollDirection ==
            ScrollDirection.forward;
        //print(_scrollController.position);
        // makes sure we don't call setState too much, but only when it is needed
        showOffset = _scrollController.offset;
        if (upDirection != flag) setState(() {});
        show = upDirection == false || _scrollController.offset > 0;
        if (!show && !showed)
          setState(() {
            print('show#$show || showed#$showed');
            showed = true;
          });
        else if (show && showed)
          setState(() {
            showed = false;
          });
        flag = upDirection;
        timer();
      });
    _tabController =
        TabController(vsync: this, length: 3, initialIndex: initialIndex);

//    _tabController.addListener(() {
//      //_getItems(_tabController.index);
//    });
  }

  Timer filterTimer;

//  TourModel tour;
  List<Widget> churchItems = [];

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final SliverAppBar sliverAppBar = SliverAppBar(
      floating: true,
      snap: true,
      leading: IconButton(
        icon: SvgPicture.asset(Images.hamburger.i()),
        onPressed: () => widget.hamburger(),
        tooltip: t(context, 'tooltips.hamburger'),
      ),
      actions: <Widget>[
        IconButton(
//          icon: SvgPicture.asset(mindClass.getI() + 'search.svg'),
          icon: SvgPicture.asset(Images.search.i()),
          onPressed: () => Navigator.push(
            context,
            CupertinoPageRoute(builder: (context) => SearchModeHomePage()),
          ),
          tooltip: t(context, 'tooltips.search'),
        ),
        IconButton(
//          icon: SvgPicture.asset(mindClass.getI() + 'cart.svg'),
            icon: SvgPicture.asset(Images.cart.i()),
//          onPressed: () => Navigator.pushNamed(context, 'donation'),
            onPressed: () => Navigator.push(
                context,
                CupertinoPageRoute(
                    builder: (BuildContext context) => History()))
//          tooltip: t(context, 'tooltips.cart'),
            )
      ],
      title: GestureDetector(
        child: Container(
          child: Text(""),
          width: double.infinity,
        ),
        onTap: () {
          _scrollController.animateTo(-1,
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        },
      ),
      automaticallyImplyLeading: false,
    );
    final SliverToBoxAdapter title = SliverToBoxAdapter(
        child: Padding(
            padding: const EdgeInsets.only(bottom: 16, left: 16),
            child: Text(
              t(context, 'pages.home_screen.title'),
              softWrap: true,
              overflow: TextOverflow.clip,
              style: Theme.of(context).textTheme.headline5,
            )));
    final Widget filterMenu = Padding(
        padding: EdgeInsets.only(right: 4.0, top: D().h(context) * 0.06),
        child: Container(
            height: MediaQuery.of(context).size.height,
            child: Align(
                //left: 16, //26.23 @www2Bot (213*100)/812
                //top: 0,
                alignment: Alignment.centerLeft,
                child: AnimatedOpacity(
                    duration: Duration(milliseconds: 150),
                    opacity: showOffset >= -14 ? 1 : 0,
                    curve: Curves.easeIn,
                    child: Material(
                        color: Colors.transparent,
                        child: RotatedBox(
                          child: Container(
//                      height: MediaQuery.of(context).size.width * 0.056,
//                      width: MediaQuery.of(context).size.height * 0.5,
                              child: TabBar(
                                  indicatorSize: TabBarIndicatorSize.tab,
                                  isScrollable: true,
                                  indicator: CircleTabIndicator(
                                      color: HexColor('#9783E6'), radius: 3),
                                  controller: _tabController,
                                  labelStyle: tabBarThemeSelected,
                                  unselectedLabelStyle: tabBarThemeUnselected,
                                  onTap: (value) {
                                    if (filterTimer != null)
                                      filterTimer.cancel();
                                    filterTimer =
                                        Timer(Duration(milliseconds: 300), () {
                                      setState(() {
                                        listMode = value.toString();
                                      });
                                    });
                                  },
                                  tabs: [
                                Tab(
                                    text: t(context,
                                        'pages.home_screen.filter.temples_text')),
                                Tab(
                                    text: t(context,
                                        'pages.home_screen.filter.monasteries_text')),
                                Tab(
                                    text: t(context,
                                        'pages.home_screen.filter.all_text'))
                              ])),
                          quarterTurns: -1,
                        ))))));
//    final SliverToBoxAdapter churchList = SliverToBoxAdapter(
//      child: Padding(
//        child: Container(
//          constraints:
//              BoxConstraints(minHeight: MediaQuery.of(context).size.height),
//          child: Hero(
//            tag: "searchList",
//            child: FutureBuilder<Map<String, ModelInterface>>(
//              builder:
//                  (BuildContext buildContext, AsyncSnapshot asyncSnapshot) {},
//              future: <String, ModelInterface>{}
//                  .loadAll(ChurchModel()), //mindClass.getChurches(),
//            ),
//          ),
//        ),
//        padding: EdgeInsets.only(
//            left: MediaQuery.of(context).size.width * 0.173,
//            right: MediaQuery.of(context).size.width * 0.064),
//      ),
//    );

    return Stack(overflow: Overflow.clip, children: <Widget>[
      Scaffold(
        body: FutureBuilder<Map<String, ModelInterface>>(
          future: <String, ModelInterface>{}.loadAll(ChurchModel()),
          builder: (context, asyncSnapshot) {
            SliverList churchList;
            //= SliverList(delegate: SliverChildBuilderDelegate((context,index)=>Container(),childCount:
            //    ));
            if (asyncSnapshot.hasData &&
                !asyncSnapshot.hasError &&
                asyncSnapshot.data != null) {
              List<ModelInterface> data = asyncSnapshot.data.values.toList();
              data = filterChurches(listMode, data);
              data = sortChurchesByLocation(currentLocation, data);
              churchList = SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (context, index) => data.length > index
                          ? AnimatedSwitcher(
                              duration: animationDuration,
                              child: Padding(
                                  key: Key("#homepage#$listMode#$index"),
                                  padding: churchListPadding(context),
                                  child: ChurchCard(church: data[index])))
                          : AnimatedSwitcher(
                              duration: animationDuration,
                              child: Padding(
                                key: Key("#homepage#$listMode#$index"),
                                padding: churchListPadding(context),
                                child: Container(),
                              ),
                            ),
                      childCount: data.length < 4 ? 4 : data.length));
            }

//            else {
//              churchList = SliverList(
//                  delegate: SliverChildBuilderDelegate(
//                          (context, index) => AnimatedSwitcher(
//                        duration: animationDuration,
//                        child: Padding(
//                          key: Key("#homepage#no_data#$index"),
//                          padding: churchListPadding(context),
//                          child: ChurchCard(
//                            church: null,
//                          ),
//                        ),
//                      ),
//                      childCount: 1));
//            }

            return CustomScrollView(
              controller: _scrollController,
              physics: BouncingScrollPhysics(),
              slivers: <Widget>[
                sliverAppBar,
                title,
                if (churchList != null) churchList,
                SliverToBoxAdapter(
                  child: SizedBox(height: 50),
                )
                /*Text(
                    AppLocalizations.of(context).t('pages.home_screen.title'),softWrap: true,overflow: TextOverflow.clip,)*/
              ],
            );
          },
        ),
      ),
      filterMenu,
      IgnorePointer(
          child: Container(
        color: Colors.white,
        child: SafeArea(
          child: Container(
            width: double.infinity,
            height: 1,
            child: AnimatedOpacity(
              child: Card(
                //decoration: BoxDecoration(gradient: mainLinearGradient),
                elevation: 1,
                color: Colors.transparent,
              ),
              duration: animationDuration,
              opacity: show == true ? 1 : 0,
            ),
          ),
          top: true,
        ),
      )),
    ]);
  }

  sortChurchesByLocation(currentLocation, List<ModelInterface> data) {
    if (currentLocation != null) {
      data.sort((church1, church2) {
        LatLonModel church1Coordinates = church1.get([ChurchModel.latLonField]);
        LatLonModel church2Coordinates = church2.get([ChurchModel.latLonField]);
        final double meter1 = distance(
            currentLocation,
            LatLng((church1Coordinates.get([LatLonModel.latField]) ?? -1),
                (church1Coordinates.get([LatLonModel.lonField]) ?? -1)));
        final double meter2 = distance(
            currentLocation,
            LatLng((church2Coordinates.get([LatLonModel.latField]) ?? -1),
                (church2Coordinates.get([LatLonModel.lonField]) ?? -1)));
        return meter1.compareTo(meter2).round();
      });
    }
    return data;
  }

  filterChurches(listMode, List<ModelInterface> data) {
    switch (listMode) {
      case "2":
        return data;
        break;
      case "1":
        List<ChurchModel> list = List();
        data.forEach((element) {
          if (element.get([ChurchModel.typeField]) == "monasteries")
            list.add(element);
        });
        return list;
        break;
      case "0":
        List<ChurchModel> list = List();
        data.forEach((element) {
          if (element.get([ChurchModel.typeField]) == "temples")
            list.add(element);
        });
        return list;
        break;
    }
  }

  @override
  bool get wantKeepAlive => true;
}
