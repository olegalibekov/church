import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/database_extensions/podcast_extension.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/models/podcast_model.dart';
import 'package:church/pages/podcast/podcast_player_page.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/format_date.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_icon_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../utils/relative_dimensions.dart';

class Podcast extends StatefulWidget {
  @override
  _PodcastState createState() => _PodcastState();
}

class _PodcastState extends State<Podcast> {
  PanelController pc = PanelController();
  bool _playerOptionTap = false;
  Map<String, PodcastModel> podcasts = {};
  Map<String, PodcastModel> horizontalPodcasts = {};
  Map<String, PodcastModel> verticalPodcasts = {};

  @override
  void initState() {
    super.initState();


    initPodcasts() async {
      Map<String, PodcastModel> podcasts = await mindClass.getPodcasts();
      mindClass.globalPodcasts.value = mindClass.sortPopularPodcasts(podcasts);
      mindClass.globalNewestPodcasts.value =
          mindClass.sortNewestPodcasts(podcasts);
      horizontalPodcasts = mindClass.globalNewestPodcasts.value;
      verticalPodcasts = mindClass.globalPodcasts.value;
    }

    initPodcasts();
  }

  @override
  Widget build(BuildContext context) {
//  DateTime.now().millisecondsSinceEpoch
    return Scaffold(
        body: SlidingUpPanel(
            controller: pc,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15)),
            maxHeight: D().h(context) * 0.97,
//            minHeight:D().h(context) * 0.97,
            minHeight: 0,
            body: buildBody(context),
            panelBuilder: (sc) =>
                PodcastPlayerPage(
                    podcasts: verticalPodcasts, sc: sc, pc: pc)));
  }

  Widget horizontalList() {
    return FutureBuilder(
        future: mindClass.getPodcasts(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          ///Don't delete
//          if (snapshot.hasData && !snapshot.hasError)
//            horizontalPodcasts = mindClass.sortNewestPodcasts(snapshot.data);

          return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 44),
                    child: Text('Новые подкасты',
                        style: Theme
                            .of(context)
                            .textTheme
                            .subtitle2)),
                SizedBox(height: 12),
                SizedBox(
                    height: 230,
                    child: ListView(
                        scrollDirection: Axis.horizontal,
                        physics: BouncingScrollPhysics(),
                        children: <Widget>[
                          SizedBox(width: 35),
                          for (var item in horizontalPodcasts.entries)
                            HorizontalCard(horizontalPodcast: item),
                          SizedBox(width: 6)
                        ]))
              ]);
        });
  }

  Widget verticalList() {
    return FutureBuilder(
        future: mindClass.getPodcasts(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          ///Don't delete
//          if (snapshot.hasData && !snapshot.hasError)
//            verticalPodcasts = mindClass.sortPopularPodcasts(snapshot.data);
//          mindClass.globalPodcasts.value = verticalPodcasts;

          return Container(
              width: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(left: 44.0),
                        child: Text('Популярные подкасты',
                            style: Theme
                                .of(context)
                                .textTheme
                                .subtitle2)),
                    SizedBox(height: 4),
                    for (var item in verticalPodcasts.entries)
                      VerticalCard(verticalPodcast: item),
                    SizedBox(height: 50)
                  ]));
        });
  }

  Widget bottomPlayer() {
    Widget invisibleBackButton = Opacity(opacity: 0, child: BackButtonRow());
    Widget player() =>
        Expanded(
            child: ValueListenableBuilder<MapEntry<String, PodcastModel>>(
                valueListenable: mindClass.globalLastChosenPodcast,
                builder: (BuildContext context, value, Widget child) =>
                    Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(16))),
                        child: InkWell(
                            borderRadius: BorderRadius.all(Radius.circular(16)),
                            onTap: _playerOptionTap == false
                                ? () => pc.animatePanelToPosition(1)
                                : null,
                            child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 12, bottom: 16, top: 16),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    children: <Widget>[
                                      Flexible(
                                          child: Text(
                                              value.value?.get(
                                                  [PodcastModel.nameField])
                                                  .toString(),
                                              style: playerTextStyleBold)),
                                      GestureDetector(
                                          onTapDown: (tap) =>
                                              setState(() =>
                                              _playerOptionTap = true),
                                          onTapCancel: () =>
                                              setState(() =>
                                              _playerOptionTap = false),
                                          child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                ValueListenableBuilder(
                                                    valueListenable: mindClass
                                                        .globalLastChosenPodcast,
                                                    builder: (
                                                        BuildContext context,
                                                        MapEntry<
                                                            String,
                                                            PodcastModel> value,
                                                        Widget child) =>
                                                        GradientIconButton(
                                                            iconButton: IconButton(
                                                                visualDensity:
                                                                VisualDensity
                                                                    .compact,
                                                                iconSize: 30,
                                                                padding: EdgeInsets
                                                                    .all(0),
                                                                icon:
                                                                SvgPicture
                                                                    .asset(
                                                                    Images
                                                                        .backward_media
                                                                        .i(),
                                                                    height: 13,
                                                                    width: 13),
                                                                onPressed: value
                                                                    .key ==
                                                                    mindClass
                                                                        .globalPodcasts
                                                                        .value
                                                                        .values
                                                                        .first
                                                                        .key
                                                                    ? null
                                                                    : () =>
                                                                    mindClass
                                                                        .podcastBackwardTap()),
                                                            gradientColors: value
                                                                .key ==
                                                                mindClass
                                                                    .globalPodcasts
                                                                    .value
                                                                    .values
                                                                    .first
                                                                    .key
                                                                ? playerOptionDisabledGradient
                                                                : darkGradient)),
                                                ValueListenableBuilder(
                                                    valueListenable:
                                                    mindClass
                                                        .globalPodcastIsPlaying,
                                                    builder: (
                                                        BuildContext context,
                                                        value, Widget child) =>
                                                        GradientIconButton(
                                                            iconButton: IconButton(
                                                                visualDensity:
                                                                VisualDensity
                                                                    .compact,
                                                                padding:
                                                                EdgeInsets.all(
                                                                    0),
                                                                iconSize: 45,
                                                                icon: SvgPicture
                                                                    .asset(
                                                                    value ==
                                                                        false
                                                                        ? Images
                                                                        .play_media
                                                                        .i()
                                                                        : Images
                                                                        .pause_media
                                                                        .i(),
                                                                    height: 25,
                                                                    width: 25),
                                                                onPressed: () {
                                                                  if (value ==
                                                                      false) {
                                                                    mindClass
                                                                        .podcastPlayer
                                                                        .resume();
                                                                  } else {
                                                                    mindClass
                                                                        .podcastPlayer
                                                                        .pause();
                                                                  }
                                                                }),
                                                            gradientColors:
                                                            darkGradient)),
                                                ValueListenableBuilder(
                                                  valueListenable: mindClass
                                                      .globalLastChosenPodcast,
                                                  builder: (
                                                      BuildContext context,
                                                      MapEntry<
                                                          String,
                                                          PodcastModel> value,
                                                      Widget child) =>
                                                      GradientIconButton(
                                                          iconButton: IconButton(
                                                              visualDensity:
                                                              VisualDensity
                                                                  .compact,
                                                              padding: EdgeInsets
                                                                  .all(0),
                                                              iconSize: 30,
                                                              icon: SvgPicture
                                                                  .asset(
                                                                  Images
                                                                      .forward_media
                                                                      .i(),
                                                                  height: 13,
                                                                  width: 13),
                                                              onPressed: value
                                                                  .key ==
                                                                  mindClass
                                                                      .globalPodcasts
                                                                      .value
                                                                      .values
                                                                      .last.key
                                                                  ? null
                                                                  : () =>
                                                                  mindClass
                                                                      .podcastForwardTap()),
                                                          gradientColors: value
                                                              .key ==
                                                              mindClass
                                                                  .globalPodcasts
                                                                  .value.values
                                                                  .last.key
                                                              ? playerOptionDisabledGradient
                                                              : darkGradient),
                                                )
                                              ]))
                                    ]))))));

    return Positioned(
        bottom: 10,
        child: SizedBox(
            width: D().w(context) < 500 ? D().w(context) : D().w(context) * 0.6,
            child: Padding(
                padding: EdgeInsets.only(right: D().w(context) < 500 ? 12 : 0),
                child: ValueListenableBuilder<MapEntry<String, PodcastModel>>(
                    valueListenable: mindClass.globalLastChosenPodcast,
                    builder: (context, value, child) =>
                        AnimatedOpacity(
                            duration: Duration(milliseconds: 300),
                            opacity:
                            value.value?.get([PodcastModel.nameField]) == null
                                ? 0
                                : 1,
                            child: IgnorePointer(
                                ignoring:
                                value.value?.get([PodcastModel.nameField]) ==
                                    null
                                    ? true
                                    : false,
                                child: Row(children: [
                                  invisibleBackButton,
                                  player()
                                ])))))));
  }

  Widget buildBody(BuildContext context) {
    Widget topSection = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('Подкасты', style: Theme
              .of(context)
              .textTheme
              .headline5),
          IconButton(
              icon: SvgPicture.asset(Images.search.i()),
              onPressed: () {
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                        builder: (context) =>
                            PodcastSearchPage()));
              })
        ]);

    return Stack(alignment: Alignment.center, children: <Widget>[
      ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(0),
          children: <Widget>[
            Row(children: <Widget>[
              BackButtonRow(),
              Expanded(
                  child: Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: topSection))
            ]),
            horizontalList(),
            verticalList(),
            ValueListenableBuilder(
              builder: (BuildContext context, value, Widget child) =>
                  SizedBox(height: value.key == null ? 0 : 80),
              valueListenable: mindClass.globalLastChosenPodcast,)
          ]),
      bottomPlayer()
    ]);
  }
}

class HorizontalCard extends StatefulWidget {
  final MapEntry<String, PodcastModel> horizontalPodcast;

  const HorizontalCard({Key key, this.horizontalPodcast}) : super(key: key);

  @override
  _HorizontalCardState createState() => _HorizontalCardState();
}

class _HorizontalCardState extends State<HorizontalCard> {
  @override
  void initState() {
    super.initState();
    _isPlaying = mindClass.checkPodcastPlaying(widget.horizontalPodcast.key);
    listeners();
  }

  @override
  Widget build(BuildContext context) => horizontalPodcastCard();

  bool _isPlaying = false;

  listeners() {
    mindClass.podcastPlayer.onPlayerStateChanged.listen((AudioPlayerState s) {
      if (s == AudioPlayerState.STOPPED || s == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
      if (mindClass.globalLastChosenPodcast.value.key ==
          widget.horizontalPodcast.key &&
          mindClass.globalLastChosenPodcast != null &&
          mindClass.podcastPlayer.state == AudioPlayerState.PLAYING)
        setState(() => _isPlaying = true);
      else if (mindClass.globalLastChosenPodcast.value.key ==
          widget.horizontalPodcast.key &&
          mindClass.globalLastChosenPodcast != null &&
          mindClass.podcastPlayer.state == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
    });
  }

  onTap() {
//    if (mindClass.globalLastChosenPodcast.value.key !=
//        widget.horizontalPodcast.key)
    if (globalLastChosenAudio.value.key !=
        widget.horizontalPodcast.key) mindClass.resetPodcast(
        widget.horizontalPodcast);
    if (!_isPlaying) {
      Future.delayed(Duration(milliseconds: 70))
          .then((value) => mindClass.podcastPlayer.resume());
    } else
      mindClass.podcastPlayer.pause();
  }

  Widget horizontalPodcastCard() {
    return Padding(
        padding: const EdgeInsets.only(right: 12.0),
        child: Stack(overflow: Overflow.visible, children: <Widget>[
          Material(
              elevation: 5,
              borderRadius: BorderRadius.circular(20),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                      imageUrl: widget.horizontalPodcast.value
                          .get([PodcastModel.coverField]),
                      width: 200,
                      height: 200,
                      fit: BoxFit.cover))),
          Positioned(
              top: 20,
              right: 4,
              child: MaterialButton(
                  child: SvgPicture.asset(
                      !_isPlaying
                          ? Images.play_media.i()
                          : Images.pause_media.i(),
                      color: playerBlueDark,
                      width: 20,
                      height: 20),
                  shape: CircleBorder(),
                  color: Colors.white,
                  elevation: 2.0,
                  padding: const EdgeInsets.all(16.0),
                  onPressed: () => onTap())),
          Positioned(
              bottom: 16,
              right: 4,
              child: SizedBox(
                  width: 180,
                  child: Card(
                      color: Colors.white,
                      elevation: 6,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0)),
                      child: InkWell(
                          borderRadius: BorderRadius.circular(12.0),
                          onTap: () {},
                          child: Center(
                              child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 14),
                                  child: Text(
                                      widget.horizontalPodcast.value
                                          .get([PodcastModel.nameField]),
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .subtitle2)))))))
        ]));
  }
}

class VerticalCard extends StatefulWidget {
  final MapEntry<String, PodcastModel> verticalPodcast;

  const VerticalCard({Key key, this.verticalPodcast}) : super(key: key);

  @override
  _VerticalCardState createState() => _VerticalCardState();
}

class _VerticalCardState extends State<VerticalCard> {
  @override
  void initState() {
    super.initState();
    _isPlaying = mindClass.checkPodcastPlaying(widget.verticalPodcast.key);
    listeners();
  }

  @override
  Widget build(BuildContext context) => verticalPodcastCard();

  bool _iconTap = false;
  bool _isPlaying = false;

  listeners() {
    mindClass.podcastPlayer.onPlayerStateChanged.listen((AudioPlayerState s) {
      if (s == AudioPlayerState.STOPPED || s == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
      if (mindClass.globalLastChosenPodcast.value.key ==
          widget.verticalPodcast.key &&
          mindClass.globalLastChosenPodcast != null &&
          mindClass.podcastPlayer.state == AudioPlayerState.PLAYING)
        setState(() => _isPlaying = true);
      else if (mindClass.globalLastChosenPodcast.value.key ==
          widget.verticalPodcast.key &&
          mindClass.globalLastChosenPodcast != null &&
          mindClass.podcastPlayer.state == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
    });
  }

  onTap() {
//    if (mindClass.globalLastChosenPodcast.value.key !=
//        widget.verticalPodcast.key)  mindClass.resetPodcast(widget.verticalPodcast);
    if (globalLastChosenAudio.value.key != widget.verticalPodcast.key) mindClass
        .resetPodcast(widget.verticalPodcast);
    if (!_isPlaying) {
      Future.delayed(Duration(milliseconds: 70))
          .then((value) => mindClass.podcastPlayer.resume());
    } else
      mindClass.podcastPlayer.pause();
  }

  Widget verticalPodcastCard() {
    return InkWell(
        splashFactory: InkRipple.splashFactory,
        splashColor: Colors.grey[350],
        onTap: !_iconTap ? () => onTap() : null,
        child: AnimatedContainer(
            curve: Curves.easeInOut,
            duration: Duration(milliseconds: 300),
            color: _isPlaying ? Colors.grey[350] : Colors.transparent,
            child: Column(children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(
                      left: 48.0, right: 12.0, top: 10.0, bottom: 10.0),
                  child: Row(children: <Widget>[
                    Material(
                        color: Colors.transparent,
                        elevation: 5,
                        borderRadius: BorderRadius.circular(12),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(6),
                            child: CachedNetworkImage(
                                imageUrl: widget.verticalPodcast.value
                                    .get([PodcastModel.coverField]),
                                width: 40,
                                height: 40,
                                fit: BoxFit.cover))),
                    SizedBox(width: 10),
                    Expanded(
                        flex: 3,
                        child: Text(
                            widget.verticalPodcast.value
                                .get([PodcastModel.nameField]),
                            style: playerTextStyleBold)),
                    Expanded(
                        flex: 1,
                        child: Text(
                            formatToMinutes(
                                Duration(
                                    seconds: widget.verticalPodcast.value
                                        .get([PodcastModel.durationField])),
                                false)
                                .toString(),
                            textAlign: TextAlign.end,
                            style: playerTextStyle)),
                    SizedBox(width: 4),
                    InkWell(
                        customBorder: CircleBorder(),
                        onTapDown: (details) => setState(() => _iconTap = true),
                        onTapCancel: () => setState(() => _iconTap = false),
                        onTap: () {
                          setState(() => _iconTap = false);
                          onTap();
                        },
                        child: IconButton(
                            icon: SvgPicture.asset(
                                _isPlaying == false
                                    ? Images.rounded_play_media.i()
                                    : Images.rounded_pause_media.i(),
                                color: playerBlueDark,
                                width: 40,
                                height: 40),
                            onPressed: null))
                  ])),
              Padding(
                  padding: EdgeInsets.only(bottom: 0, left: 98, right: 20),
                  child:
                  Divider(color: dividerColor01, thickness: 1, height: 0))
            ])));
  }
}

class PodcastSearchPage extends StatefulWidget {

  const PodcastSearchPage({Key key}) : super(key: key);

  @override
  _PodcastSearchPageState createState() => _PodcastSearchPageState();
}

class _PodcastSearchPageState extends State<PodcastSearchPage> {
  String searchString = "";
  Timer timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Container(
                color: Colors.white,
                child: TextField(
                  onChanged: (value) {
                    if (timer != null) timer.cancel();
                    timer = Timer(Duration(milliseconds: 300), () {
                      setState(() {
                        searchString = value;
                      });
                    });
                  },
                  style: Theme
                      .of(context)
                      .textTheme
                      .bodyText1,
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(color: black3105)),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: black3105)),
                      hintStyle: Theme
                          .of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: black3105),
                      hintText: "Поиск",
                      contentPadding: EdgeInsets.all(8)),
                )),
            actions: []),
        body: Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Builder(builder: (BuildContext context) {
              if (searchString.isEmpty)
                return Padding(
                    padding: const EdgeInsets.only(left: 54),
                    child: AnimatedSwitcher(
                        duration: Duration(milliseconds: 300),
                        child: Container(
                            key: Key(searchString),
                            child: Text("Введите поисковый запрос",
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(color: black0005)))));
              Map<String, PodcastModel> data = mindClass.globalPodcasts.value;
              if (searchString.isNotEmpty) {
                Map<String, PodcastModel> podList = Map();
                podList.clear();
                for (var element in data.entries) {
                  if (element.value.get([PodcastModel.nameField])
                      .toLowerCase()
                      .contains(searchString.toLowerCase()) ||
                      searchString
                          .toLowerCase()
                          .contains(element.value.get([PodcastModel.nameField])
                          .toLowerCase()))
                    podList[element.key] = element.value;
                }
                data = podList;
              }
              if (data.length == 0)
                return AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: Container(
                        key: Key(searchString),
                        child: Padding(
                            padding: const EdgeInsets.only(left: 54),
                            child: Text("Не найдено",
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(color: black0005)))));
              return AnimatedSwitcher(
                  duration: Duration(milliseconds: 300),
                  child: Container(
                      key: Key(searchString),
                      constraints: BoxConstraints(
                          minHeight: MediaQuery
                              .of(context)
                              .size
                              .height),
                      child: ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: data.length,
                          itemBuilder: (context, index) {
                            return VerticalCard(
                                verticalPodcast: data.entries.toList()[index ]);
                          })));
            })));
  }
}