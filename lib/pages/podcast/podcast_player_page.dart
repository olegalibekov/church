import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/models/podcast_model.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/format_date.dart';
import 'package:church/utils/hex_color.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:church/utils/theme_constants/color_constants.dart';
import 'package:church/utils/theme_constants/widget_constants.dart';
import 'package:church/widgets/gradient_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../custom_track_shape.dart';
import '../../utils/hex_color.dart';
import '../../utils/relative_dimensions.dart';

class PodcastPlayerPage extends StatefulWidget {
  final ScrollController sc;
  final PanelController pc;
  final Map<String, PodcastModel> podcasts;

  const PodcastPlayerPage({Key key, this.podcasts, this.sc, this.pc})
      : super(key: key);

  @override
  _PodcastPlayerPageState createState() => _PodcastPlayerPageState();
}

class _PodcastPlayerPageState extends State<PodcastPlayerPage> {
  @override
  void initState() {
    super.initState();
    mindClass.globalPodcastListener();
  }

  @override
  Widget build(BuildContext context) => buildSongList(context);

  Widget topSection() =>
      Column(children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(top: 12),
            child: Container(
                width: 45,
                height: 6,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    color: black31.withOpacity(0.2)))),
        Padding(
            padding: const EdgeInsets.only(bottom: 12.0, right: 12.0),
            child: Align(
                alignment: Alignment.topRight,
                child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                        onTap: () => setState(() => widget.pc.close()),
                        child: Padding(
                            padding: EdgeInsets.all(4),
                            child: Text('Закрыть',
                                style:
                                Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText1))))))
      ]);

  Widget podcastCover() =>
      Center(
          child: SizedBox(
              width: 240,
              height: 220,
              child: Material(
                  elevation: 5,
                  borderRadius: BorderRadius.circular(20),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: ValueListenableBuilder(
                          valueListenable: mindClass.globalLastChosenPodcast,
                          builder: (context, value, child) {
                            return CachedNetworkImage(
                                imageUrl:
                                value.value?.get([PodcastModel.coverField]),
                                height: 200,
                                fit: BoxFit.cover);
                          })))));

  Widget audioPlayer() {
    return Column(children: <Widget>[
      SizedBox(
          width: D().w(context) * 0.87,
          child: Divider(color: dividerColor01, thickness: 2)),
      Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 8),
          child: ValueListenableBuilder(
              valueListenable: mindClass.globalLastChosenPodcast,
              builder: (context, value, child) =>
                  Text(
                      mindClass.globalLastChosenPodcast.value.value
                          .get([PodcastModel.nameField]),
                      style: playerTextStyleBold.copyWith(color: blueDark)))),
      SizedBox(height: 8),
      Container(
          height: 20,
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            ValueListenableBuilder(
                valueListenable: mindClass.globalPodcastCurrentTime,
                builder: (BuildContext context, int value, Widget child) =>
                    Text(formatToMinutes(Duration(seconds: value), false),
                        style: playerTextStyle.copyWith(color: blueDark))),
            SizedBox(width: 12),
            SizedBox(
                width: D().w(context) * 0.5,
                child: SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        inactiveTrackColor: HexColor('#D9CFFF'),
                        trackShape: CustomTrackShape(),
                        trackHeight: 5.0,
                        thumbShape:
                        RoundSliderThumbShape(enabledThumbRadius: 0.0)),
                    child: ValueListenableBuilder(
                        valueListenable: mindClass
                            .globalPodcastIndicatorPosition,
                        builder: (context, value, child) =>
                            Slider(
                                activeColor: HexColor('#A092FF'),
                                value: value,
                                onChangeStart: (double value) {
                                  mindClass.globalPodcastIndPosIsChanging
                                      .value = true;
                                },
                                onChanged: (double value) {
                                  mindClass.globalPodcastIndicatorPosition
                                      .value = value;
                                },
                                onChangeEnd: (double value) {
                                  mindClass.globalPodcastIndPosIsChanging
                                      .value = false;

                                  var seekTime = (mindClass
                                      .globalLastChosenPodcast.value.value
                                      .get([PodcastModel.durationField]) *
                                      value)
                                      .toInt();

                                  mindClass.globalPodcastIndicatorPosition
                                      .value = value;
                                  mindClass.globalPodcastCurrentTime.value =
                                      seekTime;
                                  mindClass.podcastPlayer
                                      .seek(Duration(seconds: seekTime));
                                })))),
            SizedBox(width: 12),
            ValueListenableBuilder(
                valueListenable: mindClass.globalLastChosenPodcast,
                builder: (context, value, child) =>
                    Text(
                        formatToMinutes(
                            Duration(
                                seconds:
                                value.value.get([PodcastModel.durationField])),
                            false),
                        style: playerTextStyle.copyWith(color: blueDark)))
          ])),
      SizedBox(height: 4),
      Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Material(
                    color: Colors.transparent,
                    child: ValueListenableBuilder(
                        valueListenable: mindClass.globalLastChosenPodcast,
                        builder: (BuildContext context,
                            MapEntry<String, PodcastModel> value,
                            Widget child) =>
                            GradientIconButton(
                                iconButton: IconButton(
                                    iconSize: 45,
                                    icon: SvgPicture.asset(
                                        Images.backward_media.i()),
                                    onPressed: value.key ==
                                        mindClass.globalPodcasts.value
                                            .values.first.key
                                        ? null
                                        : () => mindClass.podcastBackwardTap()),
                                gradientColors: value.key ==
                                    mindClass.globalPodcasts.value.values
                                        .first.key
                                    ? playerOptionDisabledGradient
                                    : darkGradient)))),
            ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Material(
                    color: Colors.transparent,
                    child: ValueListenableBuilder(
                        valueListenable: mindClass.globalPodcastIsPlaying,
                        builder: (BuildContext context, value, Widget child) =>
                            GradientIconButton(
                                iconButton: IconButton(
                                    iconSize: 60,
                                    icon: SvgPicture.asset(value == false
                                        ? Images.play_media.i()
                                        : Images.pause_media.i()),
                                    onPressed: () {
                                      if (value == false)
                                        mindClass.podcastPlayer.resume();
                                      else
                                        mindClass.podcastPlayer.pause();
                                    }),
                                gradientColors: darkGradient)))),
            Padding(
                padding: const EdgeInsets.only(bottom: 1.5),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Material(
                        color: Colors.transparent,
                        child: ValueListenableBuilder(
                          valueListenable: mindClass.globalLastChosenPodcast,
                          builder: (BuildContext context,
                              MapEntry<String, PodcastModel> value,
                              Widget child) =>
                              GradientIconButton(
                                  iconButton: IconButton(
                                      iconSize: 45,
                                      icon: SvgPicture.asset(
                                          Images.forward_media.i()),
                                      onPressed: value.key ==
                                          mindClass.globalPodcasts.value
                                              .values.last.key
                                          ? null
                                          : () =>
                                          mindClass.podcastForwardTap()),
                                  gradientColors: value.key ==
                                      mindClass.globalPodcasts.value.values
                                          .last.key
                                      ? playerOptionDisabledGradient
                                      : darkGradient),
                        ))))
          ]),
      Padding(
          padding: const EdgeInsets.only(left: 46, right: 21),
          child: Divider(height: 0, color: dividerColor01, thickness: 1))
    ]);
  }

  Widget podcastList() {
    return Column(children: <Widget>[
      for (var podcast in mindClass.globalPodcasts.value.entries)
        VerticalCard(podcast: podcast)
    ]);
  }

  Widget buildSongList(BuildContext context) {
    return ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(0),
        controller: widget.sc,
        children: <Widget>[
          topSection(),
          SizedBox(height: 12),
          podcastCover(),
          SizedBox(height: 24),
          audioPlayer(),
          podcastList()
        ]);
  }
}

class VerticalCard extends StatefulWidget {
  final MapEntry<String, PodcastModel> podcast;

  const VerticalCard({Key key, this.podcast}) : super(key: key);

  @override
  _VerticalCardState createState() => _VerticalCardState();
}

class _VerticalCardState extends State<VerticalCard> {
  @override
  void initState() {
    super.initState();
    _isPlaying = mindClass.checkPodcastPlaying(widget.podcast.key);
    listeners();
  }

  @override
  Widget build(BuildContext context) => verticalPodcastCard();

  bool _iconTap = false;
  bool _isPlaying = false;

  listeners() {
    mindClass.podcastPlayer.onPlayerStateChanged.listen((AudioPlayerState s) {
      if (s == AudioPlayerState.STOPPED || s == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
      if (mindClass.globalLastChosenPodcast.value.key == widget.podcast.key &&
          mindClass.globalLastChosenPodcast != null &&
          mindClass.podcastPlayer.state == AudioPlayerState.PLAYING)
        setState(() => _isPlaying = true);
      else if (mindClass.globalLastChosenPodcast.value.key ==
          widget.podcast.key &&
          mindClass.globalLastChosenPodcast != null &&
          mindClass.podcastPlayer.state == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
    });
  }

  onTap() {
//    if (mindClass.globalLastChosenPodcast.value.key != widget.podcast.key)
    if (globalLastChosenAudio.value.key != widget.podcast.key) mindClass
        .resetPodcast(widget.podcast);
    if (!_isPlaying) {
      Future.delayed(Duration(milliseconds: 70))
          .then((value) => mindClass.podcastPlayer.resume());
    } else
      mindClass.podcastPlayer.pause();
  }

  Widget verticalPodcastCard() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          splashFactory: InkRipple.splashFactory,
          splashColor: Colors.grey[350],
          onTap: !_iconTap ? () => onTap() : null,
          child: AnimatedContainer(
              curve: Curves.easeInOut,
              duration: Duration(milliseconds: 300),
              color: _isPlaying ? Colors.grey[350] : Colors.transparent,
              child: Column(children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(
                        left: 48.0, right: 12.0, top: 10.0, bottom: 10.0),
                    child: Row(children: <Widget>[
                      Material(
                          color: Colors.transparent,
                          elevation: 5,
                          borderRadius: BorderRadius.circular(12),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: CachedNetworkImage(
                                  imageUrl: widget.podcast.value
                                      .get([PodcastModel.coverField]),
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover))),
                      SizedBox(width: 10),
                      Expanded(
                          flex: 3,
                          child: Text(
                              widget.podcast.value
                                  .get([PodcastModel.nameField]),
                              style: playerTextStyleBold)),
                      Expanded(
                          flex: 1,
                          child: Text(
                              formatToMinutes(
                                  Duration(
                                      seconds: widget.podcast.value.get(
                                          [PodcastModel.durationField])),
                                  false)
                                  .toString(),
                              textAlign: TextAlign.end,
                              style: playerTextStyle)),
                      SizedBox(width: 8),
                    ])),
                Padding(
                    padding: EdgeInsets.only(bottom: 0, left: 98, right: 20),
                    child:
                    Divider(color: dividerColor01, thickness: 1, height: 0))
              ]))),
    );
  }
}
