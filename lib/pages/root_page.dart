import 'package:church/head_module/brain.dart';
import 'package:church/head_module/route.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';

import 'admin/admin_page.dart';
import 'home/home_screen_page.dart';

class RootPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RootPage();
}

GlobalKey<ScaffoldState> _globalKeyScaffold = GlobalKey<ScaffoldState>();

class _RootPage extends State<RootPage> {
  final GlobalKey<InnerDrawerState> _innerDrawerKey =
      GlobalKey<InnerDrawerState>();

  void toggle() {
    setState(() {
      _innerDrawerKey.currentState.open();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _globalKeyScaffold,
        body: Container(
            decoration: BoxDecoration(gradient: mainLinearGradient),
            child: InnerDrawer(
                key: _innerDrawerKey,
                onTapClose: true,
                offset: IDOffset.horizontal(0.273),
                scale: IDOffset.horizontal(0.748),
                colorTransition: Colors.white,
                backgroundColor: Colors.transparent,
                leftChild: LeftPage(),
                boxShadow: [],
                borderRadius: 15,
                scaffold: HomeScreen(hamburger: toggle))));
  }
}

class LeftPage extends StatelessWidget {
  getMenuItem(
      {@required Function onTap, @required var icon, @required String text}) {
    return Row(
      children: <Widget>[
        Icon(icon, color: Colors.white),
        SizedBox(width: 16),
        Flexible(
            child: GestureDetector(
                onTap: onTap,
                child: Text(
                  text,
                  style: leftMenuTextStyle,
                )))
      ],
    );
  }

  space([double space = 24]) {
    return SizedBox(height: space);
  }

  @override
  Widget build(BuildContext context) {
    Widget menuItems(bool isSmallScreen) {
      return Column(children: [
        space(MediaQuery
            .of(context)
            .size
            .height * 0.187),
        getMenuItem(
            onTap: () => Navigator.pushNamed(context, Pages.profile()),
            icon: Icons.account_circle,
            text: "Мой профиль"),
        space(),
        getMenuItem(
            icon: Icons.library_music,
            onTap: () => Navigator.pushNamed(context, Pages.podcast()),
            text: "Аудио подкасты"),
        space(),
        getMenuItem(
            icon: Icons.bookmark,
            onTap: () => Navigator.pushNamed(context, Pages.bookmarks()),
            text: "Закладки"),
        space(),
        getMenuItem(
            icon: Icons.timelapse,
            onTap: () => Navigator.pushNamed(context, Pages.history()),
            text: "История"),
        space(),
        getMenuItem(
            icon: Icons.shopping_cart,
            onTap: () => Navigator.pushNamed(context, 'cart'),
            text: "Корзина"),
        isSmallScreen ? space(48) : Spacer(),
        getMenuItem(
            icon: Icons.vpn_key,
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Admin()),
              );
            },
            text: "Администратор"),
        space(),
        getMenuItem(
            onTap: () async {
              if (!mindClass.user.isAnonymous) {
                showLoadingPage(_globalKeyScaffold);
                await mindClass.auth.signOut();
                await mindClass.launchBrain();
                Navigator.pop(context);
                await Future.delayed(animationDuration);
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/', (Route<dynamic> route) => false);
              }
            },
            icon: Icons.exit_to_app,
            text: "Выйти"),
        space(D().h(context) * 0.12)
      ]);
    }

    if (D().h(context) > 600)
      return Padding(
          padding: const EdgeInsets.only(left: 24.0), child: menuItems(false));
    else
      return Padding(
          padding: const EdgeInsets.only(left: 24.0),
          child: SizedBox(
            height: 800,
            child: SingleChildScrollView(child: menuItems(true)),
          ));
  }
}
