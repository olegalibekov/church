import 'dart:async';

import 'package:church/head_module/brain.dart';
import 'package:church/head_module/database_extender.dart';
import 'package:church/head_module/database_extensions/church_extension.dart';
import 'package:church/models/church/church_model.dart';
import 'package:church/models/church/lat_lon_model.dart';
import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/church_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';

class Bookmarks extends StatefulWidget {
  Bookmarks({Key key}) : super(key: key);

  @override
  _BookmarksState createState() => _BookmarksState();
}

class _BookmarksState extends State<Bookmarks>
    with SingleTickerProviderStateMixin {
  ScrollController _scrollController;
  bool upDirection = true, flag = true;
  bool show = false;

  Timer showTabsTimer;
  bool showTabs = true;
  double showOffset = 0;

  timer() {
    makeItOn() {
      setState(() {
        showTabsTimer = null;
        showTabs = true;
      });
    }

    if (showTabsTimer == null)
      setState(() {
        showTabs = false;
      });
    if (showTabsTimer != null) showTabsTimer.cancel();
    showTabsTimer = Timer(Duration(milliseconds: 100), makeItOn);
  }

  String listMode = "2";
  LatLng currentLocation;
  final Distance distance = new Distance();
  bool showed = false;

  @override
  void initState() {
    super.initState();

    getLocation() async {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      if (position == null) return;
      currentLocation = null;
      setState(() {
        currentLocation = LatLng(position.latitude, position.longitude);
      });
    }

    getLocation();
    _scrollController = ScrollController();
    _scrollController
      ..addListener(() {
        upDirection = _scrollController.position.userScrollDirection ==
            ScrollDirection.forward;
        showOffset = _scrollController.offset;
        if (upDirection != flag) setState(() {});
        show = upDirection == false || _scrollController.offset > 0;
        if (!show && !showed)
          setState(() {
            print('show#$show || showed#$showed');
            showed = true;
          });
        else if (show && showed)
          setState(() {
            showed = false;
          });
        flag = upDirection;
        timer();
      });
  }

  Timer filterTimer;
  List<Widget> churchItems = [];
  bool loadingProcess = true;
  bool hasData = false;

  @override
  Widget build(BuildContext context) {
    Widget bodyTitle =
        Text('Закладки', style: Theme.of(context).textTheme.headline5);

    return Scaffold(
        body: Stack(overflow: Overflow.clip, children: <Widget>[
          FutureBuilder(
              future: mindClass.getBookmarks(),
              builder: (context, asyncSnapshot) {
                ListView churchList;
                if (asyncSnapshot.data != null && !asyncSnapshot.hasError) {
              List data = asyncSnapshot.data.toList();

              if (data.isEmpty)
                hasData = false;
              else
                hasData = true;
              loadingProcess = false;
              churchList = ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) =>
                      AnimatedSwitcher(
                          duration: animationDuration,
                          child: ChurchCard(church: data[index])),
                  itemCount: data.length);
                } else if (asyncSnapshot.hasError) {
                  hasData = false;
                  loadingProcess = false;
                }
//            loadingProcess = false;
//                else {
//                  churchList = ListView.builder(
//                      shrinkWrap: true,
//                      itemBuilder: (context, index) => AnimatedSwitcher(
//                          duration: animationDuration,
//                          child: ChurchCard(church: null)),
//                      itemCount: 2);
//                }

//                physics: BouncingScrollPhysics(),
//                padding: EdgeInsets.all(0),
                return Stack(children: [
                  SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.all(0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          BackButtonRow(),
                          Expanded(
                              child: Padding(
                                  padding:
                                  const EdgeInsets.only(left: 4, right: 24.0),
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        SizedBox(height: 70),
                                        bodyTitle,
                                        if (churchList != null) churchList
                                      ])))
                        ]),
                  ),
                  AnimatedOpacity(
                      duration: Duration(milliseconds: 300),
                      opacity: loadingProcess == true ? 1 : 0,
                      child: Center(child: CircularProgressIndicator())),
                  if (hasData == false)
                    AnimatedOpacity(
                        duration: Duration(milliseconds: 300),
                        opacity: loadingProcess == true ? 0 : 1,
                        child:
                        Center(child: Text('Список пуст', style: noDataStyle))),

//              loadingProcess = false;
//                Center(child: CircularProgressIndicator())
                ]);
              })
        ]));
  }

  sortChurchesByLocation(currentLocation, List<ModelInterface> data) {
    if (currentLocation != null) {
      data.sort((church1, church2) {
        LatLonModel church1Coordinates = church1.get([ChurchModel.latLonField]);
        LatLonModel church2Coordinates = church2.get([ChurchModel.latLonField]);
        final double meter1 = distance(
            currentLocation,
            LatLng((church1Coordinates.get([LatLonModel.latField]) ?? -1),
                (church1Coordinates.get([LatLonModel.lonField]) ?? -1)));
        final double meter2 = distance(
            currentLocation,
            LatLng((church2Coordinates.get([LatLonModel.latField]) ?? -1),
                (church2Coordinates.get([LatLonModel.lonField]) ?? -1)));
        return meter1.compareTo(meter2).round();
      });
    }
    return data;
  }
}
