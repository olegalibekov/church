//import 'package:cached_network_image/cached_network_image.dart';
//import 'package:church/head_module/brain.dart';
//import 'package:church/head_module/database_extensions/users_extension.dart';
//import 'package:church/utils/theme_constants/color_constants.dart';
//import 'package:church/widgets/back_button_row.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';
//import 'package:flutter/widgets.dart';
//import 'package:flutter_svg/svg.dart';
//
//import '../../utils/relative_dimensions.dart';
//
//class Bookmarks extends StatefulWidget {
//  @override
//  _BookmarksState createState() => _BookmarksState();
//}
//
//class _BookmarksState extends State<Bookmarks> {
//  bool playing = false;
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(body: buildBody(context));
//  }
//
//  testMethod() async {
//    var response = await mindClass.getUsers();
//    return response;
//  }
//
//  Widget churchCard({String imageUrl, String churchTitle, String distant}) {
//    return Padding(
//      padding: const EdgeInsets.only(left: 14.0),
//      child: Column(
//        children: <Widget>[
//          Stack(
//            overflow: Overflow.visible,
//            children: <Widget>[
//              Container(
//                clipBehavior: Clip.antiAlias,
//                decoration: BoxDecoration(
//                    color: Colors.grey[400],
//                    borderRadius: BorderRadius.circular(12)),
//                child: Opacity(
//                  opacity: .9,
//                  child: CachedNetworkImage(
//                      fit: BoxFit.cover,
//                      width: D().w(context) * 0.8,
//                      height: D().w(context) * 0.6,
//                      imageUrl: imageUrl,
//                      placeholder: (context, url) =>
//                          CircularProgressIndicator(),
//                      errorWidget: (context, url, error) => Icon(Icons.error)),
//                ),
//              ),
//              Positioned(
//                right: 12,
//                top: 12,
//                child: Row(
//                  children: <Widget>[
//                    Card(
//                        elevation: 3,
//                        shape: RoundedRectangleBorder(
//                            borderRadius: BorderRadius.circular(12.0)),
//                        child: IconButton(
//                            icon: Text(
//                          distant,
//                          style: Theme.of(context)
//                              .textTheme
//                              .caption
//                              .copyWith(color: blue05),
//                          textAlign: TextAlign.center,
//                        ), onPressed: null)),
//                    Card(
//                        elevation: 3,
//                        shape: RoundedRectangleBorder(
//                            borderRadius: BorderRadius.circular(12.0)),
//                        child: IconButton(
//                          icon: SvgPicture.asset(
//                              mindClass.getI() + 'bookmark_filled.svg'),
//                          onPressed: () {},
//                        ))
//                  ],
//                ),
//              ),
//              Positioned(
//                bottom: -30,
//                left: 10,
//                child: Container(
//                  width: D().w(context) * 0.65,
//                  child: Card(
//                      elevation: 5,
//                      shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(12.0)),
//                      child: Padding(
//                          padding: const EdgeInsets.all(16.0),
//                          child: Text(
//                            churchTitle,
//                            textAlign: TextAlign.left,
//                            style: Theme.of(context)
//                                .textTheme
//                                .bodyText2
//                                .copyWith(fontWeight: FontWeight.w500),
//                          ))),
//                ),
//              )
//            ],
//          ),
//          SizedBox(height: 50)
//        ],
//      ),
//    );
//  }
//
//  Widget buildBody(BuildContext context) {
//    Widget bodyTitle =
//        Text('Закладки', style: Theme.of(context).textTheme.headline5);
//
//    return SingleChildScrollView(
//      physics: BouncingScrollPhysics(),
//      padding: EdgeInsets.all(0),
//      child: Row(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          BackButtonRow(),
//          Expanded(
//            child: Padding(
//              padding: const EdgeInsets.only(left: 4, right: 24.0),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  SizedBox(height: 60),
//                  bodyTitle,
//                  SizedBox(height: 20),
////                  churchCard(
////                      imageUrl:
////                      'https://sib-catholic.ru/wp-content/uploads/2017/05/2017-05-18_14-53-24.png',
////                      churchTitle: 'Преображенский мужской монастырь',
////                      distant: '7.2 км'),
////                  churchCard(),
//                  churchCard(
//                      imageUrl:
//                          'https://sib-catholic.ru/wp-content/uploads/2017/05/2017-05-18_14-53-24.png',
//                      churchTitle: 'Преображенский мужской монастырь',
//                      distant: '7.2 км'),
//                ],
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//}
