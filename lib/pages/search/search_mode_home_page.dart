import 'dart:async';

import 'package:church/head_module/brain.dart';
import 'package:church/models/church/church_model.dart';
import 'package:church/utils/constants.dart';
import 'package:church/widgets/church_card.dart';
import 'package:flutter/material.dart';

import '../../head_module/database_extensions/church_extension.dart';

class SearchModeHomePage extends StatefulWidget {
  SearchModeHomePage({Key key}) : super(key: key);

  @override
  _SearchModeHomePageState createState() => _SearchModeHomePageState();
}

class _SearchModeHomePageState extends State<SearchModeHomePage> {
  String searchString = "";

  Timer timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
            color: Colors.white,
            child: TextField(
              onChanged: (value) {
                if (timer != null) timer.cancel();
                timer = Timer(Duration(milliseconds: 300), () {
                  setState(() {
                    searchString = value;
                  });
                });
              },
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyText1,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: black3105)),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: black3105)),
                  hintStyle: Theme
                      .of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: black3105),
                  hintText: "Поиск",
                  contentPadding: EdgeInsets.all(8)),
            )),
        actions: [],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 16, left: 54),
        child: FutureBuilder(
          builder: (BuildContext buildContext, AsyncSnapshot asyncSnapshot) {
            if (searchString.isEmpty)
              return Align(
                alignment: Alignment.topLeft,
                child: AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: Container(
                        key: Key(searchString),
                        child: Text("Введите поисковый запрос",
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(color: black0005)))),
              );
            if (asyncSnapshot.hasData &&
                !asyncSnapshot.hasError &&
                asyncSnapshot.data != null) {
              List<ChurchModel> data =
                  (asyncSnapshot.data as Map).values.toList();
              if (searchString.isNotEmpty) {
                List<ChurchModel> list = List();
                data.forEach((element) {
                  if (element
                          .get([ChurchModel.nameField])
                          .toLowerCase()
                          .contains(searchString.toLowerCase()) ||
                      searchString.toLowerCase().contains(
                          element.get([ChurchModel.nameField]).toLowerCase()))
                    list.add(element);
                });
                data = list;
              }
              if (data.length == 0) {
                return AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: Container(
                        key: Key(searchString),
                        child: Text("Не найдено",
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(color: black0005))));
              }
              return Padding(
                padding: const EdgeInsets.only(right: 16),
                child: AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: Container(
                        key: Key(searchString),
                        constraints: BoxConstraints(
                            minHeight: MediaQuery
                                .of(context)
                                .size
                                .height),
                        child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: data.length,
                            itemBuilder: (context, index) {
                              return ChurchCard(
                                church: data[index],
                              );
                            }))),
              );
            } else {
              return Text("Нет данных");
            }
          },
          future: mindClass.getChurches(),
        ),
      ),
    );
  }
}
