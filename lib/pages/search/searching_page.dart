//import 'package:church/pages/podcast/podcast_page.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//
//class Searching extends StatefulWidget {
//  List<PodcastModel> verticalPodcastList;
//
//  Searching(this.verticalPodcastList);
//
//  @override
//  _SearchingState createState() => _SearchingState();
//}
//
//class _SearchingState extends State<Searching> {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: buildAppBar(),
//    );
//  }
//
//  Widget buildAppBar() {
//    return PreferredSize(
//      preferredSize: Size.fromHeight(180),
//      child: AppBar(actions: <Widget>[
//        IconButton(
//            icon: Icon(Icons.search, color: Colors.black),
//            onPressed: () {
////              showSearch(
////                context: context,
////                delegate: CustomSearchDelegate(widget.verticalPodcastList),
////              );
//            })
//      ]),
//    );
//  }
//}
//
//class CustomSearchDelegate extends SearchDelegate {
//  CustomSearchDelegate({this.list});
//
//  List<PodcastModel> list = [];
//
//  @override
//  List<Widget> buildActions(BuildContext context) {
//    return [
//      IconButton(
//        icon: Icon(Icons.clear),
//        visualDensity: VisualDensity.compact,
//        onPressed: () {
//          query = '';
//        },
//      ),
//    ];
//  }
//
//  @override
//  Widget buildLeading(BuildContext context) {
//    return IconButton(
//      icon: Icon(Icons.arrow_back),
//      onPressed: () {
//        close(context, null);
//      },
//    );
//  }
//
//  @override
//  Widget buildResults(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        for (var i = 0; i < list.length; i++)
//          if (query.toLowerCase() == (list[i].songName.toLowerCase()))
//            Text(list[i].songName)
//      ],
//    );
//  }
//
//  @override
//  Widget buildSuggestions(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        for (var i = 0; i < list.length; i++)
//          if (list[i].songName.toLowerCase().contains(query.toLowerCase()) &&
//              query.length > 0)
//            GestureDetector(
//              onTap: () {
//                query = list[i].songName + list[i].songLength;
//              },
//              child: Text(list[i].songName + list[i].songLength),
//            )
//      ],
//    );
//  }
//}
