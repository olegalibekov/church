import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/database_extensions/tours_church_extension.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/models/tour/slide_model.dart';
import 'package:church/models/tour/tour_model.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/format_date.dart';
import 'package:church/widgets/gradient_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:srt_parser/srt_parser.dart';

import '../../utils/hex_color.dart';
import '../../utils/relative_dimensions.dart';

class Tour extends StatefulWidget {
  final String tourId;
  final PanelController pc;
  final ScrollController sc;
  final bool draggable;
  final TourState tourState;

  const Tour(
      {Key key, this.tourId, this.pc, this.sc, this.draggable, this.tourState})
      : super(key: key);

  @override
  TourState createState() => TourState();
}

class TourState extends State<Tour> {
  PageController pageController;
  FixedExtentScrollController fixedExtentScrollController =
  FixedExtentScrollController();
  int maxDuration = 0;
  int currIndex = 0;
  TourModel tour;
  double indicatorPosition = 0;
  double opac = 1;
  ScrollController subtitleScrollController = ScrollController();
  bool indicatorPositionIsChanging = false;
  bool hasMaxDurInList = false;
  bool showLoading = true;
  Map<dynamic, dynamic> subtitlesMap = {};
  Map slides = {};
  List<int> keyList = [];
  List<Subtitle> subtitles;

  @override
  void initState() {
    initTour();
    listeners();
    pageController = PageController(initialPage: 0, viewportFraction: 0.85);
    super.initState();
  }

  static AudioPlayer audioPlayer = AudioPlayer();
  var nowTime = 0;
  String subtitle = '';
  int itemId = 0;

  @override
  Widget build(BuildContext context) => buildBody();
  bool maxDurTicked = false;

  initTour() async {
    tour = await mindClass.getTour(widget.tourId);
    await audioPlayer.setUrl(tour[[TourModel.urlField]]);
    await audioPlayer.setReleaseMode(ReleaseMode.STOP);
    audioPlayer.setVolume(0);
    audioPlayer.resume();
    if (maxDurTicked == false) {
      audioPlayer.onDurationChanged.first.then((value) {
        audioPlayer.pause();
        audioPlayer.setVolume(1);
        keyList.add(value.inSeconds + 1);
        setState(() => maxDuration = value.inSeconds);
        setState(() => showLoading = false);
        maxDurTicked = true;
      });
    }
    AudioPlayer.logEnabled = false;
    slides = tour[[TourModel.slidesField, SlideModel.headerField]];
    List tempList = slides.keys.toList();
    for (var i in tempList) keyList.add(int.parse(i));
    keyList.sort();
    setState(() {
      audioTitle = slides['${keyList[0]}']['header'];
      subtitle = slides['${keyList[0]}']['text'];
    });
  }

  Widget imageSlider(int index) {
    return AnimatedBuilder(
        animation: pageController,
        builder: (BuildContext context, Widget child) {
          double value = 1;
          if (pageController.position.haveDimensions) {
            value = pageController.page - index;
            value = (1 - (value.abs() * 0.33)).clamp(0.1, 1);
          }
          return Center(
              child: SizedBox(
                  height: D().w(context) < 600
                      ? Curves.easeInOut.transform(value) * 200
                      : Curves.easeInOut.transform(value) * 400,
                  width: D().w(context) < 600
                      ? Curves.easeInOut.transform(value) * 600
                      : Curves.easeInOut.transform(value) * 600,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 5),
                          child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              width: D().w(context),
                              height: D().h(context) * 0.3,
                              imageUrl: slides['${keyList[index]}']['url'])))));
        });
  }

//  String formatToMinutes(Duration duration, bool leftPart) {
//    String twoDigitsM(int n) {
//      if (n >= 10) return "$n";
//      return "$n";
//    }
//
//    String twoDigitsS(int n) {
//      if (n >= 10) return "$n";
//      return "0$n";
//    }
//
//    String twoDigitMinutes = twoDigitsM(duration.inMinutes.remainder(60));
//    String twoDigitSeconds = twoDigitsS(duration.inSeconds.remainder(60));
//    if (leftPart == true)
//      return "-$twoDigitMinutes:$twoDigitSeconds";
//    else
//      return "$twoDigitMinutes:$twoDigitSeconds";
//  }

  Widget pageViewB() {
    return SizedBox(
      height:
      D().w(context) < 600 ? D().h(context) * 0.33 : D().h(context) * 0.37,
      child: PageView.builder(
          physics: NeverScrollableScrollPhysics(),
          controller: pageController,
          itemCount: slides.length,
          allowImplicitScrolling: false,
          itemBuilder: (context, position) {
            return imageSlider(position);
          }),
    );
  }

  static bool iconIsPlaying = false;
  bool isLaunched = false;
  double rating = 0;

  var cont = FixedExtentScrollController(initialItem: 2);
  var previousKey;

  var subtitleAfterSlider;
  String audioTitle = '';

  List<int> timeBeforeChange = [];
  List<int> timeAfterChange = [];

  Widget bottomPlayer() {
    return Column(children: <Widget>[
      Container(
          width: D().w(context) * 0.87,
          child: Divider(
            color: HexColor('#DEDEDE'),
            thickness: 2,
            height: 0,
          )),
      Padding(
          padding: EdgeInsets.only(top: 18.0, bottom: D().w(context) * 0.03),
          child: AnimatedOpacity(
              duration: Duration(milliseconds: 250),
              opacity: opac,
              child: GradientText(audioTitle,
                  textStyle: playerTextStyleBold.copyWith(color: Colors.white),
                  gradientColors: darkGradient))),
      SizedBox(
          height: D().w(context) * 0.1,
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            GradientText(formatToMinutes(Duration(seconds: nowTime), true),
                textStyle: playerTextStyle.copyWith(color: Colors.white),
                gradientColors: darkGradient),
            SizedBox(width: 12),
            SizedBox(
                width: D().w(context) * 0.5,
                child: SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        inactiveTrackColor: HexColor('#D9CFFF'),
                        trackShape: CustomTrackShape(),
                        trackHeight: 5.0,
                        thumbShape:
                        RoundSliderThumbShape(enabledThumbRadius: 0.0)),
                    child: Slider(
                      activeColor: HexColor('#A092FF'),
                      value: indicatorPosition,
                      onChangeStart: (double value) {
                        indicatorPositionIsChanging = true;
                        for (var element in keyList) {
                          if ((nowTime > keyList[keyList.indexOf(element)] &&
                              nowTime <
                                  keyList[keyList.indexOf(element) + 1]) ||
                              nowTime == keyList[keyList.indexOf(element)]) {
                            timeBeforeChange
                                .add(keyList[keyList.indexOf(element)]);
                            timeBeforeChange
                                .add(keyList[keyList.indexOf(element) + 1]);
                            break;
                          }
                        }
                      },
                      onChangeEnd: (double value) {
                        indicatorPositionIsChanging = false;
                        int indexToChange;
                        setState(() {
                          indicatorPosition = value;
                          nowTime = (value * maxDuration).toInt();
                        });
                        audioPlayer.seek(
                            Duration(seconds: (value * maxDuration).toInt()));
                        for (var element in keyList) {
                          if ((nowTime > keyList[keyList.indexOf(element)] &&
                              nowTime <
                                  keyList[keyList.indexOf(element) + 1]) ||
                              nowTime == keyList[keyList.indexOf(element)]) {
                            timeAfterChange
                                .add(keyList[keyList.indexOf(element)]);
                            timeAfterChange
                                .add(keyList[keyList.indexOf(element) + 1]);
                            indexToChange = keyList.indexOf(element);
                            break;
                          }
                        }
                        setState(() => currIndex = indexToChange);

                        if (timeBeforeChange[0] != timeAfterChange[0] &&
                            timeBeforeChange[1] != timeAfterChange[1]) {
                          opac = 0;
                          pageController.animateToPage(indexToChange,
                              duration: Duration(milliseconds: 300),
                              curve: Curves.easeInOut);
                          Future.delayed(Duration(milliseconds: 350))
                              .then((value) {
                            setState(() {
                              subtitleScrollController.jumpTo(0);
                              opac = 1;
                              subtitle =
                              slides['${keyList[indexToChange]}']['text'];
                              audioTitle =
                              slides['${keyList[indexToChange]}']['header'];
                            });
                          });
                        }
                        timeBeforeChange.clear();
                        timeAfterChange.clear();
                      },
                      onChanged: (double value) =>
                          setState(() => indicatorPosition = value),
                    ))),
            SizedBox(width: 12),
            GradientText(formatToMinutes(Duration(seconds: maxDuration), false),
                textStyle: playerTextStyle.copyWith(color: Colors.white),
                gradientColors: darkGradient),
          ])),
      Material(
          color: Colors.transparent,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                    iconSize: 40,
                    icon: SvgPicture.asset(Images.backward_media.i(),
                        color: currIndex != 0
                            ? playerBlueDark
                            : playerBlueDarkDisabled),
                    onPressed: currIndex != 0
                        ? () {
                      setState(() {
                        currIndex = currIndex - 1;
                        indicatorPosition =
                            keyList[currIndex] / maxDuration;
                        nowTime = keyList[currIndex];
                        opac = 0;
                      });

                      audioPlayer
                          .seek(Duration(seconds: (keyList[currIndex])));

                      pageController.animateToPage(currIndex,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.easeInOut);

                      Future.delayed(Duration(milliseconds: 350))
                          .then((value) {
                        setState(() {
                          subtitleScrollController.jumpTo(0);
                          opac = 1;
                          subtitle =
                          slides['${keyList[currIndex]}']['text'];
                          audioTitle =
                          slides['${keyList[currIndex]}']['header'];
                        });
                      });
                    }
                        : null),
                SizedBox(width: 6),
                IconButton(
                    iconSize: 60,
                    icon: SvgPicture.asset(
                        iconIsPlaying == true
                            ? Images.pause_media.i()
                            : Images.play_media.i(),
                        color: blueDark),
                    onPressed: () {
                      if (iconIsPlaying) {
                        audioPlayer.pause();
                        setState(() => iconIsPlaying = false);
                      } else {
                        audioPlayer.resume();
                        setState(() => iconIsPlaying = true);
                      }
                    }),
                SizedBox(width: 6),
                Padding(
                    padding: const EdgeInsets.only(bottom: 1.5),
                    child: IconButton(
                        iconSize: 40,
                        icon: SvgPicture.asset(Images.forward_media.i(),
                            color: currIndex != keyList.length - 2
                                ? playerBlueDark
                                : playerBlueDarkDisabled),
                        onPressed: currIndex != keyList.length - 2
                            ? () {
                          setState(() {
                            currIndex = currIndex + 1;
                            indicatorPosition =
                                keyList[currIndex] / maxDuration;
                            nowTime = keyList[currIndex];
                            opac = 0;
                          });
                          audioPlayer.seek(
                              Duration(seconds: (keyList[currIndex])));

                          pageController.animateToPage(currIndex,
                              duration: Duration(milliseconds: 300),
                              curve: Curves.easeInOut);

                          Future.delayed(Duration(milliseconds: 350))
                              .then((value) {
                            setState(() {
                              subtitleScrollController.jumpTo(0);
                              opac = 1;
                              subtitle =
                              slides['${keyList[currIndex]}']['text'];
                              audioTitle = slides['${keyList[currIndex]}']
                              ['header'];
                            });
                          });
                        }
                            : null))
              ]))
    ]);
  }

  void listeners() async {
    audioPlayer.onAudioPositionChanged.listen((event) {
      setState(() => nowTime = event.inSeconds);
      if (!indicatorPositionIsChanging)
        setState(() => indicatorPosition = event.inSeconds / maxDuration);

      for (var element in keyList) {
        if ((nowTime > keyList[keyList.indexOf(element)] &&
            nowTime < keyList[keyList.indexOf(element) + 1]) ||
            nowTime == keyList[keyList.indexOf(element)]) {
          if (currIndex != keyList.indexOf(element)) {
            setState(() => opac = 0);
            currIndex = keyList.indexOf(element);

            pageController.animateToPage(currIndex,
                duration: Duration(milliseconds: 300), curve: Curves.easeInOut);

            Future.delayed(Duration(milliseconds: 300)).then((value) {
              setState(() {
                subtitle =
                slides['${keyList[keyList.indexOf(element)]}']['text'];
                audioTitle =
                slides['${keyList[keyList.indexOf(element)]}']['header'];
                opac = 1;
              });
            });
          }
          break;
        }
      }
    });

    audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => iconIsPlaying = false);
    });
  }

  Widget buildBody() {
    Widget topSection = Column(children: <Widget>[
      Container(
          width: 45,
          height: 6,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3), color: black3102)),
      Padding(
          padding: const EdgeInsets.only(right: 16.0, bottom: 8),
          child: Align(
              alignment: Alignment.topRight,
              child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                      onTap: () {
                        setState(() {
                          widget.pc.close();
                        });
                      },
                      child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: Text('Закрыть',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyText1))))))
    ]);

    return Stack(children: [
      AnimatedOpacity(
          duration: Duration(milliseconds: 300),
          opacity: showLoading ? 0 : 1,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            SizedBox(height: 12),
            topSection,
            pageViewB(),
            Expanded(
                child: AnimatedOpacity(
                    duration: Duration(milliseconds: 250),
                    opacity: opac,
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 26.0),
                        child: ListView(
                            controller: subtitleScrollController,
                            padding: EdgeInsets.all(0),
                            children: [
                              SizedBox(height: 12),
                              Text(
                                  subtitle
                                      .replaceAll('[', '')
                                      .replaceAll(']', ''),
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .bodyText1),
                              SizedBox(height: 16)
                            ])))),
            bottomPlayer(),
            SizedBox(height: D().h(context) * 0.018)
          ])),
      Center(
          child: AnimatedOpacity(
              duration: Duration(milliseconds: 250),
              opacity: showLoading ? 1 : 0,
              child: CircularProgressIndicator()))
    ]);
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect({
    @required RenderBox parentBox,
    Offset offset = Offset.zero,
    @required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = 5;
    final double trackLeft = offset.dx;
    final double trackTop =
        offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}

//Widget buildAppBar() {
//  return AppBar(
//      elevation: 2,
//      centerTitle: true,
//      title: Text('Title',
//          style:
//          TextStyle(color: Colors.black, fontWeight: FontWeight.normal)));
//}
//  Widget _scrollingList(ScrollController sc) {
//    return ListView.builder(
//      controller: sc,
//      itemCount: 1,
//      itemBuilder: (BuildContext context, int i) {
//        return buildBody();
//      },
//    );
//  }

//  Widget onChanged(int index) {
//    Future.delayed(Duration(milliseconds: 350)).then((value) {
//
//      setState(() {
//        subtitle =
//        slides['${keyList[index]}']['text'];
//        audioTitle =
//        slides['${keyList[index]}']['header'];
//        opac = 1;
//
//      });
//    });

//    print('OnChanged index: $index');
//  }

//  Widget showCard(BuildContext context, int itemIndex) {
////    print('Item index: $itemIndex');
//
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.start,
//      children: <Widget>[
//        Container(
//          height: 200,
//          child: Center(
//            child: AnimatedContainer(
//                duration: Duration(milliseconds: 600),
//                width: 300,
//                height: itemIndex == currIndex ? 190 : 150,
//                decoration: BoxDecoration(
//                    color: itemIndex == 0
//                        ? Colors.teal
//                        : itemIndex == 1
//                            ? Colors.green
//                            : Colors.lightBlueAccent,
//                    borderRadius: BorderRadius.all(Radius.circular(15)))),
//          ),
//        ),
//        SizedBox(height: 16),
//        AnimatedOpacity(
//            duration: Duration(milliseconds: 300),
//            opacity: itemIndex == currIndex ? 1 : 0,
//            child: Text(
////              itemIndex == 0 ? first : itemIndex == 1 ? second : first,
//              subtitle,
//              style: Theme.of(context).textTheme.bodyText1,
//            ))
//      ],
//    );
//  }

//  Widget _scrollList(ScrollController sc) {
//    return ListView.builder(
//      controller: sc,
//      itemCount: 1,
//      itemBuilder: (BuildContext context, int i) {
//        return buildSongList();
//      },
//    );
//  }
//
//  Widget imageSlider(int index) {
//    return AnimatedBuilder(
//        animation: pageController,
//        builder: (BuildContext context, Widget child) {
//          double value = 1;
//          if (pageController.position.haveDimensions) {
//            value = pageController.page - index;
//            value = (1 - (value.abs() * 0.32)).clamp(0.1, 1);
//          }
//          return Column(
//            mainAxisAlignment: MainAxisAlignment.start,
//            children: <Widget>[
//              Container(
//                height: 200,
//                child: Center(
//                  child: SizedBox(
//                    height: Curves.easeInOut.transform(value) * 160,
//                    width: Curves.easeInOut.transform(value) * 600,
//                    child: Container(
//                        margin: EdgeInsets.symmetric(horizontal: 5),
//                        decoration: BoxDecoration(
//                            color: index == 0
//                                ? Colors.teal
//                                : index == 1
//                                    ? Colors.green
//                                    : Colors.purpleAccent,
//                            borderRadius:
//                                BorderRadius.all(Radius.circular(15)))),
//                  ),
//                ),
//              ),
////              SizedBox(height: D().h(context) * 0.04),
//              Text(
//                  index == 0 ? first : index == 1 ? second : first,
//                  style: Theme.of(context).textTheme.bodyText1),
//            ],
//          );
//        });
//  }
