import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_bottom_button.dart';
import 'package:church/widgets/gradient_card.dart';
import 'package:church/widgets/gradient_container.dart';
import 'package:church/widgets/gradient_icon.dart';
import 'package:church/widgets/gradient_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import '../../utils/hex_color.dart';

class SendTrebHealth extends StatefulWidget {
  @override
  _SendTrebHealthState createState() => _SendTrebHealthState();
}

class _SendTrebHealthState extends State<SendTrebHealth> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: buildBody(context), bottomNavigationBar: bottomButtons());
  }

//  buildBody(context)
  bool typeHealth = true;

  List<DropDownModel> _dropdownItemsApplicationType = new List();
  final formKey = new GlobalKey<FormState>();

  DropDownModel _dropdownValueApplicationType;

  final textController = TextEditingController();

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  List listOfNames = [];
  String nameText = '';

  @override
  void initState() {
    super.initState();
    setState(() {
      _dropdownItemsApplicationType
          .add(DropDownModel('Сорокоуст 40 дней', '240₽'));
      _dropdownItemsApplicationType
          .add(DropDownModel('Сорокоуст 80 дней', '450₽'));
      _dropdownItemsApplicationType
          .add(DropDownModel('Сорокоуст 120 дней', '650₽'));
      _dropdownValueApplicationType = _dropdownItemsApplicationType[0];
    });
//    textController.addListener(() {
//      setState(() {
//        nameText = textController.text;
//        print(nameText);
//      });
//    });
//
//    textController.notifyListeners();
  }

  Widget applicationTypeFormField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      Text('Тип записки',
          style:
          Theme
              .of(context)
              .textTheme
              .bodyText1
              .copyWith(color: black0005)),
      SizedBox(height: 8),
      Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    offset: Offset(0, 8),
                    blurRadius: 10),
              ]),
          child: GradientCard(
              radius: 13,
              strokeWidth: 1,
              gradientColors: typeHealth == true ? redGradient : darkGradient,
              radiusOnly: false,
              child: FormField(builder: (FormFieldState state) {
                return DropdownButtonHideUnderline(
                    child: InputDecorator(
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 1),
                            border: InputBorder.none),
                        isEmpty: _dropdownValueApplicationType == null,
                        child: DropdownButton<DropDownModel>(
                            isExpanded: true,
                            value: _dropdownValueApplicationType,
                            iconEnabledColor: typeHealth == true
                                ? HexColor('#CE2E8E')
                                : HexColor('#5E5095'),
                            iconSize: 30,
                            icon: Padding(
                                padding: const EdgeInsets.only(right: 6.0),
                                child: Icon(Icons.arrow_drop_down)),
                            onChanged: (DropDownModel newValue) {
                              setState(() =>
                              _dropdownValueApplicationType = newValue);
                            },
                            items: _dropdownItemsApplicationType
                                .map((DropDownModel value) {
                              return DropdownMenuItem<DropDownModel>(
                                  value: value,
                                  child: Row(children: <Widget>[
                                    SizedBox(width: 12),
                                    Center(
                                      child: GradientText(value.title,
                                          gradientColors: typeHealth == true
                                              ? redGradient
                                              : darkGradient,
                                          textStyle: Theme
                                              .of(context)
                                              .textTheme
                                              .bodyText1
                                              .copyWith(color: Colors.white)),
                                    ),
                                    SizedBox(width: 8),
                                    Text(value.price,
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .bodyText1
                                            .copyWith(color: black0005))
                                  ]));
                            }).toList())));
              })))
    ]);
  }

  Widget buildBody(BuildContext context) {
    Widget bodyTitle =
    Text('Подать записку', style: Theme
        .of(context)
        .textTheme
        .headline5);

    Widget applicationType(bool active, String text) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                if (text == 'О Упокоении')
                  typeHealth = false;
                else if (text == 'О Здравии') typeHealth = true;
              });
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GradientContainer(
                    container: Container(
                        width: 8,
                        height: 8,
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle)),
                    gradientColors: typeHealth == true && text == 'О Здравии'
                        ? redGradient
                        : typeHealth == false && text == 'О Упокоении'
                        ? darkGradient
                        : [HexColor('E0E0E0'), HexColor('E0E0E0')]),
                SizedBox(width: 12),
                GradientIcon(
                    icon: Images.about_health.i(),
                    gradientColors: typeHealth == true && text == 'О Здравии'
                        ? redGradient
                        : typeHealth == false && text == 'О Упокоении'
                        ? darkGradient
                        : [HexColor('E0E0E0'), HexColor('E0E0E0')]),
                SizedBox(width: 10),
                Text('$text',
                    style: typeHealth == true && text == 'О Здравии'
                        ? Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontWeight: FontWeight.w500)
                        : typeHealth == false && text == 'О Упокоении'
                        ? Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontWeight: FontWeight.w500)
                        : Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(
                        fontWeight: FontWeight.w500, color: blackLight)
//                    TextStyle(
//                        color: typeHealth == true && text == 'О Здравии'
//                            ? Colors.black
//                            : typeHealth == false && text == 'О Упокоении'
//                            ? Colors.black
//                            : HexColor('E0E0E0'),
//                        fontSize: 22)
                )
              ],
            ),
          ),
          SizedBox(height: 16),
        ],
      );
    }

    Widget addNameField(String title) {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
          Widget>[
        Text('$title',
            style: Theme
                .of(context)
                .textTheme
                .bodyText1
                .copyWith(color: black0005)),
        SizedBox(height: 8),
        Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 8),
                      blurRadius: 10)
                ]),
            child: GradientCard(
                radius: 13,
                radiusOnly: false,
                gradientColors: typeHealth == true ? redGradient : darkGradient,
                strokeWidth: 1,
                child: TextField(
                    controller: textController,
                    autofocus: false,
                    enabled: listOfNames.length < 10 ? true : false,
                    onChanged: (value) {
                      setState(() {
                        nameText = value;
                      });
                    },
//                          focusNode: true,
                    keyboardType: TextInputType.text,
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 12),
                        hintText: 'Имя',
                        hintStyle: Theme
                            .of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(color: black0005),
                        border: InputBorder.none))))
      ]);
    }

    Widget addButton = Center(
        child: GradientCard(
            strokeWidth: 1,
            radius: 12,
            onPressed: textController.text.isNotEmpty
                ? () {
              setState(() {
                if (textController.text.isNotEmpty &&
                    listOfNames.length < 10) {
                  listOfNames.add(nameText);
                  nameText = '';
                  textController.clear();
                }
              });

//              if (textController.text.isNotEmpty && listOfNames.length < 10) {
//                setState(() {
//                  listOfNames.add(textController.text);
//                  textController.clear();
//                });
//              } else if (textController.text.isEmpty) {
//                showAlertDialog(context, true);
//              } else if (listOfNames.length == 10) {
//                showAlertDialog(context, false);
//              }
            }
                : null,
            gradientColors: nameText.isNotEmpty && typeHealth == true
                ? redGradient
                : nameText.isNotEmpty && typeHealth == false
                ? darkGradient
                : <Color>[HexColor('D3D3D3'), HexColor('D3D3D3')],
            radiusOnly: false,
            child: Padding(
                padding: EdgeInsets.all(18),
                child: GradientText(
                  'Добавить'.toUpperCase(),
                  gradientColors: nameText.isNotEmpty && typeHealth == true
                      ? redGradient
                      : nameText.isNotEmpty && typeHealth == false
                      ? darkGradient
                      : disabled3105Gradient,
                  textStyle: Theme
                      .of(context)
                      .textTheme
                      .button,
                ))));

    List<Color> greyGradient = [HexColor('#D3D3D3'), HexColor('#D3D3D3')];


    Widget nameCard(String name, int index) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, right: 16),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            offset: Offset(0, 8),
                            blurRadius: 10)
                      ]),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                    ),
                    child: GradientCard(
                      radius: 12,
                      strokeWidth: 1,
                      radiusOnly: false,
                      gradientColors: greyGradient,
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                              padding: EdgeInsets.all(16),
                              child: Text(name,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .bodyText1
                                      .copyWith(color: black31)))),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
                right: 7,
                top: -2.5,
                child: Material(
                  color: Colors.white,
                  child: GradientCard(
                      onPressed: () {
                        setState(() {
                          listOfNames.removeAt(index);
                          //  nameList.remove(nameList[id]);
                          //   nameList.removeWhere((key, value) => key == id);
                        });
                      },
                      radius: 10,
                      strokeWidth: 1,
                      radiusOnly: false,
                      gradientColors: greyGradient,
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Icon(
                          Icons.clear,
                          size: 18,
                        ),
                      )),
                ))
          ],
        ),
      );
    }

//    return applicationTypeFormField();
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.all(0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          BackButtonRow(),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 65),
                        bodyTitle,
                        SizedBox(height: 16),
                        Text('Тип записки',
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(color: black0005)),
                        SizedBox(height: 20),
                        applicationType(true, 'О Здравии'),
                        applicationType(false, 'О Упокоении'),
                        SizedBox(height: 4),
                        applicationTypeFormField(),
                        SizedBox(height: 16),
                        addNameField('Добавить имя (до 10 имен)'),
                        SizedBox(height: 18),
                        addButton,
                        SizedBox(
                          height: 16,
                        )
                      ]),
                ),
//                 nameCard(listOfNames[0], 0),
                SizedBox(
                  width: double.infinity,
                  child: Wrap(
                    spacing: 20,
                    children: <Widget>[
                      for (var i = 0; i < listOfNames.length; i++)
                        nameCard(listOfNames[i], i),
                    ],
                  ),
                ),
                SizedBox(height: 24)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget bottomButtons() {
    return Row(
      children: <Widget>[
        Expanded(
          child: SizedBox(
            height: 90,
            child: GradientCard(
                radius: 15,
                strokeWidth: 1,
                onPressed: () {},
                gradientColors: typeHealth == true ? redGradient : darkGradient,
                radiusOnly: true,
                child: Center(
                  child: GradientText(
                    'Добавить в корзину'.toUpperCase(),
                    gradientColors:
                    typeHealth == true ? redGradient : darkGradient,
                    textStyle: Theme
                        .of(context)
                        .textTheme
                        .button,
                  ),
                )),
          ),
        ),
        Expanded(
          child: SizedBox(
              height: 90,
              child: GradientBottomButton(
                  onPressed: () {},
                  customGradient: true,
                  gradient: typeHealth == true ? redGradient : darkGradient,
                  text: 'Оформить'.toUpperCase())),
        ),
      ],
    );
  }
}

class DropDownModel {
  String title;
  String price;

  DropDownModel(this.title, this.price);
}
