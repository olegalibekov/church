//import 'package:cached_network_image/cached_network_image.dart';
//import 'package:church/head_module/brain.dart';
//import 'package:church/head_module/database_extensions/church_extension.dart';
//import 'package:church/models/church_model.dart';
//import 'package:church/utils/app_localizations.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';
//import 'package:flutter_svg/flutter_svg.dart';
//
//class ChurchPage extends StatefulWidget {
//  final ChurchModel church;
//
//  const ChurchPage({Key key, @required this.church}) : super(key: key);
//
//  @override
//  _ChurchPageState createState() => _ChurchPageState();
//}
//
//class _ChurchPageState extends State<ChurchPage>
//    with SingleTickerProviderStateMixin {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: buildBody(context),
//    );
//  }
////
////  Widget buildBody(BuildContext context) {
////    //   final size = MediaQuery.of(context).size;
////    return Stack(
////      children: <Widget>[
////        Padding(
////          padding: const EdgeInsets.only(top: 64.0),
////          child: GestureDetector(
////            onTap: () => Navigator.pop(context),
////            child: Container(
////              child: Column(
////                children: <Widget>[
////                  IconButton(
////                    icon: Icon(Icons.arrow_back, color: HexColor('#313131')),
////                  ),
////                  RotatedBox(
////                      quarterTurns: -1,
////                      child: Text(
////                        'Назад',
////                        style: TextStyle(color: Colors.grey),
////                      )),
////                ],
////              ),
////            ),
////          ),
////        ),
////        Padding(
////          padding: const EdgeInsets.only(top: 16.0),
////          child: Stack(
////            children: <Widget>[
////              NotificationListener<OverscrollIndicatorNotification>(
////                onNotification: (overscroll) {
////                  overscroll.disallowGlow();
////                },
////                child: ListView(
////                  children: <Widget>[
////                    Align(
////                      alignment: Alignment.centerRight,
////                      child: Stack(
////                        overflow: Overflow.visible,
////                        children: <Widget>[
////                          Container(
////                            clipBehavior: Clip.antiAlias,
////                            decoration: BoxDecoration(
////                                color: Colors.grey[400],
////                                borderRadius: BorderRadius.only(
////                                    topLeft: Radius.circular(12),
////                                    bottomLeft: Radius.circular((12)))),
////                            child: Opacity(
////                              opacity: .9,
////                              child: CachedNetworkImage(
////                                fit: BoxFit.cover,
////                                height:
////                                    MediaQuery.of(context).size.height * 0.29,
////                                //   height: MediaQuery.of(context).size.height * 0.3,
////                           //     imageUrl:
//////                                https://sib-catholic.ru/wp-content/uploads/2017/05/2017-05-18_14-53-24.png
////                               //     "${widget.church.urls[0]}",
////                                placeholder: (context, url) =>
////                                    CircularProgressIndicator(),
////                                errorWidget: (context, url, error) =>
////                                    Icon(Icons.error), imageUrl: '',
////                              ),
////                            ),
////                          ),
////                          Positioned(
////                            left: 8,
////                            top: 175,
////                            child: Row(
////                              children: <Widget>[
////                                Card(
////                                  elevation: 3,
////                                  shape: RoundedRectangleBorder(
////                                    borderRadius: BorderRadius.circular(12.0),
////                                  ),
////                                  child: StreamBuilder(
////                                      stream: mindClass
////                                          .isBookmarked(widget.church.churchId),
////                                      builder: (BuildContext context,
////                                          AsyncSnapshot asyncSnapshot) {
////                                        if (asyncSnapshot.hasData &&
////                                            !asyncSnapshot.hasError &&
////                                            asyncSnapshot.data.snapshot.value !=
////                                                null) {
////                                          var isBookmarked =
////                                              asyncSnapshot.data.snapshot.value;
////                                          return IconButton(
////                                            icon: isBookmarked
////                                                ? SvgPicture.asset(
////                                                    'assets/icons/bookmark_filled.svg')
////                                                : SvgPicture.asset(
////                                                    'assets/icons/bookmark.svg'),
////                                            onPressed: () => mindClass
////                                                .bookmarkChurch(widget.church),
////                                            tooltip:
////                                                AppLocalizations.of(context)
////                                                    .t('tooltips.bookmark'),
////                                          );
////                                        }
////                                        return IconButton(
////                                          icon: SvgPicture.asset(
////                                              'assets/icons/bookmark.svg'),
////                                          onPressed: () => mindClass
////                                              .bookmarkChurch(widget.church),
////                                          tooltip: AppLocalizations.of(context)
////                                              .t('tooltips.bookmark'),
////                                        );
////                                      }),
////                                ),
////                                Card(
////                                  elevation: 3,
////                                  shape: RoundedRectangleBorder(
////                                    borderRadius: BorderRadius.circular(12.0),
////                                  ),
////                                  child: IconButton(
////                                      icon:
////                                          Icon(Icons.settings_input_component),
////                                      onPressed: () {}),
////                                ),
////                                Container(
////                                  height: 60,
////                                  width: 140,
////                                  child: Card(
////                                    elevation: 3,
////                                    shape: RoundedRectangleBorder(
////                                      borderRadius: BorderRadius.circular(12.0),
////                                    ),
////                                    child: Row(
////                                      children: <Widget>[
////                                        IconButton(
////                                          icon: Icon(
////                                            Icons.info_outline,
////                                            color: HexColor('#3036CB'),
////                                          ),
////                                          onPressed: () {},
////                                          iconSize: 32,
////                                        ),
////                                        Container(
////                                          //   color: Colors.green,
////                                          child: Flexible(
////                                            child: Text(
////                                              'Полезная\nИнформация',
////                                              textAlign: TextAlign.center,
////                                            ),
////                                          ),
////                                        )
////                                      ],
////                                    ),
////                                  ),
////                                ),
////                              ],
////                            ),
////                          ),
////                        ],
////                      ),
////                    ),
////                    Padding(
////                      padding:
////                          const EdgeInsets.only(top: 36, left: 50, right: 8),
////                      child: Container(
////                        child: Column(
////                          children: <Widget>[
////                            Wrap(
////                              children: <Widget>[
////                                Text(
////                                  '${widget.church.name}',
////                                  style: TextStyle(
////                                      fontSize: 20,
////                                      fontWeight: FontWeight.bold),
////                                ),
////                              ],
////                            ),
////                            Padding(
////                              padding:
////                                  const EdgeInsets.only(top: 16.0, bottom: 12),
////                              child: Row(
////                                children: <Widget>[
////                                  SvgPicture.asset('assets/icons/search.svg'),
////                                  Padding(
////                                      padding: EdgeInsets.only(left: 12),
////                                      child: Text('Настоятель Алексий Шамов',
////                                          style: TextStyle(
////                                              color: Colors.grey,
////                                              fontSize: 16)))
////                                ],
////                              ),
////                            ),
////                            Column(
////                              crossAxisAlignment: CrossAxisAlignment.start,
////                              children: <Widget>[
////                                Padding(
////                                  padding: const EdgeInsets.only(bottom: 8.0),
////                                  child: Text(
//////                                      'В Каменск-Уральском Преображенском мужском монастыре пребывают пять святынь – частицы мощей святых Православной Церкви, которым вы можете поклониться, и ...',
////                                    '${widget.church.about}',
////                                    style: TextStyle(fontSize: 16),
////                                  ),
////                                ),
////                                GestureDetector(
////                                  onTap: () {},
////                                  child: Text(
////                                    '+ Дальше',
////                                    style: TextStyle(
////                                        fontSize: 12,
////                                        color: HexColor('#3036CB')),
////                                  ),
////                                )
////                              ],
////                            ),
////                            Padding(
////                              padding:
////                                  const EdgeInsets.only(top: 24, right: 24),
////                              child: Wrap(
////                                children: <Widget>[
////                                  Column(
////                                    children: <Widget>[
////                                      Row(
////                                        children: <Widget>[
////
////                                          Icon(
////                                            Icons.mobile_screen_share,
////                                            color: HexColor('#9783E6'),
////                                          ),
////                                          Spacer(
////                                            flex: 1,
////                                          ),
////                                          Text('Телефон',
////                                              style: TextStyle(
////                                                  color: HexColor('#9783E6'),
////                                                  fontSize: 16)),
////                                          Spacer(
////                                            flex: 9,
////                                          ),
////                                          Text('+7(3439)37-99-75',
////                                              style: TextStyle(fontSize: 16))
////                                        ],
////                                      ),
////                                      SizedBox(
////                                        height: 10,
////                                      ),
////                                      Divider(color: Colors.grey),
////                                      SizedBox(
////                                        height: 10,
////                                      ),
////                                      Row(
////                                        crossAxisAlignment:
////                                            CrossAxisAlignment.start,
////                                        children: <Widget>[
////                                          Padding(
////                                            padding: const EdgeInsets.only(
////                                                right: 12.0),
////                                            child: Icon(
////                                              Icons.location_on,
////                                              color: HexColor('#9783E6'),
////                                            ),
////                                          ),
////                                          Text('Адрес',
////                                              style: TextStyle(
////                                                  color: HexColor('#9783E6'),
////                                                  fontSize: 16)),
////                                          Spacer(),
////                                          Flexible(
////                                            child: Text(
////                                              '${widget.church.address}',
////                                              style: TextStyle(fontSize: 16),
////                                              textAlign: TextAlign.right,
////                                            ),
////                                          )
////                                        ],
////                                      ),
////                                      SizedBox(
////                                        height: 20,
////                                      ),
////                                      Container(
////                                        decoration: BoxDecoration(
////                                            //color: Colors.orange,
////                                            borderRadius: BorderRadius.all(
////                                                Radius.circular(10))),
////                                        height:
////                                            MediaQuery.of(context).size.height *
////                                                0.3,
////                                        width: double.infinity,
////                                      )
////                                    ],
////                                  ),
////                                ],
////                              ),
////                            ),
////                          ],
////                        ),
////                      ),
////                    ),
////                  ],
////                ),
////              ),
////              Positioned(
////                bottom: MediaQuery.of(context).size.height * 0.02,
////                child: Column(
////                  crossAxisAlignment: CrossAxisAlignment.end,
////                  children: <Widget>[
////                    Container(
////                      height: MediaQuery.of(context).size.height * 0.125,
////                      width: MediaQuery.of(context).size.width,
////                      child:
////                          NotificationListener<OverscrollIndicatorNotification>(
////                        onNotification: (overscroll) {
////                          overscroll.disallowGlow();
////                        },
////                        child: ListView(
////                          scrollDirection: Axis.horizontal,
////                          children: <Widget>[
////                            SizedBox(
////                              width: 50,
////                            ),
////                            Padding(
////                              padding:
////                                  const EdgeInsets.symmetric(vertical: 8.0),
////                              child: Container(
////                                height:
////                                    MediaQuery.of(context).size.height * 0.11,
////                                width: MediaQuery.of(context).size.width * 0.51,
////                                child: RaisedGradientButton(
////                                  child: Padding(
////                                    padding: const EdgeInsets.symmetric(
////                                        horizontal: 16),
////                                    child: Row(
////                                      children: <Widget>[
////                                        Icon(
////                                          Icons.note,
////                                          size: 30,
////                                          color: Colors.white,
////                                        ),
////                                        SizedBox(
////                                          width: 12,
////                                        ),
////                                        Text('Подать записку',
////                                            style: TextStyle(
////                                                fontSize: 16,
////                                                color: Colors.white)),
////                                      ],
////                                    ),
////                                  ),
////                                  gradient: LinearGradient(
////                                    colors: <Color>[
////                                      HexColor('#9783E6'),
////                                      HexColor('#666DE1')
////                                    ],
////                                  ),
////                                ),
////                              ),
////                            ),
////                            SizedBox(
////                              width: 12,
////                            ),
////                            Padding(
////                              padding:
////                                  const EdgeInsets.symmetric(vertical: 8.0),
////                              child: Container(
////                                height:
////                                    MediaQuery.of(context).size.height * 0.11,
////                                width: MediaQuery.of(context).size.width * 0.5,
////                                child: RaisedGradientButton(
////                                  child: Padding(
////                                    padding: const EdgeInsets.symmetric(
////                                        horizontal: 16),
////                                    child: Row(
////                                      children: <Widget>[
////                                        Icon(
////                                          Icons.attach_money,
////                                          size: 30,
////                                          color: Colors.black,
////                                        ),
////                                        SizedBox(
////                                          width: 12,
////                                        ),
////                                        Text('Пожертвовать',
////                                            style: TextStyle(
////                                                fontSize: 16,
////                                                color: Colors.black)),
////                                      ],
////                                    ),
////                                  ),
////                                  color: Colors.white,
////                                ),
////                              ),
////                            ),
////                            SizedBox(
////                                width:
////                                    MediaQuery.of(context).size.width * 0.087),
////                          ],
////                        ),
////                      ),
////                    ),
////                  ],
////                ),
////              ),
////            ],
////          ),
////        ),
////      ],
////    );
////  }
////}
//
//class RaisedGradientButton extends StatelessWidget {
//  final Widget child;
//  final Gradient gradient;
//  final Color color;
//  final double width;
//  final double height;
//  final Function onPressed;
//
//  const RaisedGradientButton({
//    Key key,
//    @required this.child,
//    this.color,
//    this.gradient,
//    this.width = double.infinity,
//    this.height = 50.0,
//    this.onPressed,
//  }) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      width: width,
//      height: 50.0,
//      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(20)),
//          gradient: gradient,
//          color: color,
//          boxShadow: [
//            BoxShadow(
//              color: Colors.grey[500],
//              offset: Offset(0.0, 1.5),
//              blurRadius: 1.5,
//            ),
//          ]),
//      child: Material(
//        color: Colors.transparent,
//        child: InkWell(
//            onTap: onPressed,
//            child: Center(
//              child: child,
//            )),
//      ),
//    );
//  }
//}
//
//class HexColor extends Color {
//  static int _getColorFromHex(String hexColor) {
//    hexColor = hexColor.toUpperCase().replaceAll("#", "");
//    if (hexColor.length == 6) {
//      hexColor = "FF" + hexColor;
//    }
//    return int.parse(hexColor, radix: 16);
//  }
//
//  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
//}
