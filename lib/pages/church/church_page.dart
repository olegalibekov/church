import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/database_extensions/church_extension.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/models/church/church_model.dart';
import 'package:church/models/church/lat_lon_model.dart';
import 'package:church/models/church/owner_model.dart';
import 'package:church/models/podcast_model.dart';
import 'package:church/pages/booking/booking_page.dart';
import 'package:church/pages/church/send_treb_health.dart';
import 'package:church/pages/church/tour_page.dart';
import 'package:church/pages/history/story_to_road_page.dart';
import 'package:church/pages/podcast/podcast_player_page.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/maps_launcher.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_button.dart';
import 'package:church/widgets/gradient_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:latlong/latlong.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../utils/hex_color.dart';
import '../../utils/relative_dimensions.dart';
import '../payment/dontation_page.dart';

class ChurchPage extends StatefulWidget {
  final ChurchModel churchModel;

  const ChurchPage({Key key, this.churchModel}) : super(key: key);

  @override
  _ChurchPageState createState() => _ChurchPageState();
}

class _ChurchPageState extends State<ChurchPage>
    with SingleTickerProviderStateMixin {
  PanelController pc = PanelController();
  double lat;
  double lon;
  bool isBookmarked;

  @override
  void initState() {
    checkBookmark();
    lat = widget.churchModel[[ChurchModel.latLonField, LatLonModel.latField]];
    lon = widget.churchModel[[ChurchModel.latLonField, LatLonModel.lonField]];
    super.initState();
  }

  bool draggable = true;
  final globalKey = GlobalKey<ScaffoldState>();
  final globalKeyTour = GlobalKey<TourState>();
  String _openPageByName = 'tour';
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return WillPopScope(
      // ignore: missing_return
        onWillPop: () {
          if (pc.isPanelOpen)
            pc.close();
          else
            Navigator.of(context).pop();
        },
        child: Scaffold(
            key: globalKey,
            body: SlidingUpPanel(
                controller: pc,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(17),
                    topRight: Radius.circular(17)),
                maxHeight: D().h(context) * 0.97,
                minHeight: 0,
//       panelBuilder: (ScrollController sc) => _scrollingList(sc),
                body: buildBody(context),
                panel: _openPageByName == 'tour' ? Tour(
                    key: globalKeyTour,
                    tourId: widget.churchModel[[ChurchModel.tourIdField]],
                    pc: pc,
                    draggable: draggable)
                    : null,
                panelBuilder: (sc) =>
                _openPageByName == 'podcast' ? PodcastPlayerPage()
                    : StoryToRoad(),
                isDraggable: true,
                onPanelClosed: () {
                  setState(() => TourState.iconIsPlaying = false);
                  TourState.audioPlayer.pause();
                  globalKey.currentState.hideCurrentSnackBar();
                }
            )
        )
    );
  }

  ScrollController scrollC;

  buildModalSheet(BuildContext context) {
    showModalBottomSheet(
        elevation: 1,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15), topRight: Radius.circular(15))),
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return DraggableScrollableSheet(
            expand: false,
            initialChildSize: .95,
            builder: (BuildContext context, ScrollController scrollController) {
              scrollC = scrollController;
              return Tour(
                  tourId: widget.churchModel[[ChurchModel.tourIdField]],
                  pc: pc,
                  sc: scrollController);
            },
          );
        });
  }

  Future checkBookmark() async {
    var result = await mindClass.isBookmarked(widget.churchModel.key);
    setState(() => isBookmarked = result);
  }

  Widget imageSection(BuildContext context) {
    return Align(
        alignment: Alignment.centerRight,
        child: SizedBox(
          width: D().w(context),
          height: D().w(context) < 600 ? D().h(context) * 0.33 : 340,
          child: Stack(overflow: Overflow.visible, children: <Widget>[
            Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                    color: Colors.grey[400],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12),
                        bottomLeft: Radius.circular((12)))),
                child: Opacity(
                    opacity: .9,
                    child: CachedNetworkImage(
                        width: D().w(context),
                        height: D().w(context) < 600
                            ? D().h(context) * 0.3
                            : 320,
                        fit: BoxFit.cover,
                        imageUrl:
                        widget.churchModel.get([ChurchModel.urlsField])[0],
//                      "https://sib-catholic.ru/wp-content/uploads/2017/05/2017-05-18_14-53-24.png",
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            Icon(Icons.error)))),
            Positioned(
                left: 8,
                bottom: 0,
                child: Row(children: <Widget>[
                  SizedBox(
                      height: 56,
                      child: Card(
                          shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 0.5,
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              borderRadius: BorderRadius.circular(12.0)),
                          child: InkWell(
                              borderRadius: BorderRadius.circular(12.0),
                              onTap: () async {
                                await mindClass
                                    .bookmarkChurch(widget.churchModel.key);
                                checkBookmark();
                              },
                              child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 3),
                                  child: IconButton(
                                      visualDensity: VisualDensity.compact,
                                      icon: isBookmarked == true
                                          ? SvgPicture.asset(
                                          Images.bookmark_filled.i())
                                          : SvgPicture.asset(
                                          Images.bookmark.i()), onPressed: null
                                  )
                              )
//                        child: Padding(
//                            padding: EdgeInsets.all(12),
//                            child: ),
                          ))),
                  SizedBox(
                      height: 56,
                      child: Card(
                          shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 0.5,
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              borderRadius: BorderRadius.circular(12.0)),
                          child: InkWell(
                              borderRadius: BorderRadius.circular(12.0),
                              onTap: () {},
                              child: IconButton(
                                  icon: SvgPicture.asset(Images.route.i()),
                                  onPressed: null)))),
                  SizedBox(
                    height: 56,
                    child: Card(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                width: 0.5,
                                color: Color.fromRGBO(0, 0, 0, 0.1)),
                            borderRadius: BorderRadius.circular(12.0)),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(12.0),
                          onTap: () {
                            _openPageByName = 'tour';
                            pc.open();
//                            print(TourState.maxDuration);
//                          Scaffold.of(context).showSnackBar(snackBar);
//                            buildModalSheet(context);
                          },
                          child: Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                children: [
                                  SvgPicture.asset(Images.info.i()),
                                  SizedBox(width: 6),
                                  Text(
                                    'Полезная\nинформация',
                                    textAlign: TextAlign.center,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .caption
                                        .copyWith(color: black00),
                                  )
                                ],
                              )),
//                          child: Padding(
//                              padding: const EdgeInsets.only(
//                                  left: 6.0, right: 10),
//                              child: Row(children: <Widget>[
//                                SvgPicture.asset(Images.info.i()),
//                                Flexible(
//                                    child: Text(
//                                      'Полезная информация',
//                                      textAlign: TextAlign.center,
//                                      style: Theme
//                                          .of(context)
//                                          .textTheme
//                                          .caption
//                                          .copyWith(color: black00),
//                                    ))
//                              ]))
                        )),
                  )
                ]))
          ]),
        ));
  }

  Widget priest() {
    return Padding(
        padding: const EdgeInsets.only(top: 20.0, bottom: 18),
        child: Row(children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(11),
              child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  height: D().w(context) < 600 ? 45 : 60,
                  width: D().w(context) < 600 ? 45 : 60,
                  imageUrl: '${widget.churchModel[[
                    ChurchModel.ownerField,
                    OwnerModel.url
                  ]]}',
                  placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error))),
          SizedBox(width: 16),
          Flexible(
              child: Text(
                  '${widget.churchModel[[
                    ChurchModel.ownerField,
                    OwnerModel.postField
                  ]]} ${widget.churchModel[[
                    ChurchModel.ownerField,
                    OwnerModel.nameField
                  ]]}',
                  style: Theme
                      .of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: black0005)))
        ]));
  }

//
  Widget churchDescription() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DescriptionTextWidget(
              text: widget.churchModel[[ChurchModel.aboutField]]),
          SizedBox(height: 10),
        ]);
  }

  Widget rowTitle(String rowTitle) {
    return Text(rowTitle, style: Theme
        .of(context)
        .textTheme
        .subtitle2);
  }

  Widget excursionReference() {
    return Column(children: <Widget>[
      Align(
          alignment: Alignment.centerLeft,
          child: rowTitle('Экскурсионная контора')),
      SizedBox(height: 10),
      Align(
          alignment: Alignment.centerLeft,
          child: InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0),
                child: Text('Ссылка на контору',
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: HexColor('#554B8E'))),
              ))),
      SizedBox(height: 20)
    ]);
  }

  Widget personalInfoCard(var icon, String title, String info) {
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
//      Icon(icon, color: blue05),
      Row(children: <Widget>[
        if (icon != Images.phone.i())
          SizedBox(
            width: 3.7,
          ),
        Align(alignment: Alignment.topLeft, child: SvgPicture.asset(icon)),
        if (icon != Images.phone.i()) SizedBox(width: 12),
        if (icon == Images.phone.i()) SizedBox(width: 7),
        Text(title,
            style:
            Theme
                .of(context)
                .textTheme
                .bodyText1
                .copyWith(color: blue05))
      ]),
      SizedBox(width: 6),
      Expanded(
          child: Padding(
            padding: EdgeInsets.only(top: icon == Images.phone.i() ? 3.0 : 0),
            child: SelectableText(info,
                style: bodyTextMedium, textAlign: TextAlign.right),
          ))
    ]);
  }

  Widget taxiServices(BuildContext context) {
    return Column(children: <Widget>[
      SizedBox(height: 20),
      Align(
          alignment: Alignment.centerLeft,
          child: rowTitle('Рекомендуемые сервисы такси')),
      SizedBox(height: 10),
      Row(children: <Widget>[
        IconButton(
            icon: SvgPicture.asset(Images.uber_taxi.i()),
            padding: EdgeInsets.all(0),
            onPressed: () {
              MapsLauncher.launchCoordinates(lat, lon, context);
            }),
        SizedBox(width: 10),
        IconButton(
            icon: SvgPicture.asset(Images.yandex_taxi.i()),
            padding: EdgeInsets.all(0),
            onPressed: () {
              MapsLauncher.launchCoordinates(lat, lon, context);
            })
      ])
    ]);
  }

//  MapController mapController = MapController();
  Widget personalInfo(BuildContext context) {
//    print('Personal: ${widget.churchModel[[
//      ChurchModel.latLonField,
//      LatLonModel.lonField
//    ]]}');
    return Column(children: <Widget>[
      SizedBox(height: 20),
      personalInfoCard(
//          SvgPicture.asset(Images.i())
          Images.phone.i(),
//          Icons.mobile_screen_share,
          'Телефон',
          '${widget.churchModel[[
            ChurchModel.ownerField,
            OwnerModel.phoneNumberField
          ]]}'),
      SizedBox(height: 10),
      Divider(color: dividerColor01, thickness: 1),
      SizedBox(height: 10),
      personalInfoCard(Images.location_marker.i(), 'Адрес',
          widget.churchModel[[(ChurchModel.addressField)]]),
      SizedBox(height: 20),
//        ClipRRect(
//            borderRadius: BorderRadius.circular(15),
//            child: CachedNetworkImage(
//              fit: BoxFit.cover,
//              width: double.infinity,
//              height: D().h(context) * 0.3,
//              imageUrl: widget.churchModel.get([ChurchModel.urlsField])[0],
////              "https://sib-catholic.ru/wp-content/uploads/2017/05/2017-05-18_14-53-24.png",
//              placeholder: (context, url) => CircularProgressIndicator(),
//              errorWidget: (context, url, error) => Icon(Icons.error),
//            )),
      ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: AspectRatio(
              aspectRatio: 1.3650306,
              child: GestureDetector(
                onTapDown: (tapped) =>
                    MapsLauncher.launchCoordinates(lat, lon, context),
                child: FlutterMap(
                    options: MapOptions(
                        center: LatLng(lat, lon),
                        zoom: 13.0,
                        interactive: false),
//                      minZoom: 8,
//                      maxZoom: 16
                    layers: [
                      TileLayerOptions(
                        urlTemplate: "https://api.tiles.mapbox.com/v4/"
                            "{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
                        additionalOptions: {
                          'accessToken':
                          'pk.eyJ1IjoiZmVodHkiLCJhIjoiY2s4dHZ0MzZvMDJnNTNtb2FwZTZhZGN5MCJ9.YtZRdkNz4Fxj7x_cPZ3WTA',
                          'id': 'mapbox.streets',
                        },
                      ),
                      MarkerLayerOptions(
                        markers: [
                          Marker(
                              width: 100.0,
                              height: 100.0,
                              point: LatLng(lat, lon),
                              builder: (ctx) =>
                                  Container(
                                      child: Icon(Icons.place,
                                          color: HexColor('#EA4335'),
                                          size: 35)))
                        ],
                      ),
                    ]),
              ))),
      SizedBox(height: 24),
    ]);
  }

  Widget floatingBottomButton(var icon,
      String title,
      Color firstGradientColor,
      Color secondGradientColor,
      Color textColor,
      String iconColor,
      bool opacity,
      Function onPressed) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 12),
        child: PhysicalModel(
            elevation: 3,
            borderRadius: BorderRadius.circular(11),
            color: Colors.transparent,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(11),
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 1, sigmaY: 1),
                    child: Opacity(
                      opacity: 0.93,
                      child: GradientButton(
                          onPressed: onPressed,
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 24, right: 22),
                              child: Row(children: <Widget>[
                                icon,
                                SizedBox(width: 14),
                                Text(title,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .bodyText1
                                        .copyWith(
                                        color: textColor,
                                        fontWeight: FontWeight.w500)),
                              ])),
                          gradientColor: [
                            firstGradientColor.withOpacity(
                                opacity == true ? 0.94 : 1),
                            secondGradientColor.withOpacity(
                                opacity == true ? 0.94 : 1)
                          ]),
                    )
                ))
        ));
  }

  Widget bottomButtonsListView(BuildContext context) {
    return SizedBox(
        height: 85,
        width: double.infinity,
        child: ListView(
//            shrinkWrap: true,
            padding: EdgeInsets.all(0),
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              SizedBox(width: 40),
//    Navigator.push(
//    context,
//    MaterialPageRoute(builder: (context) => SecondRoute()),
//    );
              floatingBottomButton(
                  SvgPicture.asset(Images.new_note.i()),
                  'Подать записку',
                  Color.fromRGBO(161, 165, 254, 0.85),
                  Color.fromRGBO(160, 126, 255, 0.85),
                  Colors.white,
                  '#FFFFFF',
                  true, () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SendTrebHealth()),
                );
              }),
              SizedBox(width: 12),
              floatingBottomButton(
                  SvgPicture.asset(Images.booking.i()),
                  'Забронировать',
                  Color.fromRGBO(255, 255, 255, 1),
                  Color.fromRGBO(255, 255, 255, 1),
                  Colors.black,
                  '#000000',
                  false, () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Booking()));
              }),
              SizedBox(width: 12),
              floatingBottomButton(
                  SvgPicture.asset(Images.ruble.i()),
                  'Пожертвовать',
                  Color.fromRGBO(255, 255, 255, 1),
                  Color.fromRGBO(255, 255, 255, 1),
                  Colors.black,
                  '#000000',
                  false, () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Donation()),
                );
              }),
              SizedBox(width: 23)
            ]));
  }

  bool playing = true;

  bool playerOptionTap = false;

  Widget bottomPlayer() {
    Widget invisibleBackButton = Opacity(
        opacity: 0, child: SizedBox(height: 0, child: BackButtonRow()));
    Widget player() {
      return Expanded(
          child: PhysicalModel(
              elevation: 3,
              borderRadius: BorderRadius.circular(12),
              color: Colors.transparent,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: BackdropFilter(
                      filter: ImageFilter.blur(
                          sigmaX: 1, sigmaY: 2),
                      child: Container(
                          color: Colors.white.withOpacity(0.94),
                          child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(12)),
                                  onTap: playerOptionTap == false
                                      ? () {
                                    _openPageByName =
                                        globalCurrentAudioType.value;
                                    pc.animatePanelToPosition(1);
                                  }
                                      : null,
                                  child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 16,
                                          right: 12,
                                          bottom: 14,
                                          top: 14),
                                      child: Row(
                                          mainAxisAlignment: MainAxisAlignment
                                              .spaceBetween,
                                          children: <Widget>[
                                            Flexible(
                                                child: ValueListenableBuilder(
                                                    valueListenable: globalLastChosenAudio,
                                                    builder: (
                                                        BuildContext context,
                                                        MapEntry<String,
                                                            dynamic> value,
                                                        Widget child) =>
                                                        Text(
                                                            value.value?.get(
                                                                [
                                                                  PodcastModel
                                                                      .nameField
                                                                ])
                                                                .toString(),
                                                            style: playerTextStyleBold)
                                                )),
                                            ValueListenableBuilder(
                                                valueListenable:
                                                mindClass
                                                    .globalPodcastIsPlaying,
                                                builder: (BuildContext context,
                                                    bool podcastIsPlaying,
                                                    Widget child) =>
                                                    ValueListenableBuilder(
                                                        valueListenable: mindClass
                                                            .globalStoryIsPlaying,

                                                        builder: (
                                                            BuildContext context,
                                                            bool storyIsPlaying,
                                                            Widget child) =>
                                                            GestureDetector(
                                                                onTapDown: (
                                                                    tap) =>
                                                                    setState(() =>
                                                                    playerOptionTap =
                                                                    true),
                                                                onTapCancel: () =>
                                                                    setState(() =>
                                                                    playerOptionTap =
                                                                    false),
                                                                child: Row(
                                                                    mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                    children: <
                                                                        Widget>[
                                                                      GradientIconButton(
                                                                          iconButton: IconButton(
                                                                              visualDensity:
                                                                              VisualDensity
                                                                                  .compact,
                                                                              iconSize: 30,
                                                                              padding: EdgeInsets
                                                                                  .all(
                                                                                  0),
                                                                              icon:
                                                                              SvgPicture
                                                                                  .asset(
                                                                                  Images
                                                                                      .backward_media
                                                                                      .i(),
                                                                                  height: 13,
                                                                                  width: 13),
                                                                              onPressed: globalCurrentAudioType
                                                                                  .value ==
                                                                                  DatabasePaths
                                                                                      .podcast &&
                                                                                  globalLastChosenAudio
                                                                                      .value
                                                                                      .key ==
                                                                                      mindClass
                                                                                          .globalPodcasts
                                                                                          .value
                                                                                          .values
                                                                                          .first
                                                                                          .key
                                                                                  ? null
                                                                                  : globalCurrentAudioType
                                                                                  .value ==
                                                                                  DatabasePaths
                                                                                      .story &&
                                                                                  globalLastChosenAudio
                                                                                      .value
                                                                                      .key ==
                                                                                      mindClass
                                                                                          .globalStories
                                                                                          .value
                                                                                          .values
                                                                                          .first
                                                                                          .key
                                                                                  ? null
                                                                                  : globalCurrentAudioType
                                                                                  .value ==
                                                                                  DatabasePaths
                                                                                      .podcast
                                                                                  ? () =>
                                                                                  mindClass
                                                                                      .podcastBackwardTap()
                                                                                  : () =>
                                                                                  mindClass
                                                                                      .storyBackwardTap()),
                                                                          gradientColors: globalCurrentAudioType
                                                                              .value ==
                                                                              DatabasePaths
                                                                                  .podcast &&
                                                                              globalLastChosenAudio
                                                                                  .value
                                                                                  .key ==
                                                                                  mindClass
                                                                                      .globalPodcasts
                                                                                      .value
                                                                                      .values
                                                                                      .first
                                                                                      .key
                                                                              ? playerOptionDisabledGradient
                                                                              : globalCurrentAudioType
                                                                              .value ==
                                                                              DatabasePaths
                                                                                  .story &&
                                                                              globalLastChosenAudio
                                                                                  .value
                                                                                  .key ==
                                                                                  mindClass
                                                                                      .globalStories
                                                                                      .value
                                                                                      .values
                                                                                      .first
                                                                                      .key
                                                                              ? playerOptionDisabledGradient
                                                                              : darkGradient),
                                                                      GradientIconButton(
                                                                          iconButton: IconButton(
                                                                              visualDensity:
                                                                              VisualDensity
                                                                                  .compact,
                                                                              padding:
                                                                              EdgeInsets
                                                                                  .all(
                                                                                  0),
                                                                              iconSize: 45,
                                                                              icon: SvgPicture
                                                                                  .asset(

                                                                                  storyIsPlaying ==
                                                                                      false &&
                                                                                      podcastIsPlaying ==
                                                                                          false
                                                                                      ? Images
                                                                                      .play_media
                                                                                      .i()
                                                                                      : Images
                                                                                      .pause_media
                                                                                      .i(),
                                                                                  height: 25,
                                                                                  width: 25),
                                                                              onPressed: () {
                                                                                if (globalCurrentAudioType
                                                                                    .value ==
                                                                                    DatabasePaths
                                                                                        .podcast &&
                                                                                    !podcastIsPlaying) {
                                                                                  mindClass
                                                                                      .podcastPlayer
                                                                                      .resume();
                                                                                } else
                                                                                if (globalCurrentAudioType
                                                                                    .value ==
                                                                                    DatabasePaths
                                                                                        .podcast &&
                                                                                    podcastIsPlaying) {
                                                                                  mindClass
                                                                                      .podcastPlayer
                                                                                      .pause();
                                                                                } else
                                                                                if (globalCurrentAudioType
                                                                                    .value ==
                                                                                    DatabasePaths
                                                                                        .story &&
                                                                                    !storyIsPlaying) {
                                                                                  mindClass
                                                                                      .storyPlayer
                                                                                      .resume();
                                                                                } else
                                                                                if (globalCurrentAudioType
                                                                                    .value ==
                                                                                    DatabasePaths
                                                                                        .story &&
                                                                                    storyIsPlaying) {
                                                                                  mindClass
                                                                                      .storyPlayer
                                                                                      .pause();
                                                                                }
                                                                              }),
                                                                          gradientColors:
                                                                          darkGradient
                                                                      ),
                                                                      GradientIconButton(
                                                                          iconButton: IconButton(
                                                                              visualDensity:
                                                                              VisualDensity
                                                                                  .compact,
                                                                              padding: EdgeInsets
                                                                                  .all(
                                                                                  0),
                                                                              iconSize: 30,
                                                                              icon: SvgPicture
                                                                                  .asset(
                                                                                  Images
                                                                                      .forward_media
                                                                                      .i(),
                                                                                  height: 13,
                                                                                  width: 13),
                                                                              onPressed: globalCurrentAudioType
                                                                                  .value ==
                                                                                  DatabasePaths
                                                                                      .podcast &&
                                                                                  globalLastChosenAudio
                                                                                      .value
                                                                                      .key ==
                                                                                      mindClass
                                                                                          .globalPodcasts
                                                                                          .value
                                                                                          .values
                                                                                          .last
                                                                                          .key
                                                                                  ? null
                                                                                  : globalCurrentAudioType
                                                                                  .value ==
                                                                                  DatabasePaths
                                                                                      .story &&
                                                                                  globalLastChosenAudio
                                                                                      .value
                                                                                      .key ==
                                                                                      mindClass
                                                                                          .globalStories
                                                                                          .value
                                                                                          .values
                                                                                          .last
                                                                                          .key
                                                                                  ? null
                                                                                  : globalCurrentAudioType
                                                                                  .value ==
                                                                                  DatabasePaths
                                                                                      .podcast
                                                                                  ? () =>
                                                                                  mindClass
                                                                                      .podcastForwardTap()
                                                                                  : () =>
                                                                                  mindClass
                                                                                      .podcastForwardTap()),
                                                                          gradientColors: globalCurrentAudioType
                                                                              .value ==
                                                                              DatabasePaths
                                                                                  .podcast &&
                                                                              globalLastChosenAudio
                                                                                  .value
                                                                                  .key ==
                                                                                  mindClass
                                                                                      .globalPodcasts
                                                                                      .value
                                                                                      .values
                                                                                      .last
                                                                                      .key
                                                                              ? playerOptionDisabledGradient
                                                                              : globalCurrentAudioType
                                                                              .value ==
                                                                              DatabasePaths
                                                                                  .story &&
                                                                              globalLastChosenAudio
                                                                                  .value
                                                                                  .key ==
                                                                                  mindClass
                                                                                      .globalStories
                                                                                      .value
                                                                                      .values
                                                                                      .last
                                                                                      .key
                                                                              ? playerOptionDisabledGradient
                                                                              : darkGradient)
                                                                    ])
                                                            )
                                                    )
                                            )
                                          ])))
                          )
                      )
                  )
              )
          ));
    }
    return SizedBox(
        width: D().w(context) < 500 ? D().w(context) : D().w(context) *
            0.6,
        child: Padding(
            padding: EdgeInsets.only(
                right: D().w(context) < 500 ? 22 : 0),
            child: ValueListenableBuilder<MapEntry<String, dynamic>>(
                valueListenable: globalLastChosenAudio,
                builder: (context, value, child) =>
                    AnimatedOpacity(
                        duration: Duration(milliseconds: 300),
                        opacity:
                        value.value?.get([PodcastModel.nameField]) == null
                            ? 1
                            : 1,
                        child: IgnorePointer(
                            ignoring:
                            value.value?.get([PodcastModel.nameField]) ==
                                null
                                ? true
                                : false,
                            child: Row(
                                mainAxisSize: MainAxisSize.max, children: [
                              invisibleBackButton,
                              player()
                            ]))))));
  }

  Widget buildBody(BuildContext context) {
    Widget churchTitle = Align(
        alignment: Alignment.centerLeft,
        child: Text(widget.churchModel[[(ChurchModel.nameField)]],
            style: Theme
                .of(context)
                .textTheme
                .headline6)
    );

//    Widget bottomPlayer() {
//      return Visibility(
//        visible: false,
//        child: Container(
//            height: D().w(context) *
//                (PythT().calcDiag(196, 67) / PythT().calcDiag(375, 812)),
//            width: D().w(context) * 0.845,
//            child: Card(
//                elevation: 4,
//                shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.all(Radius.circular(16))),
//                child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      SizedBox(width: 16),
//                      Text('The Dark Side',
//                          style: TextStyle(fontSize: 18, color: Colors.black)),
//                      Row(
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          children: <Widget>[
//                            Container(
//                                child: IconButton(
//                                    visualDensity: VisualDensity.compact,
//                                    iconSize: 30,
//                                    padding: EdgeInsets.all(0),
//                                    icon: Icon(Icons.fast_rewind,
//                                        color: HexColor('#393D79')),
//                                    onPressed: () {})),
//                            Container(
//                                child: IconButton(
//                                    padding: EdgeInsets.all(0),
//                                    visualDensity: VisualDensity.compact,
//                                    iconSize: 45,
//                                    icon: Icon(
//                                        playing
//                                            ? Icons.pause
//                                            : Icons.play_arrow,
//                                        color: HexColor('#393D79')),
//                                    onPressed: () {
//                                      if (playing == false)
//                                        setState(() => playing = true);
//                                      else
//                                        setState(() => playing = false);
//                                    })),
//                            Container(
//                                child: IconButton(
//                                    padding: EdgeInsets.all(0),
//                                    visualDensity: VisualDensity.compact,
//                                    iconSize: 30,
//                                    icon: Icon(Icons.fast_forward,
//                                        color: HexColor('#393D79')),
//                                    onPressed: () {}))
//                          ])
//                    ]))),
//      );
//    }

//    return Container(color: Colors.blue);
    return Stack(overflow: Overflow.visible, children: <Widget>[
      SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                BackButtonRow(),
                Expanded(
                    child: Column(children: <Widget>[
                      SizedBox(height: 38),
                      imageSection(context),
                      SizedBox(height: 8),
                      Padding(
                          padding: const EdgeInsets.only(right: 24.0),
                          child: Column(children: <Widget>[
                            churchTitle,
                            priest(),
                            churchDescription(),
                            excursionReference(),
                            personalInfoCard(
                                Images.location.i(),
                                'Сувениры',
                                '${widget.churchModel[[
                                  ChurchModel.souvenirAddressField
                                ]]}'),
                            taxiServices(context),
                            personalInfo(context),
                          ])),
                      SizedBox(height: globalLastChosenAudio.value.key == null
                          ? 100
                          : 170),
//                      SizedBox(height: D().w(context) * 0.35)
                    ]))
              ])),
      Positioned(
          bottom: 20,
          left: 0,
          right: 0,
          child: Column(children: <Widget>[
            bottomButtonsListView(context),
            Visibility(
                visible: globalLastChosenAudio.value.key == null ? false : true,
                child: bottomPlayer())
          ]))
    ]);
  }
}

class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() =>
      new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;
  bool flag = true;
  double opacity = 0;

  @override
  void initState() {
    super.initState();
    if (widget.text.length > 100) {
      firstHalf = widget.text.substring(0, 100);
      secondHalf = widget.text.substring(100, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: D().w(context) > 600 ? Text(firstHalf + secondHalf) : secondHalf
            .isEmpty
            ? Text(firstHalf)
            : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AnimatedCrossFade(
                duration: Duration(milliseconds: 150),
                crossFadeState: flag
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                firstChild: Text((firstHalf + "..."),
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: black3108)),
                secondChild: Text(firstHalf + secondHalf,
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: black3108)),
              ),
//                    AnimatedOpacity(
//                      duration: Duration(milliseconds: 1000),
//                      opacity: opacity,
//                      child: Text(
//                          flag ? (firstHalf + "...") : (firstHalf + secondHalf),
//                          style: Theme.of(context)
//                              .textTheme
//                              .bodyText1
//                              .copyWith(color: black3108)),
//                    ),
//                  Text(
//                      flag ? (firstHalf + "...") : (firstHalf + secondHalf),
//                      style: Theme.of(context)
//                          .textTheme
//                          .bodyText1
//                          .copyWith(color: black3108)),
              InkWell(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6.0),
                      child: Text(flag ? "+ Дальше" : "- Скрыть",
                          style: Theme
                              .of(context)
                              .textTheme
                              .subtitle2
                              .copyWith(color: blue))),
                  onTap: () {
                    setState(() {
                      flag = !flag;
                      if (opacity == 0)
                        opacity = 1;
                      else
                        opacity = 0;
                    });
                  })
            ]));
  }
}

//  Widget churchDescription() {
//    return Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          DescriptionTextWidget(text: fullText,),
////          Text(
////              isShrinkedText == true
////                  ? widget.churchModel.get([ChurchModel.aboutField])
////                  : widget.churchModel.get([ChurchModel.aboutField]),
////              style: TextStyle(fontSize: 18, color: HexColor('5A5A5A'))),
//          SizedBox(height: 10),
//          GestureDetector(
//              onTap: () {
//                setState(() {
//                  isShrinkedText = !isShrinkedText;
//                });
//              },
//              child: Text(isShrinkedText == true ? '+ Дальше' : '- Скрыть',
//                  style: TextStyle(
//                      fontSize: 14,
//                      color: HexColor('#3036CB'),
//                      fontWeight: FontWeight.w600)))
//        ]);
//  }
// checkBookmark();
//                       isBookmarked = await mindClass.isBookmarkedPage('first_church');
//                         print('Variable is: ${}');
//                          mindClass.isBookmarkedPage('first_church').then((value) => print('Variable is: ${value}'));
//                        Future.value(mindClass.isBookmarkedPage('first_church')).then((value) => print('Variable is: ${value}'));
//                          print(
//                              'isBookmarked: ${mindClass.isBookmarkedPage('first_church')}');
//                          print(
//                              'isBookmarked: ${Future.value(mindClass.isBookmarkedPage('first_church')).then((value) => value.toString())}');
//                         setState(() async {
//                           isBookmarked = await mindClass.isBookmarkedPage('first_church');
//
//                         });
//                          UserModel user = UserModel();
//                          user.key = mindClass.user.uid;
//                          await user.reload();
//                          var response = user[[
//                            UserModel.churchesField,
//                            BookmarkedChurchModel.bookmarkField.key('first_church')
//                          ]];
//                          print('is Bookm: ${response.toString()}');
//                          print(mindClass.isBookmarkedPage('first_church'));
//                          setState(() => isBookmarked =
//                              mindClass.isBookmarked('first_church'));
//                          UserModel user = UserModel();
//                          user.key = mindClass.user.uid;
//                          user.set([
//                            UserModel.churchesField,
//                            BookmarkedChurchModel.bookmarkField.key('first_church')
//                          ], true);

//                          UserModel user = UserModel();
//                          user.key = mindClass.user.uid;
//                          await user.reload();
//                          print('Data is: ${user[[
//                            UserModel.churchesField,
//                            BookmarkedChurchModel.bookmarkField.key('first_church')
//                          ]]}');
//                          print('Uid is: ${mindClass.user.uid}');
//                          if (user[[
//                                UserModel.churchesField,
//                                BookmarkedChurchModel.bookmarkField
//                                    .key('first_church')
//                              ]] == false) {
//                            user.set([
//                              UserModel.churchesField,
//                              BookmarkedChurchModel.bookmarkField
//                                  .key('first_church')
//                            ], true);
//                          } else {
//                            user.set([
//                              UserModel.churchesField,
//                              BookmarkedChurchModel.bookmarkField
//                                  .key('first_church')
//                            ], false);
//                          }
//                          user.set([
//                              UserModel.churchesField,
//                              BookmarkedChurchModel.bookmarkField
//                                  .key('first_church')
//                            ], false);
//                          await user.upload();
//                              mindClass.isBookmarkedPage('first_church') ==
//                                      false
//                                  ? mindClass.getI() + 'bookmark_filled.svg'
//                                  : mindClass.getI() + 'bookmark.svg'),
//                          child: SvgPicture.asset(isBookmarked
//                              ? mindClass.getI() + 'bookmark_filled.svg'
//                              : mindClass.getI() + 'bookmark.svg'),
