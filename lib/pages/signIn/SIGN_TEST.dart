import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../utils/hex_color.dart';

class SignTest extends StatefulWidget {
  @override
  _SignTestState createState() => _SignTestState();
}

class _SignTestState extends State<SignTest>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: buildAppBar(), body: buildBody());
  }

  ScrollController _scrollController;
  bool upDirection = true, flag = true;
  bool show = false;
  TabController _tabController;

  Timer showTabsTimer;
  bool showTabs = true;
  double showOffset = 0;

  timer() {
    makeItOn() {
      setState(() {
        showTabsTimer = null;
        showTabs = true;
      });
    }

    if (showTabsTimer == null)
      setState(() {
        showTabs = false;
      });
    if (showTabsTimer != null) showTabsTimer.cancel();
    showTabsTimer = Timer(Duration(milliseconds: 100), makeItOn);
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController
      ..addListener(() {
        upDirection = _scrollController.position.userScrollDirection ==
            ScrollDirection.forward;
        //print(_scrollController.position);
        // makes sure we don't call setState too much, but only when it is needed
        showOffset = _scrollController.offset;
        if (upDirection != flag) setState(() {});
        show = upDirection == false || _scrollController.offset > 0;
        setState(() {});
        flag = upDirection;
        timer();
      });
    _tabController = TabController(vsync: this, length: 2, initialIndex: 1);

    _tabController.addListener(() {
      //_getItems(_tabController.index);
    });
  }

  Widget buildAppBar() {
    return AppBar(
      flexibleSpace: SafeArea(
//            child: Directionality(
//              textDirection: TextDirection.rtl,
        child: Container(
          color: Colors.white,
          child: TabBar(
//   isScrollable: true,
            controller: _tabController,
            tabs: [
              Container(
                child: Tab(
                  child: Text('Регистрация'),
                ),
              ),
              Container(
                child: Tab(
                  child: Text('Вход'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 100, left: 50, right: 50),
        child: ListView(
          children: <Widget>[
            RichText(
              text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  children: [
                    TextSpan(text: 'С ', style: TextStyle(fontSize: 28)),
                    TextSpan(
                        text: 'Возвращением',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28)),
                  ]),
            ),
            Container(
              padding: EdgeInsets.only(top: 50, bottom: 30),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(hintText: 'Email'),
                  ),
                  TextField(
                    decoration: InputDecoration(hintText: 'Пароль'),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SvgPicture.asset('assets/icons/search.svg'),
                Spacer(),
                SvgPicture.asset('assets/icons/cart.svg'),
                Spacer(),
                SvgPicture.asset('assets/icons/hamburger.svg'),
                Spacer(
                  flex: 3,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Text(
                    'Пропустить',
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
              ],
            ),
            Padding(
                padding: const EdgeInsets.only(top: 80.0),
                child: Container(
                  height: 80,
                  child: Stack(
                    fit: StackFit.loose,
                    children: <Widget>[
                      Positioned(
                        bottom: 80,
                        left: 10,
                        child: Text(
                          'Забыл пароль?',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                      Container(
                        color: HexColor('#EAE7F4'),
                      ),
                      Positioned(
                        bottom: 60,
                        left: 150,
                        child: FlatButton(
                          color: HexColor('#A1A4FE'),
                          textColor: Colors.white,
                          splashColor: Colors.purple,
                          padding: EdgeInsets.all(16),
                          onPressed: () {},
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Text(
                            'Вход',
                          ),
                        ),
                      ),
                    ],
                    overflow: Overflow.visible,
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
