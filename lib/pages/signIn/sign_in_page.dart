import 'package:align_positioned/align_positioned.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/head_module/route.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/hex_color.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

import '../../head_module/database_extensions/auth_extension.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> with SingleTickerProviderStateMixin {
  TabController _tabController;
  int _currentIndex = 0;
  Widget myAnimatedWidget = Text('');
  BuildContext buildContext;

  GlobalKey<ScaffoldState> pageKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2, initialIndex: 0);
    _tabController.addListener(() {
      setState(() {
        _currentIndex = _tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    this.buildContext = context;
    return Scaffold(
      body: buildBody(context),
      key: pageKey,
    );
  }

  Widget tabBar() {
    return SizedBox(
      height: 50,
      child: Align(
        alignment: Alignment.topRight,
        child: TabBar(
            isScrollable: true,
            labelStyle: tabBarThemeSelected,
            unselectedLabelStyle: tabBarThemeUnselected,
            indicator:
                CircleTabIndicator(color: HexColor('#8079E4'), radius: 3.5),
            controller: _tabController,
            tabs: [Tab(child: Text('Вход')), Tab(child: Text('Регистрация'))]),
      ),
    );
  }

  Widget title(bool isSignIn) {
    if (isSignIn == true)
      return Align(
        alignment: Alignment.centerLeft,
        child: RichText(
            text: TextSpan(children: [
          TextSpan(
              text: 'С ',
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(fontWeight: FontWeight.w300)),
          TextSpan(
              text: 'Возвращением',
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(fontWeight: FontWeight.w600))
        ])),
      );
    else
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Приветствуем',
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    .copyWith(fontWeight: FontWeight.w600)),
            SizedBox(height: 10),
            Text('Заполните информацию ниже или зайдите с помощью соц. сетей',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(fontWeight: FontWeight.w500))
          ]);
  }

  Widget textInputField(String hintText) {
    return Column(
      children: <Widget>[
        TextField(
            enabled: true,
            style: signInTextStyle,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
//                    borderRadius:
//                     BorderRadius.all(const Radius.circular(15.0)),
                    borderSide: BorderSide(color: dividerColor02, width: 1)),
                focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: dividerColor01, width: 1)),
                hintText: hintText,
                hintStyle: signInTextStyle.copyWith(color: black0004))),
        SizedBox(height: 12)
      ],
    );
  }

  Widget socialNetworks() {
    return Row(
      children: <Widget>[
        Row(children: <Widget>[
          IconButton(
              visualDensity: VisualDensity.compact,
              icon: SvgPicture.asset(Images.social_vk.i()),
              padding: EdgeInsets.all(0),
              onPressed: () {}),
          SizedBox(width: 12),
          IconButton(
              visualDensity: VisualDensity.compact,
              icon: SvgPicture.asset(Images.social_gplus.i()),
              padding: EdgeInsets.all(0),
              onPressed: () async {
                showLoadingPage(pageKey);
                FirebaseUser user = await mindClass.signInWithGoogle();
                Navigator.pop(context);
                if ((user) != null) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      Pages.profile(), (Route<dynamic> route) => route.isFirst);
                }
              }),
          SizedBox(width: 10),
          IconButton(
              visualDensity: VisualDensity.compact,
              icon: SvgPicture.asset(Images.social_facebook.i()),
              padding: EdgeInsets.all(0),
              onPressed: () async {
                showLoadingPage(pageKey);
                FirebaseUser user = await mindClass.signInWithFacebook();
                Navigator.pop(context);
                if ((user) != null) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      Pages.profile(), (Route<dynamic> route) => route.isFirst);
                  setState(() {});
                }
              }),
        ]),
        Spacer(),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Пропустить',
                style:
                    signInTextStyle.copyWith(fontSize: 18, color: black0005)),
          ),
          onTap: () => Navigator.pop(buildContext),
        )
      ],
    );
  }

  Widget bottomSection(bool isSignIn) {
    return SizedBox(
      height: 130,
      child: Stack(children: <Widget>[
        Positioned.fill(
            child: Container(
                width: double.maxFinite,
                color: isSignIn == true
                    ? HexColor('#EAE7F4')
                    : HexColor('#F4E7ED'))),
        AlignPositioned(
            alignment: Alignment.topRight,
            moveByChildHeight: -0.5,
            childHeightRatio: 0.5,
            minChildWidth: 130,
            dx: -20,
            touch: Touch.inside,
            child: RaisedButton(
                color:
                    isSignIn == true ? HexColor('#A1A4FE') : HexColor('E67482'),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(14.0)),
                child: Text(isSignIn == true ? 'ВХОД' : 'РЕГИСТРАЦИЯ',
                    style: signInButtonTextStyle),
                onPressed: () {}))
      ]),
    );
  }

//  void _onHorizontalDrag(DragEndDetails details) {
//    if (details.primaryVelocity == 0)
//      return; // user have just tapped on screen (no dragging)
//
//    if (details.primaryVelocity.compareTo(0) == -1)
//      setState(() {
//        gestIndex = true;
//      });
//    else
//      setState(() {
//        gestIndex = false;
//      });
//  }
  Widget decoratedBox(bool gradient) {
    return Opacity(
        opacity: 0.1,
        child: Container(
            height: D().w(context) * 0.27,
            width: D().w(context) * 0.27,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: gradient == false
                        ? [HexColor('#827AE4'), HexColor('#827AE4')]
                        : [HexColor('#F1967C'), HexColor('#CE2E8E')]),
                borderRadius: BorderRadius.all(Radius.circular(20)))));
  }

  Widget smallSignIn() {
    return SizedBox(
      height: 600,
      child: Column(children: [
        Stack(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 36.0, top: 50, right: 24),
              child: Column(children: <Widget>[
                SizedBox(height: 60),
                title(true),
                SizedBox(height: 45),
                textInputField('Email'),
                SizedBox(height: 18),
                socialNetworks(),
                SizedBox(height: 70)
              ])),
          decoratedBox(false),
          Positioned(top: 50, left: -50, child: decoratedBox(true)),
          Positioned(top: 120, right: 0, child: decoratedBox(true))
        ]),
//SizedBox(height: MediaQuery.of(context).size.height * 0.35),
        Spacer(),
        bottomSection(true)
      ]),
    );
  }

  Widget smallSignUp() {
    return SizedBox(
      height: 600,
      child: Column(children: <Widget>[
        Stack(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 36.0, top: 50, right: 24),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 40),
                    title(false),
                    SizedBox(height: 40),
                    textInputField('Email'),
                    SizedBox(height: 18),
                    socialNetworks(),
                    SizedBox(height: 100),
                  ])),
          Positioned(top: 20, left: -50, child: decoratedBox(true)),
          Positioned(top: 60, right: 0, child: decoratedBox(true)),
          Positioned(top: 120, right: 50, child: decoratedBox(false))
        ]),
        Spacer(),
        bottomSection(false)
      ]),
    );
  }

  Widget signIn() {
    return Expanded(
      child: Column(children: [
        Stack(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 36.0, top: 50, right: 24),
              child: Column(children: <Widget>[
                D().h(context) < 600
                    ? SizedBox(height: 0)
                    : SizedBox(height: 60),
                title(true),
                SizedBox(height: 45),
                textInputField('Email'),
                SizedBox(height: 18),
                socialNetworks(),
                SizedBox(height: 70)
              ])),
          decoratedBox(false),
          Positioned(top: 50, left: -50, child: decoratedBox(true)),
          Positioned(top: 120, right: 0, child: decoratedBox(true))
        ]),
//SizedBox(height: MediaQuery.of(context).size.height * 0.35),
        Spacer(),
        bottomSection(true)
      ]),
    );
  }

  Widget signUp() {
    return Expanded(
      child: Column(children: <Widget>[
        Stack(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 36.0, top: D().h(context) < 600 ? 45 : 50, right: 24),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    D().h(context) < 600
                        ? SizedBox(height: 0)
                        : SizedBox(height: 40),
                    title(false),
                    SizedBox(height: 40),
                    textInputField('Email'),
                    SizedBox(height: 18),
                    socialNetworks(),
//                    SizedBox(height: 100),
                  ])),
          Positioned(top: 20, left: -50, child: decoratedBox(true)),
          Positioned(top: 60, right: 0, child: decoratedBox(true)),
          Positioned(top: 120, right: 50, child: decoratedBox(false))
        ]),
        Spacer(),
        bottomSection(false)
      ]),
    );
  }

  Widget buildBody(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.landscape)
      return AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          child: ListView(key: ValueKey(_currentIndex), children: <Widget>[
            tabBar(),
            _currentIndex == 0 ? smallSignIn() : smallSignUp()
          ]));
    else
      return AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          child: Column(key: ValueKey(_currentIndex), children: <Widget>[
            SizedBox(height: 12),
            tabBar(),
            _currentIndex == 0 ? signIn() : signUp()
          ]));
  }
}

class CircleTabIndicator extends Decoration {
  final BoxPainter _painter;

  CircleTabIndicator({@required Color color, @required double radius})
      : _painter = _CirclePainter(color, radius);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _CirclePainter extends BoxPainter {
  final Paint _paint;
  final double radius;

  _CirclePainter(Color color, this.radius)
      : _paint = Paint()
          ..color = color
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset circleOffset =
        offset + Offset(cfg.size.width / 2, cfg.size.height - radius - 5);
    canvas.drawCircle(circleOffset, radius, _paint);
  }
}
