import 'package:church/pages/payment/payment_page.dart';
import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_bottom_button.dart';
import 'package:church/widgets/gradient_card.dart';
import 'package:church/widgets/gradient_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import '../../utils/hex_color.dart';
import '../../utils/relative_dimensions.dart';

class Cart extends StatefulWidget {
  @override
  CartState createState() => CartState();
}

class CartState extends State<Cart> {
  GlobalKey<AnimatedListState> listKey;
  int total = 1050;
  int kof = 0;

  @override
  void initState() {
    listKey = GlobalKey();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: buildBody(context),
        bottomSheet: AnimatedContainer(
          duration: Duration(milliseconds: opacityAnimation),
          height: listIsEmpty ? 0 : null,
          child: GradientBottomButton(
              text: 'Оформить'.toUpperCase(),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Payment()),
                );
              }),
        ));
  }

  var healthTypeGradient = [HexColor('#F1967C'), HexColor('#CE2E8E')];
  var reposeTypeGradient = [HexColor('#5E5095'), HexColor('#393D79')];
  var topButtonGradient = [HexColor('#9783E6'), HexColor('#666DE1')];

  List<CardModel> cardModelList = [
    CardModel(
        id: 0,
        churchTitle: 'Преображенский мужской монастырь',
        price: 300,
        applicationType: 'О Здравии',
        applicationTypeIsHealth: true,
        lasting: 'Сорокоуст 40 дней',
        names: 'Анна, Алексий, Александр Александров, Петр Петров'),
    CardModel(
        id: 1,
        churchTitle: 'Преображенский мужской монастырь',
        price: 450,
        applicationType: 'О Упокоении',
        applicationTypeIsHealth: false,
        lasting: 'Сорокоуст 120 дней',
        names: 'Анна, Алексий, Александр Александров, Петр Петров'),
    CardModel(
        id: 2,
        churchTitle: 'Преображенский мужской монастырь',
        price: 300,
        applicationType: 'О Здравии',
        applicationTypeIsHealth: true,
        lasting: 'Сорокоуст 40 дней',
        names: 'Анна, Алексий, Александр Александров, Петр Петров'),
//    CardModel(
//        id: 3,
//        churchTitle: 'Преображенский мужской монастырь',
//        price: 450,
//        applicationType: 'О Упокоении',
//        applicationTypeIsHealth: false,
//        lasting: 'Сорокоуст 120 дней',
//        names: 'Анна, Алексий, Александр Александров, Петр Петров'),
//    CardModel(
//        id: 4,
//        churchTitle: 'Преображенский мужской монастырь',
//        price: 300,
//        applicationType: 'О Здравии',
//        applicationTypeIsHealth: true,
//        lasting: 'Сорокоуст 40 дней',
//        names: 'Анна, Алексий, Александр Александров, Петр Петров'),
//    CardModel(
//        id: 5,
//        churchTitle: 'Преображенский мужской монастырь',
//        price: 450,
//        applicationType: 'О Упокоении',
//        applicationTypeIsHealth: false,
//        lasting: 'Сорокоуст 120 дней',
//        names: 'Анна, Алексий, Александр Александров, Петр Петров'),
  ];

  bool listIsEmpty = false;
  int opacityAnimation = 450;

  Widget topSection() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('Корзина', style: Theme.of(context).textTheme.headline5),
          AnimatedOpacity(
            opacity: listIsEmpty == true ? 0 : 1,
            duration: Duration(milliseconds: opacityAnimation),
            child: InkWell(
                onTap: !listIsEmpty
                    ? () {
                  setState(() {
                    listIsEmpty = true;

//                    Future.delayed(Duration(milliseconds: opacityAnimation))
//                        .then((value) {
//
//                    });
                  });
                  Future.delayed(Duration(milliseconds: opacityAnimation))
                      .then((value) {
                    setState(() {
                      cardModelList.clear();
                      total = 0;
                    });
                  });
                }
                    : null,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 4),
                  child: GradientText(
                      cardModelList.length == 0 ? '' : 'Очистить',
                      gradientColors: bottomButtonGradient,
                      textStyle: Theme
                          .of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: Colors.white)),
                )),
          )
        ]);
  }

  Widget bottomSection() {
//    int newSum = 0;
//    for (var i = 0; i < cardModelList.length; i++)
//      newSum = newSum + cardModelList[i].price;
//    total = newSum;
//    print('Total is: $total');
    return Align(
            alignment: Alignment.bottomRight,
        child: AnimatedOpacity(
          duration: Duration(milliseconds: 300),

          opacity: listIsEmpty ? 0 : 1,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                SizedBox(height: 12),
                Container(
                    width: 200,
                    child: Divider(thickness: 1, color: dividerColor01)),
                SizedBox(height: 6),
                Text('Общая сумма',
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: black0005)),
                SizedBox(height: 12),
                Text('$total₽', style: Theme
                    .of(context)
                    .textTheme
                    .headline5)
              ]),
        ));
  }

  void onItemRemoved(int index) {
    setState(() {
//      cardModelList.removeAt(index);

      total = total - cardModelList[index].price;
      if (total == 0) listIsEmpty = true;
    });
//    kof += 1;
//    setState(() {});
//    setState(() {
//    });
//    setState(() {
//      print('List length is: ${cardModelList.length}');
//    });
  }

  Widget buildBody(BuildContext context) {
    return Stack(children: [
      SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <
              Widget>[
            BackButtonRow(),
            Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 65),
                    Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: topSection()),
                    SizedBox(height: 8),
//                                Column(children: <Widget>[
//                                  for (var i = 0; i < cardModelList.length; i++)
//                                    CardWidget(cardModelList[i], cardModelList,
//                                        total, onItemRemoved)
//                                ]),
                    if (D().w(context) < 600)
                      AnimatedOpacity(
                          duration: Duration(milliseconds: opacityAnimation),
                          opacity: listIsEmpty == true ? 0 : 1,
                          child: ListView.builder(
                              padding: EdgeInsets.all(0),
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: cardModelList.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  CardWidget(
                                      cardModelList[index],
                                      cardModelList,
                                      total,
                                      onItemRemoved,
                                      index + kof))),
                    if (D().w(context) > 599)
                      Center(
                        child: AnimatedOpacity(
                            duration: Duration(milliseconds: opacityAnimation),
                            opacity: listIsEmpty == true ? 0 : 1,
                            child: Wrap(
                              spacing: 6,
                              children: <Widget>[
                                for (int index = 0;
                                index < cardModelList.length;
                                index++)
//                                  Container(color:Colors.blue, height: 200, width: 200,)
                                  SizedBox(
                                    width: D().w(context) * 0.4,
                                    child: CardWidget(
                                        cardModelList[index],
                                        cardModelList,
                                        total,
                                        onItemRemoved,
                                        index + kof),
                                  )
                              ],
                            )),
                      ),
                    Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: bottomSection()),
                    SizedBox(height: 100),
                  ]),
            )
          ])),
      Align(
          alignment: Alignment.center,
          child: AnimatedOpacity(
              duration: Duration(milliseconds: opacityAnimation),
              opacity: listIsEmpty == true ? 1 : 0,
              child: Text('Список пуст', style: noDataStyle)))
    ]);
  }
}

class CardModel {
  int id;
  String churchTitle;
  int price;
  String applicationType;
  bool applicationTypeIsHealth;
  String lasting;
  String names;

  CardModel({
    @required this.id,
    @required this.churchTitle,
    @required this.price,
    @required this.applicationType,
    @required this.applicationTypeIsHealth,
    @required this.lasting,
    @required this.names,
  });
}

class CardWidget extends StatefulWidget {
  final CardModel cardModel;
  final List<CardModel> cardModelList;
  final int total;
  final Function(int price) onRemove;
  final cardIndex;

  const CardWidget(this.cardModel, this.cardModelList, this.total,
      this.onRemove, this.cardIndex);

  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> with TickerProviderStateMixin {
  var healthTypeGradient = [HexColor('#F1967C'), HexColor('#CE2E8E')];
  var reposeTypeGradient = [HexColor('#5E5095'), HexColor('#393D79')];
  int checkBoxValue = 1;
  bool isVisible = true;
  bool materialVisibility = true;

//  contH = D().w(context) * 0.685;
  Widget shaderMask({@required tileModeIsRepeated, @required child}) {
    return ShaderMask(
        blendMode: BlendMode.srcIn,
        shaderCallback: (Rect bounds) => RadialGradient(
                center: Alignment.topLeft,
                colors: widget.cardModel.applicationTypeIsHealth == true
                    ? healthTypeGradient
                    : reposeTypeGradient,
                tileMode:
                    tileModeIsRepeated ? TileMode.repeated : TileMode.mirror)
            .createShader(bounds),
        child: child);
  }

//  duration: Duration(milliseconds: 1000),
//  onEnd: () {
//  setState(() {
//  widget.onRemove(widget.cardIndex);
//  });
//  },
//  height: contH,
//  curve: Curves.fastOutSlowIn,

//  duration: Duration(milliseconds: 1000),
//  onEnd: () {
////          contH = 188;
////          isVisible = true;
//  widget.onRemove(widget.cardIndex);
////      setState(() {
////        widget.total = 400;
////      });
//  },
//  height: contH,
//  curve: Curves.fastOutSlowIn,
//  padding: EdgeInsets.only(bottom: isVisible == true ? 16.0 : 0, top: 0.0),
  bool tapped = false;

  @override
  Widget build(BuildContext context) {
//    print('Data is: $contH $isVisible ${widget.cardIndex}');
    return Padding(
      padding: EdgeInsets.only(top: isVisible == true ? 8.0 : 0),
      child: AnimatedOpacity(
        opacity: isVisible ? 1.0 : 0.0,
        duration: animationDuration,
        child: AnimatedSize(
          curve: Curves.fastOutSlowIn,
          duration: animationDurationX2,
          vsync: this,
          child: SizedBox(
            height: isVisible ? null : 0,
            child: Stack(overflow: Overflow.visible, children: <Widget>[
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0, top: 9),
                  child: PhysicalModel(
                    elevation: 4,
                    borderRadius: BorderRadius.circular(12),
                    color: Colors.white,
                    child: Material(
                      borderRadius: BorderRadius.circular(12),
                      child: GradientCard(
                        radius: 12,
                        strokeWidth: 0.7,
                        gradientColors:
                        widget.cardModel.applicationTypeIsHealth == true
                            ? healthTypeGradient
                            : reposeTypeGradient,
                        radiusOnly: false,
                        onPressed: () {
                          setState(() {
                            if (checkBoxValue == 1)
                              checkBoxValue = 0;
                            else
                              checkBoxValue = 1;
                          });
                        },
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                    width: 17,
                                    height: 17,
                                    child: GradientCard(
                                        gradientColors: widget.cardModel
                                            .applicationTypeIsHealth
                                            ? healthTypeGradient
                                            : reposeTypeGradient,
                                        radiusOnly: false,
                                        strokeWidth: 1,
                                        radius: 100,
                                        onPressed: () =>
                                            setState(() {
                                              if (checkBoxValue == 0)
                                                checkBoxValue = 1;
                                              else
                                                checkBoxValue = 0;
                                            }),
                                        child: Padding(
                                          padding: EdgeInsets.all(3),
                                          child: shaderMask(
                                              child: Container(
                                                  height: 11,
                                                  width: 11,
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: checkBoxValue == 1
                                                          ? HexColor('#F1967C')
                                                          : null)),
                                              tileModeIsRepeated: false),
                                        ))),
                              ),
                              Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 16, right: 14, bottom: 16),
                                      child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
//                                                flex: 3,
                                                    child: Text(
                                                        widget.cardModel
                                                            .churchTitle,
                                                        //     style: textTheme.subtitle1
                                                        style: Theme
                                                            .of(context)
                                                            .textTheme
                                                            .subtitle1),
                                                  ),
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        left: 0.0),
                                                    child: Align(
                                                        alignment:
                                                        Alignment.topRight,
                                                        child: Text(
                                                            '${widget.cardModel
                                                                .price}₽',
                                                            textAlign:
                                                            TextAlign
                                                                .right,
                                                            style: Theme
                                                                .of(
                                                                context)
                                                                .textTheme
                                                                .headline5)),
                                                  )
                                                ]),
                                            SizedBox(height: 8),
                                            GradientText(
                                                widget
                                                    .cardModel.applicationType,
                                                gradientColors: widget.cardModel
                                                    .applicationTypeIsHealth ==
                                                    true
                                                    ? healthTypeGradient
                                                    : reposeTypeGradient,
                                                textStyle: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .copyWith(
                                                    color: Colors.white)),
                                            SizedBox(height: 8),
                                            Text(widget.cardModel.lasting,
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .copyWith(
                                                    fontWeight:
                                                    FontWeight.w500)),
                                            SizedBox(height: 8),
                                            Text('Имена', style: captionGrey),
                                            SizedBox(height: 4),
                                            Text('${widget.cardModel.names}',
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .copyWith(
                                                    fontWeight:
                                                    FontWeight.w500))
                                          ])))
                            ])
                      )
                    )
                  )
                )
              ),
              Positioned(
                  right: 8,
                  top: -2,
                  child: Material(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    child: GradientCard(
                        strokeWidth: 1,
                        radius: 100,
                        radiusOnly: false,
                        onPressed: () {
                          widget.onRemove(widget.cardIndex);
                          isVisible = false;
                        },
                        gradientColors: widget.cardModel.applicationTypeIsHealth
                            ? healthTypeGradient
                            : reposeTypeGradient,
                        child: Padding(
                            padding: EdgeInsets.all(3),
                            child: Icon(
                              Icons.close,
                              size: 15,
                            )))
                  ))
            ])
          )
        )
      )
    );
  }
}
