import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_bottom_button.dart';
import 'package:church/widgets/gradient_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import '../../utils/hex_color.dart';
import '../../utils/masked_text.dart';
import '../../utils/relative_dimensions.dart';

class Payment extends StatefulWidget {
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: buildBody(context),
        bottomSheet: GradientBottomButton(
            text: 'Оплатить'.toUpperCase(), onPressed: () {}));
  }

  bool isNewCard;
  bool checkBoxValue = false;

  Widget buildBody(BuildContext context) {
    Widget topTitle = Text('Оплата',
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold));

    Widget cardChoosing({@required String text, @required bool newCard}) {
      return Row(
        children: <Widget>[
          Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                  color: newCard == true ? HexColor('8C8FD3') : Colors.grey,
                  shape: BoxShape.circle)),
          SizedBox(width: 20),
          Icon(Icons.credit_card,
              color: newCard == true ? HexColor('8C8FD3') : Colors.grey),
          SizedBox(width: 20),
          Text(text, style: TextStyle(fontSize: 16))
        ],
      );
    }

    Widget cardInfoTitle = Text('Данные карты',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold));

    Widget textFieldWithTitle(String title, String fieldType) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: Text('$title',
                  style: TextStyle(fontSize: 14, color: Colors.black54))),
          SizedBox(height: 4),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 2),
                      blurRadius: 20),
                ]),
            child: Theme(
              data: ThemeData(primaryColor: HexColor('#A094FE')),
              child: TextField(
                  keyboardType: TextInputType.number,
                  obscureText: fieldType == '3' ? true : false,
                  inputFormatters: [
                    fieldType == '1'
                        ? MaskedTextInputFormatter(
                            mask: 'xxxx xxxx xxxx xxxx', separator: ' ')
                        : fieldType == '2'
                            ? MaskedTextInputFormatter(
                                mask: 'xx/xx', separator: '/')
                            : MaskedTextInputFormatter(
                                mask: 'xxx', separator: ''),
                  ],
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16),
                  decoration: InputDecoration(
                      hoverColor: Colors.white,
                      hintText: fieldType == '1'
                          ? '**** **** **** ****'
                          : fieldType == '2' ? '**/**' : '***',
                      contentPadding: EdgeInsets.all(0),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: HexColor('#A094FE'), width: 1.0),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: HexColor('#A094FE')),
                          borderRadius:
                              BorderRadius.all(Radius.circular(12))))),
            ),
          ),
        ],
      );
    }

    Widget savedCardInfo() {
      return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(color: HexColor('#A094FE')),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  offset: Offset(0, 2),
                  blurRadius: 20),
            ]),
        height: 50,
        width: double.infinity,
        child: Row(
          children: <Widget>[
            SizedBox(width: 18),
            Transform.scale(
              scale: 1.2,
              child: GradientIcon(
                  icon: Icons.card_travel, gradientColors: visaGradient),
            ),
            Spacer(),
            Text(
              '•••• •••• •••• 5134',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            SizedBox(width: 18)
          ],
        ),
      );

//          Theme(
//            data: ThemeData(primaryColor: HexColor('#A094FE')),
//            child: TextField(
//                keyboardType: TextInputType.number,
//                obscureText: fieldType == '3' ? true : false,
//                inputFormatters: [
//                  fieldType == '1'
//                      ? MaskedTextInputFormatter(
//                          mask: 'xxxx xxxx xxxx xxxx', separator: ' ')
//                      : fieldType == '2'
//                          ? MaskedTextInputFormatter(
//                              mask: 'xx/xx', separator: '/')
//                          : MaskedTextInputFormatter(
//                              mask: 'xxx', separator: ''),
//                ],
//                textAlign: TextAlign.center,
//                style: TextStyle(fontSize: 16),
//                decoration: InputDecoration(
//                    hoverColor: Colors.white,
//                    hintText: fieldType == '1'
//                        ? '0000 0000 0000 0000'
//                        : fieldType == '2' ? '00/00' : '•••',
//                    contentPadding: EdgeInsets.all(0),
//                    enabledBorder: OutlineInputBorder(
//                        borderSide:
//                            BorderSide(color: HexColor('#A094FE'), width: 1.0),
//                        borderRadius: BorderRadius.all(Radius.circular(12))),
//                    border: OutlineInputBorder(
//                        borderSide: BorderSide(color: HexColor('A094FE')),
//                        borderRadius: BorderRadius.all(Radius.circular(12))))),
//          ),;
    }

    Widget checkBox() {
      return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
//          ShaderMask(
//              blendMode: BlendMode.srcIn,
//              shaderCallback: (Rect bounds) {
//                return ui.Gradient.linear(
//                  Offset(4.0, 24.0),
//                  Offset(20.0, 4.0),
//                  [Colors.pink, Colors.cyanAccent],
//                );
//              },
//              child:  ),
//        CheckboxS(
//            value: checkBoxValue,
//            tristate: false,
//            onChanged: (bool value) {
//              setState(() {
//                checkBoxValue = value;
//              });
//            }),

        Checkbox(
            value: checkBoxValue,
            tristate: false,
            onChanged: (bool value) {
              setState(() {
                checkBoxValue = value;
              });
            }),
        Flexible(
            child: InkWell(
              onTap: () {
                setState(() {
                  checkBoxValue = !checkBoxValue;
                });
              },
              child: Text('Сохранить для будущих платежей',
                  style: TextStyle(color: Colors.black54, fontSize: 15)),
        ))
      ]);
    }

    Widget bottomSectionPrice() {
      return Align(
        alignment: Alignment.centerRight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
                width: 120,
                height: 1,
                color: HexColor('#D5D5D6'),
                child: Divider()),
            SizedBox(height: 12),
            Text('Оплата на сумму',
                style: TextStyle(color: Colors.black54, fontSize: 16)),
            SizedBox(height: 12),
            Text('3200₽',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
          ],
        ),
      );
    }

    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BackButtonRow(),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(top: 64.0, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  topTitle,
                  SizedBox(height: 16),
                  cardChoosing(text: 'Новая карта', newCard: true),
                  SizedBox(height: 10),
                  cardChoosing(text: '**** 4313', newCard: false),
                  SizedBox(height: 10),
                  cardChoosing(text: '**** 8512', newCard: false),
                  SizedBox(height: 20),
                  cardInfoTitle,
                  SizedBox(height: 16),
//                  savedCardInfo(),
//                  SizedBox(height: 14),
                  textFieldWithTitle('Номер карты', '1'),
                  SizedBox(height: 14),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          width: D().w(context) * 0.35,
                          child: textFieldWithTitle('Срок действия', '2')),
                      Container(
                          width: D().w(context) * 0.35,
                          child: textFieldWithTitle('СVV', '3'))
                    ],
                  ),
                  SizedBox(height: 20),
                  checkBox(),
                  bottomSectionPrice(),
                  SizedBox(height: 100),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
