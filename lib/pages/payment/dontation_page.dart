import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/pages/payment/payment_page.dart';
import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_bottom_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';

class Donation extends StatefulWidget {
  @override
  _DonationState createState() => _DonationState();
}

class _DonationState extends State<Donation> {
  TextEditingController _textEditingController;

  @override
  void initState() {
    _textEditingController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
//    Widget closeButton = FlatButton(
//        splashColor: black3102,
////              typeHealth == true ? HexColor('#FA8072') : HexColor('#008ECC'),
//        child: Text('Закрыть',
//            style: Theme
//                .of(context)
//                .textTheme
//                .button
//                .copyWith(color: Color.fromRGBO(161, 152, 254, 1))),
//        onPressed: () => Navigator.pop(context));

    Widget gradientBottomButton = GradientBottomButton(
      text: 'Перейти к оплате'.toUpperCase(),
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Payment()));
//            if (_textEditingController.text.isNotEmpty)
//              Navigator.push(
//                context,
//                MaterialPageRoute(builder: (context) => Payment()),
//              );
//            else
//              showDialog(
//                  context: context, builder: (BuildContext context) => alert);
      },
    );
    return Scaffold(
        body: buildBody(context),
        bottomSheet: gradientBottomButton);
  }

  Widget textFieldWithTitle(String title, String type) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Visibility(
            visible: type == '1' ? false : true,
            child: Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Text('$title',
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: black0005))),
          ),
          SizedBox(height: 6),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 2),
                      blurRadius: 4)
                ]),
            child: Theme(
              data: ThemeData(primaryColor: Color.fromRGBO(161, 152, 254, 1)),
              child: TextField(
                  maxLines: null,
                  controller: type == '1' ? _textEditingController : null,
                  onChanged: (value) {
                    setState(() {});
                  }
                  ,
                  keyboardType:
                  type == '1' ? TextInputType.number : TextInputType.text,
                  style: type == '1'
                      ? Theme
                      .of(context)
                      .textTheme
                      .headline4
                      .copyWith(fontWeight: FontWeight.w300)
                      : Theme
                      .of(context)
                      .textTheme
                      .bodyText1,
                  decoration: InputDecoration(
                      prefixIcon: type == '1'
                          ? IconButton(icon: SvgPicture.asset(
                          Images.ruble.i(), height: 20,
                          width: 20,
                          color: black3105), onPressed: null)
                          : null,
                      hintText: type == '1' ? null : 'Ваш комментарий',
                      hintStyle: Theme
                          .of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: black3105),
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 16, vertical: type == '1' ? 12 : 20),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(161, 152, 254, 1),
                              width: 0.5),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(161, 152, 254, 1)),
                          borderRadius:
                          BorderRadius.all(Radius.circular(12))))),
            ),
          )
        ]);
  }

  Widget buildBody(BuildContext context) {
    Widget title = Text('Cделать пожертвование',
        style: Theme
            .of(context)
            .textTheme
            .headline5);
    return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              BackButtonRow(),
              Expanded(
                  child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 65),
                            title,
                            SizedBox(height: 8),
                            textFieldWithTitle('Сумма', '1'),
                            SizedBox(height: 12),
                            textFieldWithTitle('Комментарий', '2'),
                            SizedBox(height: 200),
                          ])))
            ]));
  }
}
