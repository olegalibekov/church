import 'package:audioplayers/audioplayers.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/gen_extensions/generated_img_extension.dart';
import 'package:church/models/story_model.dart';
import 'package:church/utils/constants.dart';
import 'package:church/utils/format_date.dart';
import 'package:church/widgets/gradient_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../custom_track_shape.dart';
import '../../utils/hex_color.dart';
import '../../utils/relative_dimensions.dart';

class StoryToRoad extends StatefulWidget {
  final PanelController pc;
  final ScrollController sc;

  const StoryToRoad({Key key, this.pc, this.sc}) : super(key: key);

  @override
  _StoryToRoadState createState() => _StoryToRoadState();
}

class _StoryToRoadState extends State<StoryToRoad>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    mindClass.globalStoryListener();
  }

  @override
  Widget build(BuildContext context) => buildSongList();

  Widget topSection() =>
      Column(children: <Widget>[
        SizedBox(height: 20),
        Container(
            width: 45,
            height: 6,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                color: black31.withOpacity(0.2))),
        Padding(
            padding: const EdgeInsets.only(right: 16.0, bottom: 16),
            child: Align(
                alignment: Alignment.topRight,
                child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                        onTap: () => widget.pc.close(),
                        child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text('Закрыть',
                                style:
                                Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText1))))))
      ]);

  Widget storyList() =>
      Expanded(
          child: ListView(
              padding: EdgeInsets.all(0),
              controller: widget.sc,
              children: <Widget>[
                for (var item in mindClass.globalStories.value.entries)
                  StoryCard(story: item)
              ]));

  Widget bottomPlayer() {
    return ValueListenableBuilder(
        valueListenable: mindClass.globalLastChosenStory,
        builder: (BuildContext context, MapEntry<String, StoryModel> value,
            Widget child) =>
            AnimatedSize(
                curve: Curves.fastOutSlowIn,
                duration: animationDuration,
                vsync: this,
                child: SizedBox(
                  height: value.key == null ? 0 : null,
                  child: Column(children: <Widget>[
                    SizedBox(
                        width: D().w(context) * 0.87,
                        child: Divider(color: dividerColor01, thickness: 2)),
                    Padding(
                        padding: const EdgeInsets.only(top: 12.0, bottom: 8),
                        child: ValueListenableBuilder(
                            valueListenable: mindClass.globalLastChosenStory,
                            builder: (context, value, child) =>
                                Text(
                                    value.key == null
                                        ? ''
                                        : value.value.get(
                                        [StoryModel.nameField]).toString(),
                                    style: playerTextStyleBold.copyWith(
                                        color: blueDark)))),
                    SizedBox(height: 8),
                    Container(
                        height: 20,
                        child: Row(mainAxisAlignment: MainAxisAlignment.center,
                            children: <
                                Widget>[
                              ValueListenableBuilder(
                                  valueListenable: mindClass
                                      .globalStoryCurrentTime,
                                  builder: (BuildContext context, int value,
                                      Widget child) =>
                                      Text(formatToMinutes(Duration(
                                          seconds: value), false),
                                          style: playerTextStyle.copyWith(
                                              color: blueDark))),
                              SizedBox(width: 12),
                              SizedBox(
                                  width: D().w(context) * 0.5,
                                  child: SliderTheme(
                                      data: SliderTheme.of(context).copyWith(
                                          inactiveTrackColor: HexColor(
                                              '#D9CFFF'),
                                          trackShape: CustomTrackShape(),
                                          trackHeight: 5.0,
                                          thumbShape:
                                          RoundSliderThumbShape(
                                              enabledThumbRadius: 0.0)),
                                      child: ValueListenableBuilder(
                                          valueListenable: mindClass
                                              .globalStoryIndicatorPosition,
                                          builder: (context, value, child) =>
                                              Slider(
                                                  activeColor: HexColor(
                                                      '#A092FF'),
                                                  value: value,
                                                  onChangeStart: (
                                                      double value) {
                                                    mindClass
                                                        .globalStoryIndPosIsChanging
                                                        .value =
                                                    true;
                                                  },
                                                  onChanged: (double value) {
                                                    mindClass
                                                        .globalStoryIndicatorPosition
                                                        .value =
                                                        value;
                                                  },
                                                  onChangeEnd: (double value) {
                                                    mindClass
                                                        .globalStoryIndPosIsChanging
                                                        .value =
                                                    false;

                                                    var seekTime = (mindClass
                                                        .globalLastChosenStory
                                                        .value.value
                                                        .get([
                                                      StoryModel.durationField
                                                    ]) *
                                                        value)
                                                        .toInt();

                                                    mindClass
                                                        .globalStoryIndicatorPosition
                                                        .value =
                                                        value;
                                                    mindClass
                                                        .globalStoryCurrentTime
                                                        .value = seekTime;
                                                    mindClass.storyPlayer
                                                        .seek(Duration(
                                                        seconds: seekTime));
                                                  })))),
                              SizedBox(width: 12),
                              ValueListenableBuilder(
                                  valueListenable: mindClass
                                      .globalLastChosenStory,
                                  builder: (context, value, child) =>
                                      Text(
                                          formatToMinutes(
                                              Duration(
                                                  seconds: value.value == null
                                                      ? 0
                                                      : value.value.get([
                                                    StoryModel.durationField
                                                  ])),
                                              false),
                                          style: playerTextStyle.copyWith(
                                              color: blueDark)))
                            ])),
                    SizedBox(height: 4),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Material(
                                  color: Colors.transparent,
                                  child: ValueListenableBuilder(
                                      valueListenable: mindClass
                                          .globalLastChosenStory,
                                      builder: (BuildContext context,
                                          MapEntry<String, StoryModel> value,
                                          Widget child) =>
                                          GradientIconButton(
                                              iconButton: IconButton(
                                                  iconSize: 45,
                                                  icon: SvgPicture.asset(
                                                      Images.backward_media
                                                          .i()),
                                                  onPressed: value.key ==
                                                      mindClass.globalStories
                                                          .value.values
                                                          .first.key
                                                      ? null
                                                      : () =>
                                                      mindClass
                                                          .storyBackwardTap()),
                                              gradientColors: value.key ==
                                                  mindClass.globalStories.value
                                                      .values
                                                      .first.key
                                                  ? playerOptionDisabledGradient
                                                  : darkGradient)))),
                          ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Material(
                                  color: Colors.transparent,
                                  child: ValueListenableBuilder(
                                      valueListenable: mindClass
                                          .globalStoryIsPlaying,
                                      builder: (BuildContext context, value,
                                          Widget child) =>
                                          GradientIconButton(
                                              iconButton: IconButton(
                                                  iconSize: 60,
                                                  icon: SvgPicture.asset(
                                                      value == false
                                                          ? Images.play_media
                                                          .i()
                                                          : Images.pause_media
                                                          .i()),
                                                  onPressed: () {
                                                    if (value == false)
                                                      mindClass.storyPlayer
                                                          .resume();
                                                    else
                                                      mindClass.storyPlayer
                                                          .pause();
                                                  }),
                                              gradientColors: darkGradient)))),
                          Padding(
                              padding: const EdgeInsets.only(bottom: 1.5),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Material(
                                      color: Colors.transparent,
                                      child: ValueListenableBuilder(
                                          valueListenable: mindClass
                                              ?.globalLastChosenStory,
                                          builder: (BuildContext context,
                                              MapEntry<String,
                                                  StoryModel> value,
                                              Widget child) =>
                                              GradientIconButton(
                                                  iconButton: IconButton(
                                                      iconSize: 45,
                                                      icon: SvgPicture.asset(
                                                          Images.forward_media
                                                              .i()),
                                                      onPressed: value.key ==
                                                          mindClass
                                                              .globalStories
                                                              .value
                                                              .values.last.key
                                                          ? null
                                                          : () =>
                                                          mindClass
                                                              .storyForwardTap()),
                                                  gradientColors: value.key ==
                                                      mindClass.globalStories
                                                          .value.values
                                                          .last.key
                                                      ? playerOptionDisabledGradient
                                                      : darkGradient)
                                      ))))
                        ])
                  ]),
                )
            )
    );
  }

  Widget buildSongList() =>
      Column(children: <Widget>[
        topSection(),
        storyList(),
        bottomPlayer(),
        SizedBox(height: 12)
      ]);
}

class StoryCard extends StatefulWidget {
  final MapEntry<String, StoryModel> story;

  const StoryCard({Key key, this.story}) : super(key: key);

  @override
  _StoryCardState createState() => _StoryCardState();
}

class _StoryCardState extends State<StoryCard> {
  @override
  void initState() {
    super.initState();
    _isPlaying = mindClass.checkStoryPlaying(widget.story.key);
    listeners();
  }

  @override
  Widget build(BuildContext context) => storyCard();

  bool _iconTap = false;
  bool _isPlaying = false;

  listeners() {
    mindClass.storyPlayer.onPlayerStateChanged.listen((AudioPlayerState s) {
      if (s == AudioPlayerState.STOPPED || s == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
      if (mindClass.globalLastChosenStory.value.key == widget.story.key &&
          mindClass.globalLastChosenStory != null &&
          mindClass.storyPlayer.state == AudioPlayerState.PLAYING)
        setState(() => _isPlaying = true);
      else if (mindClass.globalLastChosenStory.value.key == widget.story.key &&
          mindClass.globalLastChosenStory != null &&
          mindClass.storyPlayer.state == AudioPlayerState.PAUSED)
        setState(() => _isPlaying = false);
    });
  }

  onTap() {
//    if (mindClass.globalLastChosenStory.value.key != widget.story.key)
    if (globalLastChosenAudio.value.key != widget.story.key)
      mindClass.resetStory(widget.story);
    if (!_isPlaying) {
      Future.delayed(Duration(milliseconds: 70))
          .then((value) => mindClass.storyPlayer.resume());
    } else
      mindClass.storyPlayer.pause();
  }

  Widget storyCard() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          splashFactory: InkRipple.splashFactory,
          splashColor: Colors.grey[350],
          onTap: !_iconTap ? () => onTap() : null,
          child: AnimatedContainer(
              curve: Curves.easeInOut,
              duration: Duration(milliseconds: 300),
              color: _isPlaying ? Colors.grey[350] : Colors.transparent,
              child: Column(children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(
                        left: 16.0, right: 16.0, top: 6.0, bottom: 4.0),
                    child: Row(children: <Widget>[
                      InkWell(
                          customBorder: CircleBorder(),
                          onTapDown: (details) =>
                              setState(() => _iconTap = true),
                          onTapCancel: () => setState(() => _iconTap = false),
                          onTap: () {
                            setState(() => _iconTap = false);
                            onTap();
                          },
                          child: IconButton(
                              icon: SvgPicture.asset(
                                  _isPlaying == false
                                      ? Images.rounded_play_media.i()
                                      : Images.rounded_pause_media.i(),
                                  color: playerBlueDark,
                                  width: 40,
                                  height: 40),
                              onPressed: null)),
                      SizedBox(width: 10),
                      Expanded(
                          flex: 3,
                          child: Text(
                              widget.story.value.get([StoryModel.nameField]),
                              style: playerTextStyleBold)),
                      Expanded(
                          flex: 1,
                          child: Text(
                              formatToMinutes(
                                  Duration(
                                      seconds: widget.story.value
                                          .get([StoryModel.durationField])),
                                  false)
                                  .toString(),
                              textAlign: TextAlign.end,
                              style: playerTextStyle))
                    ])),
                Padding(
                    padding: EdgeInsets.only(bottom: 0, left: 65, right: 16),
                    child:
                    Divider(color: dividerColor01, thickness: 1, height: 0))
              ]))),
    );
  }
}

//class PodcastCard extends StatefulWidget {
//  final MapEntry<String, StoryModel> podcast;
//
//  const PodcastCard({Key key, this.podcast}) : super(key: key);
//
//  @override
//  _PodcastCardState createState() => _PodcastCardState();
//}
//
//class _PodcastCardState extends State<PodcastCard> {
//  bool iconTap = false;
//  bool isPlaying = false;
//
//  @override
//  Widget build(BuildContext context) {
//    return Material(
//        color: Colors.transparent,
//        child: InkWell(
//            onTap: iconTap == false
//                ? () {
//                    setState(() {
//                      isPlaying = !isPlaying;
//                    });
//                  }
//                : null,
//            child: Column(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  Container(
//                      height: 55,
//                      child: Padding(
//                          padding:
//                              const EdgeInsets.only(left: 12.0, right: 12.0),
//                          child: ListTile(
//                              leading: Material(
//                                  color: Colors.transparent,
//                                  child: InkWell(
//                                      borderRadius: BorderRadius.circular(100),
//                                      onTapDown: (details) {
//                                        setState(() => iconTap = true);
//                                      },
//                                      onTapCancel: () {
//                                        setState(() => iconTap = false);
//                                      },
//                                      onTap: () async {
//                                        setState(() {
//                                          iconTap = false;
//                                          isPlaying = !isPlaying;
//                                        });
//                                      },
//                                      child: IconButton(
//                                          iconSize: 42,
//                                          padding: EdgeInsets.all(0),
//                                          onPressed: null,
//                                          icon: SvgPicture.asset(
//                                              isPlaying == false
//                                                  ? Images.rounded_play_media
//                                                      .i()
//                                                  : Images.rounded_pause_media
//                                                      .i(),
//                                              color: playerBlueDark)))),
//                              title: Padding(
//                                  padding: const EdgeInsets.only(left: 4.0),
//                                  child: Text('1fdsfdsafdafdsasdfdsafdas76fgyusdhfaf78dddshufiadsfdsafdasfds', style: playerTextStyleBold)),
//                              trailing: Text('1',
//                                  style: playerTextStyle.copyWith(
//                                      color: blueDark))))),
//                  Align(
//                      alignment: Alignment.bottomCenter,
//                      child: Padding(
//                          padding: EdgeInsets.only(
//                              top: 0, bottom: 0, left: 92, right: 24),
//                          child: Divider(
//                              color: dividerColor01, thickness: 1, height: 0)))
//                ])));
//  }
//}
//      body: Container(
//        width: double.infinity,
//        height: double.infinity,
//        child: FlatButton(
//          onPressed: () {
//            playerItems.add(
//                PlayerItems(songName: 'The Dark Side', songLength: '3:30'));
//            onButtonPressed(context);
//          },
//          child: Text('Open'),
//        ),
//      ),
//  Widget _scrollingList(ScrollController sc) {
////    return SingleChildScrollView(child: buildSongList(context));
//    return ListView.builder(
//      controller: sc,
//      itemCount: 1,
//      itemBuilder: (BuildContext context, int i) {
//        return buildSongList(widget.bContext);
//      },
//    );
//  }

//  onButtonPressed(BuildContext context) {
//    showModalBottomSheet(
//        isScrollControlled: true,
//        context: context,
//        builder: (context) {
//          return StatefulBuilder(
//              builder: (BuildContext context, StateSetter modalSetState) {
//            return Container(
//                height: D().h(context) * 0.95,
//                color: Color(0xFF737373),
//                child: Container(
//                    child: buildSongList(context),
//                    decoration: BoxDecoration(
//                        color: Theme.of(context).canvasColor,
//                        borderRadius: BorderRadius.only(
//                            topLeft: const Radius.circular(15),
//                            topRight: const Radius.circular(15)))));
//          });
//        });
//  }
//void musicStateIsPlaying(int id) {
//  setState(() async {
//    if (lastChosenItemId == null) {
//      audioPlayer = await audioCache.play('spas.mp4');
//      isPlaying = true;
//    } else if (lastChosenItemId != id) {
//      isPlaying = false;
//      playerItems[lastChosenItemId].icon = Icons.play_circle_outline;
//      playerItems[lastChosenItemId].listTileIsPlaying = false;
//      audioPlayer.pause();
//      audioCache.clearCache();
//      audioPlayer = await audioCache.play('spas.mp4');
////        audioPlayer = await audioCache.play('spas.mp4');
//      audioPlayer.resume();
//      isPlaying = true;
//    } else {
//      audioPlayer.resume();
//      isPlaying = true;
//    }
//    lastChosenItemId = id;
//    playerItems[id].icon = Icons.pause_circle_outline;
//    setState(() {});
//  });
//}
//
//void musicStateIsStopped(int id) {
//  setState(() {
//    audioPlayer.pause();
//    isPlaying = false;
//    playerItems[id].icon = Icons.play_arrow;
//  });
//}
