import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/head_module/brain.dart';
import 'package:church/head_module/database_extensions/stories_extension.dart';
import 'package:church/models/story_model.dart';
import 'package:church/pages/history/story_to_road_page.dart';
import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_button.dart';
import 'package:church/widgets/gradient_card.dart';
import 'package:church/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../utils/relative_dimensions.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  PanelController pc = PanelController();

  @override
  void initState() {
    initStories() async {
      Map<String, StoryModel> stories = await mindClass?.getStories();
//      Map<String, StoryModel> stories = {};
      mindClass.globalStories.value =
          await mindClass.sortPopularStories(stories);
      setState(() {});
    }

    initStories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SlidingUpPanel(
            controller: pc,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15), topRight: Radius.circular(15)),
            maxHeight: D().h(context) * 0.97,
//            minHeight: D().h(context) * 0.97,
            minHeight: 0,
//            panel: Container(),
            panelBuilder: (sc) => StoryToRoad(pc: pc),
            body: buildBody(context)));
  }

  Widget buildCard({@required String churchTitle,
    @required String checkInDate,
    @required String checkOutDate,
    @required String guestNumber,
    @required String comment,
    @required String price,
    @required String imageUrl,
    @required bool applicationTypeIsHealth,
    @required bool future}) {
    return Padding(
        padding: EdgeInsets.only(bottom: 24),
        child: Stack(overflow: Overflow.visible, children: <Widget>[
          PhysicalModel(
              elevation: 4,
              borderRadius: BorderRadius.circular(12),
              color: Colors.white,
              child: Material(
                  borderRadius: BorderRadius.circular(12),
                  child: GradientCard(
                      radius: 12,
                      strokeWidth: 1,
                      onPressed: () {},
                      gradientColors: applicationTypeIsHealth == true
                          ? redGradient
                          : darkGradient,
                      radiusOnly: false,
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: 16, left: 16, bottom: 16, right: 8),
                          child: IntrinsicHeight(
                              child: Row(children: [
                                Flexible(
                                    flex: 3,
                                    child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(churchTitle,
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .subtitle1),
                                          SizedBox(height: 8),
                                          Text('Даты:', style: captionGrey),
                                          SizedBox(height: 4),
                                          Text(
                                              'С $checkInDate до $checkOutDate',
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .copyWith(
                                                  fontWeight: FontWeight.w500)),
                                          SizedBox(height: 6),
                                          Text('Количество гостей:',
                                              style: captionGrey),
                                          SizedBox(height: 4),
                                          Text('$guestNumber',
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .copyWith(
                                                  fontWeight: FontWeight.w500)),
                                          SizedBox(height: 8),
                                          Text('Комментарий:',
                                              style: captionGrey),
                                          SizedBox(height: 4),
                                          Align(
                                              alignment: Alignment.topLeft,
                                              child: Text('$comment',
                                                  style: Theme
                                                      .of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      .copyWith(
                                                      fontWeight:
                                                      FontWeight.w500)))
                                        ])),
                                Flexible(
                                    flex: 2,
                                    child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: GradientText('$price₽',
                                            textStyle: Theme
                                                .of(context)
                                                .textTheme
                                                .headline5
                                                .copyWith(color: Colors.white),
                                            gradientColors: darkGradient)))
                              ])))))),
          Positioned(
              right: -6,
              top: -5,
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: 92,
                      height: 92,
                      imageUrl: imageUrl,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          Icon(Icons.error)))),
        ]));
  }

  Widget buildBody(BuildContext context) {
    List<Color> gradient = [
      Color.fromRGBO(161, 165, 254, 0.85),
      Color.fromRGBO(160, 126, 255, 0.85)
    ];

    Widget bottomButton = Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
            padding: const EdgeInsets.only(bottom: 24, right: 16),
            child: SizedBox(
                width: D().w(context) < 500
                    ? D().w(context)
                    : D().w(context) * 0.6,
                height: 68,
                child: Row(children: <Widget>[
                  Opacity(
                      opacity: 0.0,
                      child: IgnorePointer(child: BackButtonRow())),
                  Expanded(
                      child: PhysicalModel(
                          elevation: 3,
                          borderRadius: BorderRadius.circular(12),
                          color: Colors.transparent,
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: BackdropFilter(
                                  filter:
                                  ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                                  child: GradientButton(
                                      onPressed: () =>
                                          pc.animatePanelToPosition(1),
//                                        onPressed: () => buildModalSheet(context),
                                      child: Stack(children: <Widget>[
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 12.0),
                                                child: Icon(Icons.music_note,
                                                    size: 30,
                                                    color: Colors.white))),
                                        Align(
                                            alignment: Alignment.center,
                                            child: Text('Аудио в дорогу',
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .subtitle1
                                                    .copyWith(
                                                    color: Colors.white)))
                                      ]),
                                      gradientColor: gradient)))))
                ]))));

    Widget title =
    Text('История', style: Theme
        .of(context)
        .textTheme
        .headline5);

    return Stack(children: <Widget>[
      SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(children: <Widget>[
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: <
                Widget>[
              BackButtonRow(),
              Expanded(
                  child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 65),
                            title,
                            SizedBox(height: 14),
                            Text('Предстоит',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(color: black0005)),
                            SizedBox(height: 16),
                            if (D().w(context) > 599)
                              Wrap(spacing: 20, children: <Widget>[
                                Center(
                                    child: SizedBox(
                                        width: D().w(context) * 0.4,
                                        child: buildCard(
                                            churchTitle:
                                            'Преображенский мужской монастырь',
                                            checkInDate: '10/03',
                                            checkOutDate: '14/03',
                                            guestNumber: '2',
                                            comment: 'Текст комментария',
                                            price: '13 250',
                                            imageUrl:
                                            'https://idilliainn.ru/wp-content/uploads/2019/10/kuznechnyj-per-18-fullsize-24.jpg',
                                            applicationTypeIsHealth: true,
                                            future: true)))
                              ]),
                            if (D().w(context) < 600)
                              buildCard(
                                  churchTitle:
                                  'Преображенский мужской монастырь',
                                  checkInDate: '10/03',
                                  checkOutDate: '14/03',
                                  guestNumber: '2',
                                  comment: 'Текст комментария',
                                  price: '13 250',
                                  imageUrl:
                                  'https://idilliainn.ru/wp-content/uploads/2019/10/kuznechnyj-per-18-fullsize-24.jpg',
                                  applicationTypeIsHealth: true,
                                  future: true),
                            Text('Прошлые',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(color: black0005)),
                            SizedBox(height: 14),
                            if (D().w(context) > 599)
                              Center(
                                  child: Wrap(spacing: 20, children: <Widget>[
                                SizedBox(
                                    width: D().w(context) * 0.4,
                                    child: buildCard(
                                        churchTitle:
                                        'Муромский мужской монастырь',
                                        checkInDate: '22/02',
                                        checkOutDate: '24/02',
                                        guestNumber: '1',
                                        comment: 'Текст комментария',
                                        price: '9 560',
                                        imageUrl:
                                        'https://www.oksana-hotel.com/upload/resize_cache/iblock/59c/580_372_2/%D0%B4%D0%B5%D1%81%D1%8F%D1%82%D1%8B%D0%B5%20%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%B0_1.jpg',
                                        applicationTypeIsHealth: false,
                                        future: false)),
                                SizedBox(
                                    width: D().w(context) * 0.4,
                                    child: buildCard(
                                        churchTitle:
                                        'Преображенский мужской монастырь',
                                        checkInDate: '16/01',
                                        checkOutDate: '19/01',
                                        guestNumber: '2',
                                        comment: 'Текст комментария',
                                        price: '14 540',
                                        imageUrl:
                                        'https://media-cdn.tripadvisor.com/media/photo-s/07/59/da/19/likehome-hostel.jpg',
                                        applicationTypeIsHealth: false,
                                        future: false))
                                  ])),
                            if (D().w(context) < 600)
                              Column(children: <Widget>[
                                buildCard(
                                    churchTitle: 'Муромский мужской монастырь',
                                    checkInDate: '22/02',
                                    checkOutDate: '24/02',
                                    guestNumber: '1',
                                    comment: 'Текст комментария',
                                    price: '9 560',
                                    imageUrl:
                                    'https://www.oksana-hotel.com/upload/resize_cache/iblock/59c/580_372_2/%D0%B4%D0%B5%D1%81%D1%8F%D1%82%D1%8B%D0%B5%20%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%B0_1.jpg',
                                    applicationTypeIsHealth: false,
                                    future: false),
                                buildCard(
                                    churchTitle:
                                    'Преображенский мужской монастырь',
                                    checkInDate: '16/01',
                                    checkOutDate: '19/01',
                                    guestNumber: '2',
                                    comment: 'Текст комментария',
                                    price: '14 540',
                                    imageUrl:
                                    'https://media-cdn.tripadvisor.com/media/photo-s/07/59/da/19/likehome-hostel.jpg',
                                    applicationTypeIsHealth: false,
                                    future: false)
                              ]),
                            SizedBox(height: 90),
                          ])))
            ])
          ])),
      ValueListenableBuilder(
        valueListenable: mindClass?.globalStories,
        builder: (BuildContext context, Map<String, StoryModel> value,
            Widget child) {
          return AnimatedOpacity(opacity: value.keys?.first == null ? 0 : 1,
              duration: animationDuration,
              child: bottomButton);
        },
      )
    ]);
  }
}

//  buildModalSheet(BuildContext context) {
//    showModalBottomSheet(
//        elevation: 1,
//        shape: RoundedRectangleBorder(
//            borderRadius: BorderRadius.only(
//                topLeft: Radius.circular(15), topRight: Radius.circular(15))),
//        clipBehavior: Clip.antiAlias,
//        context: context,
//        builder: (context) {
//          return DraggableScrollableSheet(
//              expand: false,
//              maxChildSize: .95,
//              builder:
//                  (BuildContext context, ScrollController scrollController) {
//                return StoryToRoad(Key('storyToRoad'), context, closePanel,
//                    panelStateScroll, scrollController);
//              });
//        });
//  }
