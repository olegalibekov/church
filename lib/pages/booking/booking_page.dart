import 'package:cached_network_image/cached_network_image.dart';
import 'package:church/utils/relative_dimensions.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_bottom_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'booking_guest_info_page.dart';

class Booking extends StatefulWidget {
  @override
  _BookingState createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: buildBody(context), bottomNavigationBar: bottomButton());
  }

  Widget buildCard(String title, String cover) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 12.0),
        child: AspectRatio(
          aspectRatio: 1.847560977,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      height: D().w(context) < 600 ? 45 : 60,
                      width: D().w(context) < 600 ? 45 : 60,
                      imageUrl: cover,
                      placeholderFadeInDuration: Duration(seconds: 1),
                      errorWidget: (context, url, error) => Icon(Icons.error))),
              Align(
                  alignment: Alignment.center,
                  child: Text('$title',
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .copyWith(color: Colors.white)))
            ],
          ),
//          child: Card(
//              color: cover,
//              elevation: 3,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(12.0)),
//              child: Align(
//                  alignment: Alignment.center,
//                  child: Text('$title',
//                      style: Theme
//                          .of(context)
//                          .textTheme
//                          .subtitle1
//                          .copyWith(color: Colors.white))))
        ));
  }

  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(children: <Widget>[
          Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                BackButtonRow(),
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 65),
                              Text('Тип проживания',
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .headline5),
                              SizedBox(height: 20),
                              buildCard('Одноместная комната',
                                  'https://sputnik-hotel.ru/wp-content/uploads/2016/08/%D0%A1%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D1%82-1%D0%BD%D0%BE-%D0%BC%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B9-850x450.jpg'),
                              buildCard('Двухместная комната',
                                  'https://media-cdn.tripadvisor.com/media/photo-s/08/9b/70/95/5th-floor.jpg'),
                              buildCard('Трехместная комната',
                                  'https://lh3.googleusercontent.com/proxy/InYX0zhSx6ald7nvepHccfmHn1GNZXisj4jN5ayIZT7ErZr_AjPZYPebTs0eijHrpzHTsHYvnWMNbHDNwqp8XC3nSLD1Bf6X'),
                              SizedBox(height: 35),
                            ])))
              ])
        ]));
  }

  Widget bottomButton() {
    return GradientBottomButton(
        text: 'Далее'.toUpperCase(),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => BookingGuestInfo()));
        });
  }
}
