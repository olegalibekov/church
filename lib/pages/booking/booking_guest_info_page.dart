import 'package:church/utils/constants.dart';
import 'package:church/widgets/back_button_row.dart';
import 'package:church/widgets/gradient_bottom_button.dart';
import 'package:church/widgets/gradient_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import '../../utils/hex_color.dart';
import '../../utils/masked_text.dart';
import '../../utils/relative_dimensions.dart';

class BookingGuestInfo extends StatefulWidget {
  @override
  _BookingGuestInfoState createState() => _BookingGuestInfoState();
}

class _BookingGuestInfoState extends State<BookingGuestInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: buildBody(context), bottomSheet: bottomButton());
  }

  Widget buildCard(String title, Color cover) {
    return Column(children: <Widget>[
      Container(
          width: double.infinity,
          height: 180,
          child: Card(
              color: cover,
              elevation: 3,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0)),
              child: Align(
                  alignment: Alignment.center,
                  child: Text('$title',
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold))))),
      SizedBox(height: 16)
    ]);
  }

//  var maskFormatter =
//      MaskTextInputFormatter(mask: '##/##', filter: {"#": RegExp(r'[0-9]')});

  Widget date(String title) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: Text('$title',
              style: Theme
                  .of(context)
                  .textTheme
                  .caption
                  .copyWith(color: black3F07))),
      SizedBox(height: 8),
      Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              boxShadow: [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    offset: Offset(0, 8),
                    blurRadius: 15)
              ]),
          child: Theme(
              data: ThemeData(primaryColor: Color.fromRGBO(161, 152, 254, 1)),
              child: TextField(
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  maxLength: 5,
                  inputFormatters: [
//                  maskFormatter
                    MaskedTextInputFormatter(mask: 'xx/xx', separator: '/')
                  ],
                  style: Theme
                      .of(context)
                      .textTheme
                      .bodyText1,
                  decoration: InputDecoration(
                      counterText: "",
//                      hintText: '**/**',
                      hintStyle: Theme
                          .of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: black3F07),
                      contentPadding:
                      EdgeInsets.only(top: 20, bottom: 20),
                      enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(14.0)),
                          borderSide: BorderSide(
                              color: Color.fromRGBO(161, 152, 254, 1),
                              width: 1)),
                      border: OutlineInputBorder(
                        borderRadius:
                        const BorderRadius.all(const Radius.circular(14.0)),
                      )))))
    ]);
  }

  int _value = 1;

  Widget guestNumber() {

    final List<Color> gradientColors = [
      HexColor('#A095FE'),
      HexColor('#A095FE')
    ];
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      SizedBox(height: 20),
      Text('Количество гостей',
          style:
          Theme
              .of(context)
              .textTheme
              .caption
              .copyWith(color: black3F07)),
      SizedBox(height: 20),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    offset: Offset(0, 8),
                    blurRadius: 15),
              ]),
          child: Material(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            child: GradientCard(
                onPressed: _value == 1
                    ? null
                    : () {
                        setState(() {
                          if (_value > 1) _value--;
                        });
                      },
                gradientColors: gradientColors,
                radiusOnly: false,
                radius: 100,
                strokeWidth: 1,
                child:
                Icon(Icons.remove, size: 30, color: HexColor('#4A4686'))),
          ),
        ),
        SizedBox(width: 22),
        Text('$_value',
            style: Theme
                .of(context)
                .textTheme
                .headline4
                .copyWith(color: blueDark, fontWeight: FontWeight.w500)),
        SizedBox(width: 22),
//            GradientButton(
//                width: 60,
//                height: 60,
//                onPressed: () {},
//                gradientColor: gradientColors,
//                child: Icon(Icons.add, size: 30, color: HexColor('#4A4686'))),
        Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 8),
                      blurRadius: 15),
                ]),
            child: Material(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              child: GradientCard(
//                      onPressed: _value == 0?null:() {
//                        setState(() {
//                          if (_value > 0) _value--;
//                        });
//                      },
                  onPressed: _value == 10
                      ? null
                      : () {
//                          print(_value);
                    setState(() {
                      if (_value < 10) _value++;
                    });
                  },
                  gradientColors: gradientColors,
                  radiusOnly: false,
                  radius: 100,
                  strokeWidth: 1,
                  child: Icon(Icons.add, size: 30, color: HexColor('#4A4686'))),
            ))
      ])
    ]);
  }

  Widget buildComment() {
    return Container(
        width: double.infinity,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 2.0),
                child: Text('Комментарий',
                    style: Theme
                        .of(context)
                        .textTheme
                        .caption
                        .copyWith(color: black3F07)),
              ),
              SizedBox(height: 7),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            offset: Offset(0, 10),
                            blurRadius: 15),
                      ]),
                  child: Theme(
                      data: ThemeData(
                          primaryColor: Color.fromRGBO(161, 152, 254, 1)),
                      child: TextField(
                          maxLines: null,
                          keyboardType: TextInputType.text,
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1,
                          onTap: () {
                            setState(() {});
                          },
                          decoration: InputDecoration(
                              hintText: 'Ваш комментарий',
                              hintStyle: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(color: black3F07),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 24, vertical: 20),
                              enabledBorder: const OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                      const Radius.circular(15.0)),
                                  borderSide: const BorderSide(
                                      color: Color.fromRGBO(161, 152, 254, 1),
                                      width: 1)),
                              border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                      const Radius.circular(15.0)))))))
            ]));
  }

  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              BackButtonRow(),
              Expanded(
                  child: Padding(
                      padding: const EdgeInsets.only(right: 24.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 70),
                            Text('Информация о гостях',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .headline5),
                            SizedBox(height: 30),
                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Flexible(flex: 5, child: date('Дата заезда')),
                                  SizedBox(width: D().w(context) * 0.04),
                                  Flexible(flex: 5, child: date('Дата выезда'))
                                ]),
                            guestNumber(),
                            SizedBox(height: 28),
                            buildComment(),
                            SizedBox(height: 120),
//                        SizedBox(height: 300)
                          ])))
            ]));
  }

  Widget bottomButton() {
    return GradientBottomButton(
        text: 'Забронировать'.toUpperCase(), onPressed: () {});
  }
}
