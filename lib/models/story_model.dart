import 'package:church/head_module/database_extender.dart';

class StoryModel extends ModelInterface {
  static final TString coverField = "cover".t<String>();
  static final TString durationField = "duration".t<int>();
  static final TString nameField = "name".t<String>();
  static final TString urlField = "url".t<String>();
  static final TString timestampField = "timestamp".t<int>();
  static final TString ratingField = "rating".t<Map<String, int>>();

  @override
  String child() => 'static_data/stories/';

  @override
  ModelInterface init() => StoryModel();

  @override
  Map<TString, Function> fields() => {
        coverField: null,
        durationField: null,
        nameField: null,
        urlField: null,
        timestampField: null,
        ratingField: null
      };
}
