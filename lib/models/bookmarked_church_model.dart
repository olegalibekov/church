import 'package:church/head_module/database_extender.dart';

class BookmarkedChurchModel extends ModelInterface {
  static final TString bookmarkField = "bookmark".t<bool>();

  @override
  String child() => null;

  @override
  ModelInterface init() => BookmarkedChurchModel();

  @override
  Map<TString, Function> fields() => {bookmarkField: null};
}