import 'package:church/head_module/database_extender.dart';

import '../bookmarked_church_model.dart';
import 'user_info_model.dart';

class UserModel extends ModelInterface {
  static final TString firstNameField = "first_name".t<String>();
  static final TString secondNameField = "second_name".t<String>();
  static final TString churchesField = "churches"
      .t<Map<String, BookmarkedChurchModel>>(
          objectInitializer: () => BookmarkedChurchModel());
  static final TString userInfoField =
      "user_info".t<UserInfoModel>(objectInitializer: () => UserInfoModel());

  @override
  String child() => "user_data";

  static final String owner = "owner";
  static final String admin = "admin";
  static final String editor = "editor";
  static final String user = "user";

  static final String delete = "delete";
  static final String blocked = "blocked";
  static final String enable = "enable";

  roles() {
    return [user, owner, admin, editor];
  }

  @override
  Map<TString, Function> fields() => {
    firstNameField: null,
        secondNameField: null,
        churchesField: null,
    userInfoField: null
      };

  @override
  ModelInterface init() => UserModel();
}
