import 'package:church/head_module/database_extender.dart';

class UserInfoModel extends ModelInterface {
  static final TString roleField = "role".t<String>();
  static final TString stateField = "state".t<String>();

  @override
  String child() => "user_data/";

  @override
  Map<TString, Function> fields() => {
        roleField: null,
        stateField: null,
      };

  @override
  ModelInterface init() => UserInfoModel();
}
