import 'package:church/head_module/database_extender.dart';
import 'package:church/models/tour/slide_model.dart';

class TourModel extends ModelInterface {
  static final TString durationField = "duration".t<String>();
  static final TString nameField = "name".t<String>();
  static final TString urlField = "url".t<String>();
  static final TString slidesField = "slides".t<Map<String, SlideModel>>(
      objectInitializer: () => SlideModel());

  @override
  String child() => 'static_data/tours';

  @override
  ModelInterface init() => TourModel();

  @override
  Map<TString, Function> fields() =>
      { durationField: null,
        nameField: null,
        urlField: null,
        slidesField: null};
}
