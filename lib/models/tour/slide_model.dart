import 'package:church/head_module/database_extender.dart';

class SlideModel extends ModelInterface {
  static final TString textField = "text".t<String>();
  static final TString urlField = "url".t<String>();
  static final TString headerField = "header".t<String>();

  @override
  String child() => null;

  @override
  ModelInterface init() => SlideModel();

  @override
  Map<TString, Function> fields() =>
      {textField: null, urlField: null, headerField: null};
}
