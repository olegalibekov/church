import 'package:church/head_module/database_extender.dart';

class BookingPlace extends ModelInterface {
  static final TString addressField = "address".t<String>();
  static final TString emailField = "email".t<String>();
  static final TString phoneField = "phone".t<String>();
  static final TString uidField = "uid".t<String>();
  static final TString urlsField = "urls".t<List<String>>();

  @override
  String child() => 'static_data/booking_place';

  @override
  ModelInterface init() => BookingPlace();

  @override
  Map<TString, Function> fields() =>
      {
        addressField: null,
        emailField: null,
        phoneField: null,
        uidField: null,
        urlsField: null,
      };
}
