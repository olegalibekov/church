import 'package:church/head_module/database_extender.dart';

class OwnerModel extends ModelInterface {
  static final TString<String> cardNumberField = TString("card_number");
  static final TString<String> nameField = TString("name");
  static final TString<String> phoneNumberField = TString("phone_number");
  static final TString<String> postField = TString("post");
  static final TString<String> url = TString("url");

  @override
  String child() => 'static_data/churches/first_church/owner';

  @override
  ModelInterface init() => OwnerModel();

  @override
  Map<TString, Function> fields() {
    return {
      cardNumberField: null,
      nameField: null,
      phoneNumberField: null,
      postField: null,
      url: null
    };
  }
}
