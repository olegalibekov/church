import 'package:church/head_module/database_extender.dart';

class LatLonModel extends ModelInterface {
  static final TString latField = "lat".t<double>();
  static final TString lonField = "lon".t<double>();

  @override
  String child() => null;

  @override
  ModelInterface init() => LatLonModel();

  @override
  Map<TString, Function> fields() => {latField: null, lonField: null};
}
