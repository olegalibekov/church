import 'package:church/head_module/database_extender.dart';

import 'lat_lon_model.dart';
import 'owner_model.dart';

class ChurchModel extends ModelInterface {
  static final TString aboutField = "about".t<String>();
  static final TString addressField = "address".t<String>();
  static final TString souvenirAddressField = "souvenir_address".t<String>();
  static final TString nameField = "name".t<String>();
  static final TString tourIdField = "tour_id".t<String>();
  static final TString typeField = "type".t<String>();
  static final TString bookingField = "booking".t<Map<String, int>>();
  static final TString urlsField = "urls".t<List<String>>();
  static final TString latLonField = "lat,lon".t<LatLonModel>(objectInitializer: ()=>LatLonModel());
  static final TString ownerField = "owner".t<OwnerModel>(objectInitializer: ()=>OwnerModel());

  @override
  String child() => 'static_data/churches';

  @override
  Map<TString, Function> fields() =>
      {
        aboutField: null,
        addressField: null,
        souvenirAddressField: null,
        nameField: null,
        tourIdField: null,
        typeField: null,
        bookingField: null,
        latLonField: null,
        ownerField: null,
        urlsField: null
      };

  @override
  ModelInterface init() => ChurchModel();
}
